<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg)" alt="logo" width="300px" style="margin-top: 20px;"/>

# Smart Garage Teamwork Project Assignment  

### Auto Service Management System

<p align="center">

## 1.**Project Description**
Web Application and Api that help employees of an auto repair shop to manage their day-to-day job. Having the opportunity to register arriving customers as well as their vehicles. To connect them with the services of their choice. Gives the customers the opportunity to review the repairs made to their vehicles and create a pdf file with the repairs made on the vehicles, as well as the price of these repairs in the currency of their choice. 

## 2. **Technologies used in the Smart Garage project**

ASP.NET Core 3.1 and Visual Studio 2019

REST API

Database back-end: MS SQL Server

Entity Framework Core, EF InMemory

Standard ASP.NET Identity System

Unit test framework: MSTest


External API services: Fixer a JSON API for currency exchange processing.

Frontend: HTML, CSS, JavaScript, Bootstrap

Team work: Git (GitLab) with separate branches, Git Lab Issues and board, MS Teams.

Best programming practices and principles used: OOP, SOLID, KISS and DRY principles, client-side and server-side data validation, exception handling, unit testing of the "business" functionality, etc.

<br><br>
## 3. **Problem Solving**
#### Each unauthenticated _visitor_ of the Smart Garage web app can:

✅ See **all offered services** of Smart Garae.

✅**Login** and **logout**

<br/>

####  **After receiving registration on the Smart Garage web application, an authenticated _user_ can**:
✅ Users have the ability to change password.

✅ Мonitor the **vehicles** that have been linked to him. 

✅ Мonitor information about all **services** linked to his vehicles. 

✅ Filter all **services** linked to his vehicles, by registration plate or/and date.

✅ Мonitor information about all **visits** in Smart Garage.

✅ Review how much money has paid for all his visits so far. For each visit separately and for each exact service on his vehicle.

✅ Аn opportunity is provided for the user to change the currency of the displayed price,
as well as to generate a PDF file for the desired visit.

<br />

#### **Further to the functionalities available for registered users, the _employee_ user on the Smart Garage web application can**:

✅ Create, edit and delete any **vehicles** on the app’s database and link them to a user. 

✅ Filter all **vehicles** by owner.

✅ Create, edit and delete any **vehicle manufacturers** on the app’s database.

✅ Create, edit and delete any **vehicle models** on the app’s database.

✅ Create, edit and delete any **services** on the app’s database.

✅ Filter all **services** by name or/and price.

✅ Create, edit and delete any **users** on the app’s database. 

✅ After creating **new user**, an email will be send to the new user with automatically generated login information.

✅ Filter all **users** by name, email, phone number, vehicle( model, make) and/or visit date.

✅ Order all **users** by name or visit date.

✅ Create, edit and delete any **visits** on the app’s database.

✅ See a detailed report of a visit to the shop.

### The **REST API** of Smart Garage project supports the following functionalities:

✅ **REST API** of Smart Garage supports all the functionalities listed above for the MVC UI.

✅ Authentication in Rest api is created through **Jwt token**.

✅ Swagger: http://localhost:5000/swagger/index.html


## 4. Set Up project

- 1) Download source code from **master** branch, unrar in folder of your choice and open it with **Visual Studio**.
- 2) Open MicrosoftSQL Server Management Studio and ensure that there is no database with name **Smart Garage**, if you find one, delete it.
- 3) Open Package Manager Console and write **Update-database**.
- 4) Download **SQLQuerySmartGarageDatabase**, thеn create Query in **SmartGarage Database** and add the data from the Query. 
- 5) When you start up the project from Visual Studio, run in on **Kestrel**.
- 6) Use 
{
  "username": "ruslan@smartgarage.com",
  "password": "Ruslan26!",
  "role": "employee"
}
 in Swagger to generate your Jwt token.  

