﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartGarage.Services.EmailConfigurationModels
{
    public class WelcomeRequest
    {
        public string ToEmail { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }


    }
}
