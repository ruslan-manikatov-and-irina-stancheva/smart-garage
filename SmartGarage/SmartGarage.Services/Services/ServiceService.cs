﻿using AutoMapper;
using CustomExceptions.Exceptions;
using Microsoft.EntityFrameworkCore;
using SmartGarage.Data;
using SmartGarage.Data.Models;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Services.Services
{
    /// <summary>
    /// Class ServiceService.
    /// Implements the <see cref="SmartGarage.Services.Contracts.IServiceService" />
    /// </summary>
    /// <seealso cref="SmartGarage.Services.Contracts.IServiceService" />
    public class ServiceService : IServiceService
    {
        private readonly SmartGarageDbContext context;
        private readonly IMapper mapper;
        private readonly IServiceVehicleTypesService serviceVehicleTypesService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceService"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="mapper">The mapper.</param>
        /// <param name="serviceVehicleTypesService">The service vehicle types service.</param>
        public ServiceService(SmartGarageDbContext context, IMapper mapper, IServiceVehicleTypesService serviceVehicleTypesService)
        {
            this.context = context;
            this.mapper = mapper;
            this.serviceVehicleTypesService = serviceVehicleTypesService;
        }

        /// <summary>
        /// Get all garage services as an asynchronous operation.
        /// </summary>
        /// <returns>A Task&lt;ICollection`1&gt; representing the asynchronous operation.</returns>
        public async Task<ICollection<ServiceDTO>> GetAllAsync()
        {
            var allServices = await this.context.Services
                .Select(s => new ServiceDTO
                {
                    ServiceId = s.ServiceId,
                    Name = s.Name,
                    VehicleTypes = s.ServiceVehicleTypes.Where(sv => sv.ServiceId == s.ServiceId).Select(s => s.VehicleTypeId).ToList(),
                    Price = s.Price
                })
                .OrderBy(s => s.Name)
                .ToListAsync();

            return allServices;
        }

        /// <summary>
        /// Get garage services as an asynchronous operation by given id.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>A Task&lt;ServiceUpdateDTO&gt; representing the asynchronous operation.</returns>
        /// <exception cref="CustomExceptions.Exceptions.NotFoundException"></exception>
        public async Task<ServiceUpdateDTO> GetAsync(int id)
        {
            var serviceDto = await this.context.Services
                .Where(sv => sv.ServiceId == id)
                .Select(sv => new ServiceUpdateDTO
                {
                    ServiceId = id,
                    Name = sv.Name,
                    VehicleTypes = sv.ServiceVehicleTypes.Where(svt => svt.ServiceId == id).Select(svt => mapper.Map<VehicleTypeDTO>(svt.VehicleType)).ToList(),
                    Price = sv.Price
                })
                .FirstOrDefaultAsync()
                ?? throw new NotFoundException();

            return serviceDto;
        }

        /// <summary>
        /// Get service and type as an asynchronous operation by given id.
        /// </summary>
        /// <param name="id">service id.</param>
        /// <returns>A Task&lt;ICollection`1&gt; representing the asynchronous operation.</returns>
        public async Task<ICollection<ServiceVehicleTypeDTO>> GetServiceAndTypeAsync(int id)
        {
            var service = await this.context.ServiceVehicleTypes
                .Where(sv => sv.ServiceId == id)
                .Select(sv => new ServiceVehicleTypeDTO
                {
                    ServiceId = sv.ServiceId,
                    Name = sv.Service.Name,
                    VehicleType = sv.VehicleType.Name,
                    Price = sv.Service.Price * sv.VehicleType.Coefficient
                }).ToListAsync();

            return service;
        }

        /// <summary>
        /// Get by name and price as an asynchronous operation.
        /// </summary>
        /// <param name="name">service name.</param>
        /// <param name="price">service price.</param>
        /// <returns>A Task&lt;ICollection`1&gt; representing the asynchronous operation.</returns>
        public async Task<ICollection<ServiceDTO>> GetByNameAndPriceAsync(string name, decimal price)
        { 
            var service = await this.context.ServiceVehicleTypes
               .Where(sv => sv.Service.Name.Equals(name))
               .Select(sv => new ServiceDTO
               {
                   ServiceId = sv.ServiceId,
                   Name = sv.Service.Name,
                   Price = sv.Service.Price * sv.VehicleType.Coefficient
               })
               .Where(s => s.Price == price)
               .ToListAsync();

            return service;
        }

        /// <summary>
        /// Get by name as an asynchronous operation.
        /// </summary>
        /// <param name="name">service name.</param>
        /// <returns>A Task&lt;ICollection`1&gt; representing the asynchronous operation.</returns>
        public async Task<ICollection<ServiceDTO>> GetByNameAsync(string name)
        {
            var service = await this.context.ServiceVehicleTypes
                .Where(sv => sv.Service.Name.Equals(name))
                .Select(sv => new ServiceDTO
                {
                    ServiceId = sv.ServiceId,
                    Name = sv.Service.Name,
                    Price = sv.Service.Price * sv.VehicleType.Coefficient
                }).ToListAsync();

            return service;
        }

        /// <summary>
        /// Get by price as an asynchronous operation.
        /// </summary>
        /// <param name="price">service price.</param>
        /// <returns>A Task&lt;ICollection`1&gt; representing the asynchronous operation.</returns>
        public async Task<ICollection<ServiceDTO>> GetByPriceAsync(decimal price)
        {
            var services = await this.context.ServiceVehicleTypes
                .Select(sv => new ServiceDTO
                {
                    ServiceId = sv.ServiceId,
                    Name = sv.Service.Name,
                    Price = sv.Service.Price * sv.VehicleType.Coefficient
                })
                .Where(s => s.Price == price)
                .ToListAsync();

            return services;
        }

        /// <summary>
        /// Create service as an asynchronous operation.
        /// </summary>
        /// <param name="dto">create dto with elements from the user.</param>
        /// <returns>A Task&lt;ServiceCreateDTO&gt; representing the asynchronous operation.</returns>
        public async Task<ServiceCreateDTO> CreateAsync(ServiceCreateDTO dto)
        {
            var service = mapper.Map<Service>(dto);
            var vehicleTypes = dto.VehicleTypes;

            var ids = new List<int>();
            foreach (var type in vehicleTypes)
                ids.Add(type.VehicleTypeId);

            this.context.Services.Add(service);
            await this.context.SaveChangesAsync();

            if (vehicleTypes.Count() > 0)
                await this.serviceVehicleTypesService.CreateAsync(service.ServiceId, ids);

            return dto;
        }

        /// <summary>
        /// Create service an asynchronous operation.
        /// </summary>
        /// <param name="dto">The dto.</param>
        /// <param name="types">list of vehicle types.</param>
        /// <returns>A Task&lt;ServiceCreateDTO&gt; representing the asynchronous operation.</returns>
        public async Task<ServiceCreateDTO> CreateAsync(ServiceCreateDTO dto, List<int> types)
        {
            var service = mapper.Map<Service>(dto);
            var vehicleTypes = dto.VehicleTypes;

            this.context.Services.Add(service);
            await this.context.SaveChangesAsync();

            if (types.Count() > 0)
                await this.serviceVehicleTypesService.CreateAsync(service.ServiceId, types);

            return dto;
        }

        /// <summary>
        /// Update service an asynchronous operation.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="dto">The dto.</param>
        /// <returns>A Task&lt;ServiceUpdateDTO&gt; representing the asynchronous operation.</returns>
        /// <exception cref="CustomExceptions.Exceptions.NotFoundException"></exception>
        public async Task<ServiceUpdateDTO> UpdateAsync(int id, ServiceUpdateDTO dto)
        {
            var service = await this.context.Services
               .FirstOrDefaultAsync(s => s.ServiceId == id)
               ?? throw new NotFoundException();

            var name = dto.Name;
            var price = dto.Price;
            var vehicleTypes = dto.VehicleTypes;

            var ids = new List<int>();
            foreach (var type in vehicleTypes)
                ids.Add(type.VehicleTypeId);

            if (name != null)
                service.Name = name;
            if (price > 0)
                service.Price = price;
            if (vehicleTypes.Count() > 0)
            {
                if (service.ServiceVehicleTypes.Count() > 0)
                    await this.serviceVehicleTypesService.DeleteByServiceIdAsync(id);

                await this.serviceVehicleTypesService.CreateAsync(id, ids);
            }

            return dto;
        }

        /// <summary>
        /// Update service an asynchronous operation.
        /// </summary>
        /// <param name="id">The identifier of service.</param>
        /// <param name="dto">The dto for service update.</param>
        /// <param name="types">The list ot id for vehicle types.</param>
        /// <returns>A Task&lt;ServiceUpdateDTO&gt; representing the asynchronous operation.</returns>
        /// <exception cref="CustomExceptions.Exceptions.NotFoundException"></exception>
        public async Task<ServiceUpdateDTO> UpdateAsync(int id, ServiceUpdateDTO dto, List<int> types)
        {
            var service = await this.context.Services
               .FirstOrDefaultAsync(s => s.ServiceId == id)
               ?? throw new NotFoundException();

            var name = dto.Name;
            var price = dto.Price;

            if (name != null)
                service.Name = name;
            if (price > 0)
                service.Price = price;

            await this.serviceVehicleTypesService.DeleteByServiceIdAsync(id);

            if (types.Count > 0)
                await this.serviceVehicleTypesService.CreateAsync(id, types);

            return dto;
        }

        /// <summary>
        /// Delete service an asynchronous operation.
        /// </summary>
        /// <param name="id">The identifier of service.</param>
        /// <returns>Void.</returns>
        /// <exception cref="CustomExceptions.Exceptions.NotFoundException"></exception>
        public async Task DeleteAsync(int id)
        {
            var service = await this.context.Services
                .FirstOrDefaultAsync(s => s.ServiceId == id)
               ?? throw new NotFoundException();

            await this.serviceVehicleTypesService.DeleteByServiceIdAsync(id);

            this.context.Services.Remove(service);

            await this.context.SaveChangesAsync();
        }
    }
}
