﻿using Microsoft.EntityFrameworkCore;
using SmartGarage.Data;
using SmartGarage.Data.Models;
using SmartGarage.Services.Contracts;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Services.Services
{
    /// <summary>
    /// Class ServiceVehicleTypesService.
    /// Implements the <see cref="SmartGarage.Services.Contracts.IServiceVehicleTypesService" />
    /// </summary>
    /// <seealso cref="SmartGarage.Services.Contracts.IServiceVehicleTypesService" />
    public class ServiceVehicleTypesService : IServiceVehicleTypesService
    {
        private readonly SmartGarageDbContext context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceVehicleTypesService"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public ServiceVehicleTypesService(SmartGarageDbContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Gets all ServiceVehicleTypes
        /// </summary>
        /// <returns>IQueryable&lt;ServiceVehicleTypes&gt;.</returns>
        public IQueryable<ServiceVehicleTypes> GetAll()
        {
            var allServiceVehicleTypes = this.context.ServiceVehicleTypes
                .AsQueryable();

            return allServiceVehicleTypes;
        }

        /// <summary>
        /// Gets ServiceVehicleTypes specified service identifier.
        /// </summary>
        /// <param name="serviceId">The service identifier.</param>
        /// <returns>IQueryable&lt;ServiceVehicleTypes&gt;.</returns>
        public IQueryable<ServiceVehicleTypes> Get(int serviceId)
        {
            var serviceVehicleTypes = this.context.ServiceVehicleTypes
                .Where(sv => sv.ServiceId == serviceId)
                .AsQueryable();

            return serviceVehicleTypes;
        }

        /// <summary>
        /// Create service vehicle type an asynchronous operation, and adds conection to vehicle types
        /// </summary>
        /// <param name="serviceId">The service identifier.</param>
        /// <param name="vehicleTypes">List of int, the id of vehicle types.</param>
        /// <returns>A Task representing the asynchronous operation.</returns>
        public async Task CreateAsync(int serviceId, List<int> vehicleTypes)
        {
            ICollection<ServiceVehicleTypes> serviceVehicleTypes = new Collection<ServiceVehicleTypes>();

            foreach (var id in vehicleTypes)
            {
                serviceVehicleTypes.Add(new ServiceVehicleTypes
                {
                    ServiceId = serviceId,
                    VehicleTypeId = id
                });
            }

            this.context.ServiceVehicleTypes.AddRange(serviceVehicleTypes);

            await this.context.SaveChangesAsync();
        }

        /// <summary>
        /// Delete by service identifier as an asynchronous operation.
        /// </summary>
        /// <param name="serviceId">The service identifier.</param>
        /// <returns>A Task representing the asynchronous operation.</returns>
        public async Task DeleteByServiceIdAsync(int serviceId)
        {
            var serviceVehicleTypesToRemove = await this.context.ServiceVehicleTypes
               .Where(s => s.ServiceId == serviceId)
               .ToListAsync();

            if (serviceVehicleTypesToRemove.Count > 0)
            {
                this.context.ServiceVehicleTypes.RemoveRange(serviceVehicleTypesToRemove);

                await this.context.SaveChangesAsync();
            }
        }

        /// <summary>
        /// Delete by vehicle type identifier as an asynchronous operation.
        /// </summary>
        /// <param name="vehicleTypeId">The vehicle type identifier.</param>
        /// <returns>A Task representing the asynchronous operation.</returns>
        public async Task DeleteByVehicleTypeIdAsync(int vehicleTypeId)
        {
            var serviceVehicleTypesToRemove = await this.context.ServiceVehicleTypes
               .Where(s => s.VehicleTypeId == vehicleTypeId)
               .ToListAsync();

            if (serviceVehicleTypesToRemove.Count > 0)
            {
                this.context.ServiceVehicleTypes.RemoveRange(serviceVehicleTypesToRemove);

                await this.context.SaveChangesAsync();
            }
        }
    }
}
