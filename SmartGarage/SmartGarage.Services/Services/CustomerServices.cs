﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SmartGarage.Data;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTO;

namespace SmartGarage.Services.Services
{
    /// <summary>
    /// Class CustomerServices.
    /// Implements the <see cref="SmartGarage.Services.Contracts.ICustomerServices" />
    /// </summary>
    /// <seealso cref="SmartGarage.Services.Contracts.ICustomerServices" />
    public class CustomerServices : ICustomerServices
    {
        private readonly SmartGarageDbContext context;
        private readonly ICurrencyService CurrencyService;

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerServices"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="currencyService">The currency service.</param>
        public CustomerServices(SmartGarageDbContext context, ICurrencyService currencyService)
        {
            this.context = context;
            this.CurrencyService = currencyService;
        }
        /// <summary>
        /// Gets all vehicles.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <returns>Customer dto with all vehicles for a customer</returns>
        public async Task<ICollection<CustomerVehiclesDTO>> GetAllVehicles(string username)
        {
            var vehiclesDTO = await this.context.Vehicles
                .Where(v => v.Owner.UserName == username)
                .Select(x => new CustomerVehiclesDTO
                {
                    VehicleId = x.VehicleId,
                    RegistrationPlate = x.RegistrationPlate,
                    Vin = x.Vin,
                    VehicleYear = x.Year,
                    ModelName = x.Model.Name,
                    ModelManufacturerName = x.Model.Manufacturer.Name
                }).ToListAsync();

            return vehiclesDTO;
        }

        /// <summary>
        /// Gets all services.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <returns>Customer dto with all services for a customer</returns>
        /// <exception cref="System.ArgumentNullException"></exception>
        public async Task<ICollection<CustomerGetAllServicesDTO>> GetAllServices(string username)
        {

            var visitServices = await this.context.Visits
                .Where(v => v.Vehicle.Owner.UserName == username)
                .Select(v => new CustomerGetAllServicesDTO
                {
                    VisitId = v.VisitId,
                    ArriveDate = v.ArriveDate,
                    VisitStatusName = v.VisitStatus.Name,
                    VehicleModelName = v.Vehicle.Model.Name,
                    VehicleVin = v.Vehicle.Vin,
                    VehicleRegistrationPlate = v.Vehicle.RegistrationPlate,
                    ServicesId = v.VisitServices.Select(vs => vs.Service.ServiceId).ToList(),
                    ServiceName = v.VisitServices.Select(vs => vs.Service.Name).ToList(),
                    ServicePrice = v.VisitServices.Select(vs => vs.Service.Price).ToList()
                }).ToListAsync() ?? throw new ArgumentNullException();

            return visitServices;
        }

        
        /// <summary>
        /// Filters the services of customer by reg plate.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="registrationPlate">The registration plate.</param>
        /// <returns>ICollection&lt;CustomerGetAllServicesDTO&gt;.</returns>
        /// <exception cref="System.ArgumentNullException"></exception>
        public async Task<ICollection<CustomerGetAllServicesDTO>> FilterServicesByRegPlate(string username, string registrationPlate)
        {
            var visits = await this.context.Visits
                .Where(v => v.Vehicle.Owner.UserName == username && (v.Vehicle.RegistrationPlate == registrationPlate))
                .Select(v => new CustomerGetAllServicesDTO
                {
                    VisitId = v.VisitId,
                    ArriveDate = v.ArriveDate,
                    VisitStatusName = v.VisitStatus.Name,
                    VehicleModelName = v.Vehicle.Model.Name,
                    VehicleVin = v.Vehicle.Vin,
                    VehicleRegistrationPlate = v.Vehicle.RegistrationPlate,
                    ServicesId = v.VisitServices.Select(vs => vs.Service.ServiceId).ToList(),
                    ServiceName = v.VisitServices.Select(vs => vs.Service.Name).ToList(),
                    ServicePrice = v.VisitServices.Select(vs => vs.Service.Price).ToList()
                }).ToListAsync() ?? throw new ArgumentNullException();

            return visits;
        }

        /// <summary>
        /// Filters the services of customer by date.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="date">The date.</param>
        /// <returns>Task&lt;ICollection&lt;CustomerGetAllServicesDTO&gt;&gt;.</returns>
        /// <exception cref="System.ArgumentNullException"></exception>
        public async Task<ICollection<CustomerGetAllServicesDTO>> FilterServicesByDate(string username, DateTime? date)
        {

            var visits = await this.context.Visits
                .Where(v => v.Vehicle.Owner.UserName == username && (v.ArriveDate == date))
                .Select(v => new CustomerGetAllServicesDTO
                {
                    VisitId = v.VisitId,
                    ArriveDate = v.ArriveDate,
                    VisitStatusName = v.VisitStatus.Name,
                    VehicleModelName = v.Vehicle.Model.Name,
                    VehicleVin = v.Vehicle.Vin,
                    VehicleRegistrationPlate = v.Vehicle.RegistrationPlate,
                    ServicesId = v.VisitServices.Select(vs => vs.Service.ServiceId).ToList(),
                    ServiceName = v.VisitServices.Select(vs => vs.Service.Name).ToList(),
                    ServicePrice = v.VisitServices.Select(vs => vs.Service.Price).ToList()
                }).ToListAsync() ?? throw new ArgumentNullException();

            return visits;
        }

        /// <summary>
        /// Filters the services of customer by reg plate and date.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="regPlate">The reg plate.</param>
        /// <param name="date">The date.</param>
        /// <returns>ICollection&lt;CustomerGetAllServicesDTO&gt;.</returns>
        /// <exception cref="System.ArgumentNullException"></exception>
        public async Task<ICollection<CustomerGetAllServicesDTO>> FilterServicesByRegPlateAndDate(string username, string regPlate, DateTime? date)
        {

            var visits = await this.context.Visits
               .Where(v => v.Vehicle.Owner.UserName == username && (v.ArriveDate == date) && (v.Vehicle.RegistrationPlate == regPlate))
               .Select(v => new CustomerGetAllServicesDTO
               {
                   VisitId = v.VisitId,
                   ArriveDate = v.ArriveDate,
                   VisitStatusName = v.VisitStatus.Name,
                   VehicleModelName = v.Vehicle.Model.Name,
                   VehicleVin = v.Vehicle.Vin,
                   VehicleRegistrationPlate = v.Vehicle.RegistrationPlate,
                   ServicesId = v.VisitServices.Select(vs => vs.Service.ServiceId).ToList(),
                   ServiceName = v.VisitServices.Select(vs => vs.Service.Name).ToList(),
                   ServicePrice = v.VisitServices.Select(vs => vs.Service.Price).ToList()
               }).ToListAsync() ?? throw new ArgumentNullException();

            return visits;
        }
        /// <summary>
        /// Gets the visit report of customer by identifier.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="visitId">The visit identifier.</param>
        /// <returns>Task&lt;CustomerGetVisitDTO&gt;.</returns>
        /// <exception cref="System.ArgumentNullException"></exception>
        public async Task<CustomerGetVisitDTO> GetVisitReportById(string username, int? visitId)
        {
            var visit = await this.context.Visits?
                .Include(v => v.VisitStatus)
                .Include(v => v.VisitServices)
                .ThenInclude(vs => vs.Service)
                .Include(v => v.Vehicle)
                .ThenInclude(v => v.Owner)
                .Include(v => v.Vehicle)
                .ThenInclude(v => v.VehicleType)
                .Include(v => v.Vehicle)
                .ThenInclude(v => v.Model)
                .FirstOrDefaultAsync(v => v.VisitId == visitId && v.Vehicle.Owner.UserName == username)
                ?? throw new ArgumentNullException();

            var serviceDto = new List<ServiceDTO>();

            var coeficienOfPrice = visit.Vehicle.VehicleType.Coefficient;

            decimal fullPriceOfVisit = 0;
            foreach (var item in visit.VisitServices)
            {
                var dto = new ServiceDTO();
                dto.Name = item.Service.Name;
                dto.Price = item.Service.Price * coeficienOfPrice;
                fullPriceOfVisit += dto.Price;

                serviceDto.Add(dto);
            }

            var customerVisitDTO = new CustomerGetVisitDTO
                (
                visit.Vehicle.Owner.Id,
                visit.Vehicle.Owner.FirstName,
                visit.Vehicle.Owner.LastName,
                visit.Vehicle.Owner.Email,

                visit.Vehicle.Model.Name,
                visit.Vehicle.Vin,
                visit.Vehicle.VehicleType.Name,

                visit.VisitId,
                visit.ArriveDate,
                visit.VisitStatus.Name,
                visit.Vehicle.VehicleType.Coefficient,
                serviceDto,
                fullPriceOfVisit

                );

            return customerVisitDTO;
        }

        /// <summary>
        /// Gets the visit report by identifier and currency, of customer.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="visitId">The visit identifier.</param>
        /// <param name="currency">The currency.</param>
        /// <returns>Task&lt;CustomerGetVisitDTO&gt;.</returns>
        /// <exception cref="System.ArgumentNullException"></exception>
        public async Task<CustomerGetVisitDTO> GetVisitReportByIdAndCurrency(string username, int? visitId, string currency)
        {
            var visit = await this.context.Visits?
                .Include(v => v.VisitStatus)
                .Include(v => v.VisitServices)
                    .ThenInclude(vs => vs.Service)
                .Include(v => v.Vehicle)
                    .ThenInclude(v => v.Owner)
                .Include(v => v.Vehicle)
                    .ThenInclude(v => v.VehicleType)
                .Include(v => v.Vehicle)
                    .ThenInclude(v => v.Model)
                .FirstOrDefaultAsync(v => v.VisitId == visitId && v.Vehicle.Owner.UserName == username)
                ?? throw new ArgumentNullException();

            var serviceDto = new List<ServiceDTO>();

            var coeficienOfPrice = visit.Vehicle.VehicleType.Coefficient;

            decimal fullPriceOfVisit = 0;
            foreach (var item in visit.VisitServices)
            {
                var dto = new ServiceDTO();
                dto.Name = item.Service.Name;
                dto.Price = item.Service.Price * coeficienOfPrice;
                serviceDto.Add(this.CurrencyService.ConvertCurrency(currency, dto).Result);
                fullPriceOfVisit += dto.Price;
            }

            var customerVisitDTO = new CustomerGetVisitDTO
                (
                visit.Vehicle.Owner.Id,
                visit.Vehicle.Owner.FirstName,
                visit.Vehicle.Owner.LastName,
                visit.Vehicle.Owner.Email,

                visit.Vehicle.Model.Name,
                visit.Vehicle.Vin,
                visit.Vehicle.VehicleType.Name,

                visit.VisitId,
                visit.ArriveDate,
                visit.VisitStatus.Name,
                visit.Vehicle.VehicleType.Coefficient,
                serviceDto,
                fullPriceOfVisit,
                currency
                );

            return customerVisitDTO;
        }

        /// <summary>
        /// Gets all visits report of customer.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="currency">The currency.</param>
        /// <returns>Task&lt;CustomerGetAllVisitsDTO&gt;.</returns>
        /// <exception cref="System.ArgumentNullException"></exception>
        public async Task<CustomerGetAllVisitsDTO> GetAllVisitsReport(string username, string currency)
        {
            if(currency == null)
            {
                currency = "EUR";
            }

            var customerVisits = await this.context.Visits?
                .Include(v => v.VisitStatus)
                .Include(v => v.VisitServices)
                .ThenInclude(vs => vs.Service)
                .Include(v => v.Vehicle)
                .ThenInclude(v => v.Owner)
                .Include(v => v.Vehicle)
                .ThenInclude(v => v.VehicleType)
                .Include(v => v.Vehicle)
                .ThenInclude(v => v.Model)
                .Where(v => v.Vehicle.Owner.UserName == username)
                .ToListAsync()
                ?? throw new ArgumentNullException();

            var getAllVisitsDto = new CustomerGetAllVisitsDTO();
            getAllVisitsDto.Currency = currency;
            

            foreach (var visit in customerVisits)
            {
                var customerOneVisit = new CustomerGetVisitDTO();

                customerOneVisit.CustomerId = visit.Vehicle.Owner.Id;
                customerOneVisit.FirstName = visit.Vehicle.Owner.FirstName;
                customerOneVisit.LastName = visit.Vehicle.Owner.LastName;
                customerOneVisit.Email = visit.Vehicle.Owner.Email;

                customerOneVisit.VehicleModelName = visit.Vehicle.Model.Name;
                customerOneVisit.VehicleVin = visit.Vehicle.Vin;
                customerOneVisit.vehicleType = visit.Vehicle.VehicleType.Name;

                customerOneVisit.VisitId = visit.VisitId;
                customerOneVisit.ArriveDate = visit.ArriveDate;
                customerOneVisit.VisitStatusName = visit.VisitStatus.Name;
                customerOneVisit.VisitCurrencyPrice = currency;
                customerOneVisit.VehicleTypeCoefficient = visit.Vehicle.VehicleType.Coefficient;
                

                foreach (var item in visit.VisitServices)
                {
                    var oneService = new ServiceDTO();
                    oneService.Name = item.Service.Name;
                    oneService.Price = item.Service.Price * customerOneVisit.VehicleTypeCoefficient;

                    if (!currency.Contains("EUR"))
                    {
                        var currencyResult = this.CurrencyService.ConvertCurrency(currency, oneService).Result;
                        customerOneVisit.ServiceDTOs.Add(currencyResult);
                        customerOneVisit.VisitCost += currencyResult.Price;
                        getAllVisitsDto.FullPrice += currencyResult.Price;
                        oneService.Currency = currency;
                    }
                    else
                    {
                        customerOneVisit.ServiceDTOs.Add(oneService);
                        customerOneVisit.VisitCost += oneService.Price;
                        getAllVisitsDto.FullPrice += oneService.Price;
                        oneService.Currency = currency;
                    }

                }
                getAllVisitsDto.CustomerGetVisitDTOs.Add(customerOneVisit);
            }

            return getAllVisitsDto;
        }
    }
}

    






