﻿using System;
using System.Collections.Generic;
using System.Text;
using Syncfusion.Pdf;
using Syncfusion.Pdf.Graphics;
using Syncfusion.Drawing;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using SmartGarage.Services.Contracts;
using Syncfusion.Pdf.Grid;
using SmartGarage.Services.DTO;
using AutoMapper;

namespace SmartGarage.Services.Services
{
    /// <summary>
    /// Class PdfGeneratorService.
    /// Implements the <see cref="SmartGarage.Services.Contracts.IPdfGeneratorService" />
    /// </summary>
    /// <seealso cref="SmartGarage.Services.Contracts.IPdfGeneratorService" />
    public class PdfGeneratorService : IPdfGeneratorService
    {
        private readonly IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="PdfGeneratorService"/> class.
        /// </summary>
        /// <param name="mapper">The mapper.</param>
        public PdfGeneratorService(IMapper mapper)
        {
            this.mapper = mapper;
        }


        /// <summary>
        /// Generates PDF for one visit, to generate pdf we are using Syncfusion nuget,
        /// it gaves us the ability to write throu methods in the pdf file, to create new
        /// page and manage the proces of the pdf view.
        /// </summary>
        /// <param name="customerDTO">The customer dto.</param>
        /// <returns>FileStreamResult.</returns>
        public FileStreamResult GeneratePdfForOneVisit(CustomerGetVisitDTO customerDTO)
        {
            //prepare data for DTO creation
            var pdfVisitDto = mapper.Map<ICollection<PDFVisitDto>>(customerDTO.ServiceDTOs);

            
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Car model: " + customerDTO.VehicleModelName);
            sb.AppendLine("VIN: " + customerDTO.VehicleVin);
            sb.AppendLine("Arrival date: " + customerDTO.ArriveDate);

            //Create a new PDF document
            PdfDocument document = new PdfDocument();
            //Add a page to the document
            PdfPage page = document.Pages.Add();
            //Create PDF graphics for the page
            PdfGraphics graphics = page.Graphics;
            //Set the standard font
            PdfFont font = new PdfStandardFont(PdfFontFamily.Helvetica, 20);

            //Put The logo of Smart Garage
            FileStream imageStream = new FileStream("C:\\Users\\x\\Desktop\\SmartGarage\\smart-garage\\SmartGarage\\SmartGarage.Web\\wwwroot\\Images\\Smart-Garage-Logo.png", FileMode.Open, FileAccess.Read);
            RectangleF bounds = new RectangleF(40, 0, 100, 60);
            PdfImage image = PdfImage.FromStream(imageStream);
            //Draws the image to the PDF page
            page.Graphics.DrawImage(image, bounds);

            PdfBrush solidBrush = new PdfSolidBrush(new PdfColor(126, 151, 173));
            bounds = new RectangleF(0, bounds.Bottom + 90, graphics.ClientSize.Width, 30);
            //Draws a rectangle to place the heading in that region.
            graphics.DrawRectangle(solidBrush, bounds);
            //Creates a font for adding the heading in the page
            PdfFont subHeadingFont = new PdfStandardFont(PdfFontFamily.TimesRoman, 14);
            //Creates a text element to add the invoice number
            PdfTextElement element = new PdfTextElement("INVOICE for " + customerDTO.FirstName + " " + customerDTO.LastName, subHeadingFont);
            element.Brush = PdfBrushes.White;

            //Draws the heading on the page
            PdfLayoutResult result = element.Draw(page, new PointF(10, bounds.Top + 8));
            string currentDate = "DATE " + DateTime.Now.ToString("MM/dd/yyyy");
            //Measures the width of the text to place it in the correct location
            SizeF textSize = subHeadingFont.MeasureString(currentDate);
            PointF textPosition = new PointF(graphics.ClientSize.Width - textSize.Width - 10, result.Bounds.Y);
            //Draws the date by using DrawString method
            graphics.DrawString(currentDate, subHeadingFont, element.Brush, textPosition);
            PdfFont timesRoman = new PdfStandardFont(PdfFontFamily.TimesRoman, 10);
            //Creates text elements to add the address and draw it to the page.
            element = new PdfTextElement(sb.ToString(), timesRoman);// Add the information from the customerDto
            element.Brush = new PdfSolidBrush(new PdfColor(126, 155, 203));
            result = element.Draw(page, new PointF(10, result.Bounds.Bottom + 25));
            PdfPen linePen = new PdfPen(new PdfColor(126, 151, 173), 0.70f);
            PointF startPoint = new PointF(0, result.Bounds.Bottom + 3);
            PointF endPoint = new PointF(graphics.ClientSize.Width, result.Bounds.Bottom + 3);
            //Draws a line at the bottom of the address
            graphics.DrawLine(linePen, startPoint, endPoint);
                 
            element = new PdfTextElement("Full Price of the visit: " + customerDTO.VisitCost + " " + customerDTO.VisitCurrencyPrice, timesRoman);// Add the information from the customerDto
            element.Brush = new PdfSolidBrush(new PdfColor(226, 50, 53));
            result = element.Draw(page, new PointF(10, result.Bounds.Bottom + 25));
            //Draws a line at the bottom of the address
            graphics.DrawLine(linePen, startPoint, endPoint);

            //Creates a PDF grid
            PdfGrid grid = new PdfGrid();
            //Adds the data source
            grid.DataSource = pdfVisitDto; //we gave the dto to create a table in pdf
            //Creates the grid cell styles
            PdfGridCellStyle cellStyle = new PdfGridCellStyle();
            cellStyle.Borders.All = PdfPens.White;
            PdfGridRow header = grid.Headers[0];
            //Creates the header style
            PdfGridCellStyle headerStyle = new PdfGridCellStyle();
            headerStyle.Borders.All = new PdfPen(new PdfColor(126, 151, 173));
            headerStyle.BackgroundBrush = new PdfSolidBrush(new PdfColor(126, 151, 173));
            headerStyle.TextBrush = PdfBrushes.White;
            headerStyle.Font = new PdfStandardFont(PdfFontFamily.TimesRoman, 14f, PdfFontStyle.Regular);
            //Adds cell customizations
            for (int i = 0; i < header.Cells.Count; i++)
            {
                if (i == 0 || i == 1)
                    header.Cells[i].StringFormat = new PdfStringFormat(PdfTextAlignment.Left, PdfVerticalAlignment.Middle);
                else
                    header.Cells[i].StringFormat = new PdfStringFormat(PdfTextAlignment.Right, PdfVerticalAlignment.Middle);
            }

            //Applies the header style
            header.ApplyStyle(headerStyle);
            cellStyle.Borders.Bottom = new PdfPen(new PdfColor(217, 217, 217), 0.70f);
            cellStyle.Font = new PdfStandardFont(PdfFontFamily.TimesRoman, 12f);
            cellStyle.TextBrush = new PdfSolidBrush(new PdfColor(131, 130, 136));
            //Creates the layout format for grid
            PdfGridLayoutFormat layoutFormat = new PdfGridLayoutFormat();
            // Creates layout format settings to allow the table pagination
            layoutFormat.Layout = PdfLayoutType.Paginate;
            //Draws the grid to the PDF page.
            PdfGridLayoutResult gridResult = grid.Draw(page, new RectangleF(new PointF(0, result.Bounds.Bottom + 40), new SizeF(graphics.ClientSize.Width, graphics.ClientSize.Height - 100)), layoutFormat);

            //Saving the PDF to the MemoryStream
            MemoryStream stream = new MemoryStream();
            document.Save(stream);
            //Set the position as '0'.
            stream.Position = 0;
            //Download the PDF document in the browser
            FileStreamResult fileStreamResult = new FileStreamResult(stream, "application/pdf");
            fileStreamResult.FileDownloadName = "Sample.pdf";
            return fileStreamResult;
        }







        /// <summary>
        /// Generates PDF for all visits of customer, to generate pdf we are using Syncfusion nuget,
        /// it gaves us the ability to write throu methods in the pdf file, to create new
        /// page and manage the proces of the pdf view.
        /// </summary>
        /// <param name="customerDTO">The customer dto.</param>
        /// <returns>FileStreamResult.</returns>
        public FileStreamResult GeneratePdfForAllVisitsOfCustomer(CustomerGetAllVisitsDTO customerDTO)
        {
            //Create a new PDF document
            PdfDocument document = new PdfDocument();
            
            foreach (var item in customerDTO.CustomerGetVisitDTOs)
            {

                var pdfVisitDto = mapper.Map<ICollection<PDFVisitDto>>(item.ServiceDTOs);


                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Car model: " + item.VehicleModelName);
                sb.AppendLine("VIN: " + item.VehicleVin);

                //Add a page to the document
                PdfPage page = document.Pages.Add();
                //Create PDF graphics for the page
                PdfGraphics graphics = page.Graphics;
                //Set the standard font
                PdfFont font = new PdfStandardFont(PdfFontFamily.Helvetica, 20);
                //Put The logo of Smart Garage
                RectangleF bounds = new RectangleF(80, 0, 100, 60);
                PdfBrush solidBrush = new PdfSolidBrush(new PdfColor(126, 151, 173));
                bounds = new RectangleF(0, bounds.Bottom + 90, graphics.ClientSize.Width, 30);
                //Draws a rectangle to place the heading in that region.
                graphics.DrawRectangle(solidBrush, bounds);
                //Creates a font for adding the heading in the page
                PdfFont subHeadingFont = new PdfStandardFont(PdfFontFamily.TimesRoman, 14);
                //Creates a text element to add the invoice number
                PdfTextElement element = new PdfTextElement("INVOICE for " + item.FirstName + " " + item.FirstName, subHeadingFont);
                element.Brush = PdfBrushes.White;
                //Draws the heading on the page
                PdfLayoutResult result = element.Draw(page, new PointF(10, bounds.Top + 8));
                string currentDate = "DATE " + item.ArriveDate.Date;
                //Measures the width of the text to place it in the correct location
                SizeF textSize = subHeadingFont.MeasureString(currentDate);
                PointF textPosition = new PointF(graphics.ClientSize.Width - textSize.Width - 10, result.Bounds.Y);
                //Draws the date by using DrawString method
                graphics.DrawString(currentDate, subHeadingFont, element.Brush, textPosition);
                PdfFont timesRoman = new PdfStandardFont(PdfFontFamily.TimesRoman, 10);
                //Creates text elements to add the address and draw it to the page.
                element = new PdfTextElement(sb.ToString(), timesRoman);// Add the information from the customerDto
                element.Brush = new PdfSolidBrush(new PdfColor(126, 155, 203));
                result = element.Draw(page, new PointF(10, result.Bounds.Bottom + 25));
                PdfPen linePen = new PdfPen(new PdfColor(126, 151, 173), 0.70f);
                PointF startPoint = new PointF(0, result.Bounds.Bottom + 3);
                PointF endPoint = new PointF(graphics.ClientSize.Width, result.Bounds.Bottom + 3);
                //Draws a line at the bottom of the address
                graphics.DrawLine(linePen, startPoint, endPoint);

                element = new PdfTextElement("Full Price of the visit: " + item.VisitCost + " " + item.VisitCurrencyPrice, timesRoman);// Add the information from the customerDto
                element.Brush = new PdfSolidBrush(new PdfColor(226, 50, 53));
                result = element.Draw(page, new PointF(10, result.Bounds.Bottom + 25));
                //Draws a line at the bottom of the address
                graphics.DrawLine(linePen, startPoint, endPoint);

                //Creates a PDF grid
                PdfGrid grid = new PdfGrid();
                //Adds the data source
                grid.DataSource = pdfVisitDto; //we gave the dto to create a table in pdf
                                               //Creates the grid cell styles
                PdfGridCellStyle cellStyle = new PdfGridCellStyle();
                cellStyle.Borders.All = PdfPens.White;
                PdfGridRow header = grid.Headers[0];
                //Creates the header style
                PdfGridCellStyle headerStyle = new PdfGridCellStyle();
                headerStyle.Borders.All = new PdfPen(new PdfColor(126, 151, 173));
                headerStyle.BackgroundBrush = new PdfSolidBrush(new PdfColor(126, 151, 173));
                headerStyle.TextBrush = PdfBrushes.White;
                headerStyle.Font = new PdfStandardFont(PdfFontFamily.TimesRoman, 14f, PdfFontStyle.Regular);
                //Adds cell customizations
                for (int i = 0; i < header.Cells.Count; i++)
                {
                    if (i == 0 || i == 1)
                        header.Cells[i].StringFormat = new PdfStringFormat(PdfTextAlignment.Left, PdfVerticalAlignment.Middle);
                    else
                        header.Cells[i].StringFormat = new PdfStringFormat(PdfTextAlignment.Right, PdfVerticalAlignment.Middle);
                }

                //Applies the header style
                header.ApplyStyle(headerStyle);
                cellStyle.Borders.Bottom = new PdfPen(new PdfColor(217, 217, 217), 0.70f);
                cellStyle.Font = new PdfStandardFont(PdfFontFamily.TimesRoman, 12f);
                cellStyle.TextBrush = new PdfSolidBrush(new PdfColor(131, 130, 136));
                //Creates the layout format for grid
                PdfGridLayoutFormat layoutFormat = new PdfGridLayoutFormat();
                // Creates layout format settings to allow the table pagination
                layoutFormat.Layout = PdfLayoutType.Paginate;
                //Draws the grid to the PDF page.
                PdfGridLayoutResult gridResult = grid.Draw(page, new RectangleF(new PointF(0, result.Bounds.Bottom + 40), new SizeF(graphics.ClientSize.Width, graphics.ClientSize.Height - 100)), layoutFormat);

            }

            //Saving the PDF to the MemoryStream
            MemoryStream stream = new MemoryStream();
            document.Save(stream);
            //Set the position as '0'.
            stream.Position = 0;
            //Download the PDF document in the browser
            FileStreamResult fileStreamResult = new FileStreamResult(stream, "application/pdf");
            fileStreamResult.FileDownloadName = "Sample.pdf";
            return fileStreamResult;
        }
    }

    
}
