﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using SmartGarage.Data;
using SmartGarage.Data.Models;
using SmartGarage.Services.AuthenticateModels;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.EmailConfigurationModels;
using SmartGarage.Services.DTO;

namespace SmartGarage.Services.Services
{
    /// <summary>
    /// Class AccountService.
    /// Implements the <see cref="SmartGarage.Services.Contracts.IAccountService" />
    /// </summary>
    /// <seealso cref="SmartGarage.Services.Contracts.IAccountService" />
    public class AccountService : IAccountService
    {
        private readonly SmartGarageDbContext context;
        private readonly IUserService userService;
        private readonly IEmailService emailService;
        private readonly UserManager<User> userManager;
        private readonly AppSettings appSettings;


        /// <summary>
        /// Initializes a new instance of the <see cref="AccountService"/> class.
        /// </summary>
        /// <param name="userManager">The user manager.</param>
        /// <param name="appSettings">The application settings.</param>
        /// <param name="context">The context.</param>
        /// <param name="userService">The user service.</param>
        /// <param name="emailService">The email service.</param>
        public AccountService(UserManager<User> userManager, IOptions<AppSettings> appSettings, 
                              SmartGarageDbContext context, IUserService userService, IEmailService emailService)
        {
            this.userManager = userManager;
            this.appSettings = appSettings.Value;
            this.context = context;
            this.userService = userService;
            this.emailService = emailService;
        }

        /// <summary>
        /// Authenticate as an asynchronous operation.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns>A Task;AuthenticateResponse; representing the asynchronous operation.</returns>
        public async Task<AuthenticateResponse> AuthenticateAsync(AuthenticateRequest model)
        {
            var user = await context.Users.SingleOrDefaultAsync(u => u.UserName == model.Username);
            bool isCorrectPass = await userManager.CheckPasswordAsync(user, model.Password);                  
            
            //return null if user not faund or pass not correct
            if (user == null || !isCorrectPass)
            {
                return null;
            }
            var usersRoleId = await context.UserRoles.FirstOrDefaultAsync(u => u.UserId == user.Id);
            var role = await context.Roles.FirstOrDefaultAsync(r => r.Id == usersRoleId.RoleId);
            
            if(role.Name != model.Role)
            {
                return null;
            }

            var jwtToken = GenerateJwtToken(user, role);
            return new AuthenticateResponse(user, jwtToken);
        }

        /// <summary>
        /// Generates the JWT token.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="role">The role.</param>
        /// <returns>A JWT authentication token for the rest api validation.</returns>
        private string GenerateJwtToken(User user, Role role)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
            {
                    new Claim(ClaimTypes.Name, user.UserName),
                    new Claim(ClaimTypes.Role, role.Name)
                    
            }),
                Expires = DateTime.UtcNow.AddMinutes(30),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        /// <summary>
        /// Sends the forgotten password email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <returns>string message for the user.</returns>
        public async Task<string> SendForgottenPasswordEmail(string email)
        {
            var returnMessage = "An email has been sent.";

            var user = await this.context.Users
                .SingleOrDefaultAsync(u => u.Email == email);

            if (user == null)
            {
                return returnMessage;
            }
            int randomStringthLength = 15;
            var randomToken = this.userService.GetRandomAlphanumericString(randomStringthLength);

            user.ForgottenPasswordToken = randomToken;
            user.ForgottenTokenCreation = DateTime.Now;
            this.context.SaveChanges();

            var mailRequest = new MailRequest()
            {
                ToEmail = user.Email,
                Subject = "Forgotten Password",
                Body = "You have 1 hour to use this code to change password: " + randomToken
            };
            var isSended = this.emailService.SendEmailAsync(mailRequest);

            return returnMessage;
        }

        /// <summary>
        /// Updates the forgotten password.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns>true if update password</returns>
        /// <exception cref="System.ArgumentNullException"></exception>
        /// <exception cref="System.ArgumentException"></exception>
        /// <exception cref="System.ArgumentOutOfRangeException"></exception>
        public async Task<bool> UpdateForgottenPassword(ForgottenPasswordDataDTO model)
        {

            var user = await this.context.Users
                .SingleOrDefaultAsync(u => u.Email == model.Email)
                ?? throw new ArgumentNullException();

            if (user.ForgottenPasswordToken != model.ValidationCode)
            {
                throw new ArgumentException();
            }

            var timeSpam = DateTime.Now - user.ForgottenTokenCreation;
            var allowableТime = 120;

            if (timeSpam.TotalMinutes > allowableТime)
            {
                throw new ArgumentOutOfRangeException();
            }

            var passHasher = new PasswordHasher<User>();
            user.PasswordHash = passHasher.HashPassword(user, model.NewPassword);

            return true;
            //else take the update method of the passward => chek the difference between the microsoft- identity and my method!

        }

    }
}
