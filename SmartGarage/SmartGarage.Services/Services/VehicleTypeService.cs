﻿using AutoMapper;
using CustomExceptions.Exceptions;
using Microsoft.EntityFrameworkCore;
using SmartGarage.Data;
using SmartGarage.Data.Models;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Services.Services
{
    /// <summary>
    /// Class VehicleTypeService.
    /// Implements the <see cref="SmartGarage.Services.Contracts.IVehicleTypeService" />
    /// </summary>
    /// <seealso cref="SmartGarage.Services.Contracts.IVehicleTypeService" />
    public class VehicleTypeService : IVehicleTypeService
    {
        private readonly SmartGarageDbContext context;
        private readonly IMapper mapper;
        private readonly IServiceVehicleTypesService serviceVehicleTypesService;

        /// <summary>
        /// Initializes a new instance of the <see cref="VehicleTypeService"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="mapper">The mapper.</param>
        /// <param name="serviceVehicleTypesService">The service vehicle types service.</param>
        public VehicleTypeService(SmartGarageDbContext context, IMapper mapper, IServiceVehicleTypesService serviceVehicleTypesService)
        {
            this.context = context;
            this.mapper = mapper;
            this.serviceVehicleTypesService = serviceVehicleTypesService;
        }

        /// <summary>
        /// Get  all vehicleTypes as an asynchronous operation.
        /// </summary>
        /// <returns>A Task&lt;ICollection`1&gt; representing the asynchronous operation.</returns>
        public async Task<ICollection<VehicleTypeDTO>> GetAllAsync()
        {
            var allVehicleTypes = await this.context.VehicleTypes
               .Select(v => new VehicleTypeDTO
               {
                   VehicleTypeId = v.VehicleTypeId,
                   Name = v.Name,
                   Coefficient = v.Coefficient
               }).ToListAsync();

            return allVehicleTypes;
        }

        /// <summary>
        /// Get vehicleTypes by id as an asynchronous operation.
        /// </summary>
        /// <param name="vehicleTypesId">The identifier.</param>
        /// <returns>A Task&lt;VehicleTypeDTO&gt; representing the asynchronous operation.</returns>
        /// <exception cref="System.ArgumentNullException"></exception>
        public async Task<VehicleTypeDTO> GetAsync(int vehicleTypesId)
        {
            var vehicleType = await this.context.VehicleTypes
               .FirstOrDefaultAsync(v => v.VehicleTypeId == vehicleTypesId)
               ?? throw new NotFoundException();

            var vehicleTypeDTO = mapper.Map<VehicleTypeDTO>(vehicleType);

            return vehicleTypeDTO;
        }

        /// <summary>
        /// Get vehicleTypes selected as an asynchronous operation.
        /// </summary>
        /// <param name="ids">The ids.</param>
        /// <returns>A Task&lt;ICollection`1&gt; representing the asynchronous operation.</returns>
        public async Task<ICollection<VehicleTypeDTO>> GetSelectedAsync(ICollection<int> ids)
        {
            var vehicleTypes = await this.context.VehicleTypes
               .Where(v => ids.Contains(v.VehicleTypeId))
               .ToListAsync();

            var vehicleTypesDTO = mapper.Map<ICollection<VehicleTypeDTO>>(vehicleTypes);

            return vehicleTypesDTO;
        }

        /// <summary>
        /// Create as an asynchronous operation.
        /// </summary>
        /// <param name="dto">The dto.</param>
        /// <returns>A Task&lt;VehicleTypeDTO&gt; representing the asynchronous operation.</returns>
        public async Task<VehicleTypeDTO> CreateAsync(VehicleTypeCreateDTO dto)
        {
            //var vehicleTypeNameCheck = this.context.VehicleTypes
            //    .Where(s => s.Name.Equals(dto.Name));

            //if (vehicleTypeNameCheck.Count() > 0)
            //    throw new AlreadyExistException("Service");

            var vehicleType = mapper.Map<VehicleType>(dto);
            this.context.VehicleTypes.Add(vehicleType);

            await this.context.SaveChangesAsync();

            var vehicleTypeDto = mapper.Map<VehicleTypeDTO>(vehicleType);

            return vehicleTypeDto;
        }

        /// <summary>
        /// Update vehicleTypes as an asynchronous operation.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="dto">The dto.</param>
        /// <returns>A Task&lt;VehicleTypeDTO&gt; representing the asynchronous operation.</returns>
        /// <exception cref="System.ArgumentNullException"></exception>
        public async Task<VehicleTypeDTO> UpdateAsync(int id, VehicleTypeDTO dto)
        {
            var vehicleType = await this.context.VehicleTypes
               .FirstOrDefaultAsync(v => v.VehicleTypeId == id)
               ?? throw new NotFoundException();

            var name = dto.Name;
            var coefficient = dto.Coefficient;

            if (name != null)
                vehicleType.Name = name;
            if (coefficient > 0)
                vehicleType.Coefficient = coefficient;

            await this.context.SaveChangesAsync();

            return dto;
        }

        /// <summary>
        /// Delete vehicleTypes as an asynchronous operation.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>A Task representing the asynchronous operation.</returns>
        /// <exception cref="System.ArgumentNullException"></exception>
        /// <exception cref="CustomExceptions.Exceptions.CanNotBeDeleteException"></exception>
        public async Task DeleteAsync(int id)
        {
            var vehicleType = await this.context.VehicleTypes
               .FirstOrDefaultAsync(s => s.VehicleTypeId == id)
              ?? throw new NotFoundException();

            // must not have vehicletypeid for vehicle
            var vehiclesByVehicleType = this.context.Vehicles
                .Where(v => v.VehicleTypeId == vehicleType.VehicleTypeId)
                .AsQueryable();

            if (vehiclesByVehicleType.Count() > 0)
                throw new CanNotBeDeleteException();
            else
            {
                this.context.VehicleTypes.Remove(vehicleType);
                await this.serviceVehicleTypesService.DeleteByVehicleTypeIdAsync(id);
            }

                await this.context.SaveChangesAsync();  // in or out of else{}
        }
    }
}
