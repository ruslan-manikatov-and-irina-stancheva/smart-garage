﻿using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTO;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace SmartGarage.Services.Services
{
    public class CurrencyService : ICurrencyService
    {
        /// <summary>
        /// Converts the currency.
        /// </summary>
        /// <param name="currency">The currency.</param>
        /// <param name="service">The service.</param>
        /// <returns>ServiceDTO with the converted currency;.</returns>
        public async Task<ServiceDTO> ConvertCurrency(string currency, ServiceDTO service)
        {
            currency = currency.Trim().ToUpper();
            //http://data.fixer.io/api/convert/652ed273f79f6b0d1b5c911f0b2edaa6
            //? access_key = API_KEY
            //& from = GBP
            //& to = JPY
            //& amount = 25

            var client = new HttpClient();

            ///api/v7/convert?q=USD_PHP,PHP_USD&compact=ultra&apiKey=[YOUR_API_KEY]
            //var url = "http://data.fixer.io/api/latest?access_key=5240cceb06f5a66ba1c29e24ea0bde8d" + $"&symbols={currency}";

            var url = "https://free.currconv.com/api/v7/convert" + $"?q={service.Currency}_{currency}&compact=ultra&apiKey=52f11353b11228e07d62";

            var responseTask = client.GetAsync(url);

            responseTask.Wait();

            var result = responseTask.Result;

            //if (result.IsSuccessStatusCode)
            //{
                var json = await result.Content.ReadAsStringAsync();

                //var rate = Double.Parse(json.Split(':')[1].TrimEnd('}'));
                var rate = Decimal.Parse(json.Split(':')[1].TrimEnd('}'));

                service.Currency = currency;
                //public static decimal operator * (decimal d1, decimal d2);
                service.Price = Math.Round(service.Price * rate, 2);

                return service;
            //}
            //else
            //{
            //    throw new ArgumentException();
            //}


        }   
    }
}
