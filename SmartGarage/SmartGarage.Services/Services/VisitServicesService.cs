﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using SmartGarage.Data;
using SmartGarage.Data.Models;
using SmartGarage.Services.Contracts;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Services.Services
{
    /// <summary>
    /// Class VisitServicesService.
    /// Implements the <see cref="SmartGarage.Services.Contracts.IVisitServicesService" />
    /// </summary>
    /// <seealso cref="SmartGarage.Services.Contracts.IVisitServicesService" />
    public class VisitServicesService : IVisitServicesService
    {
        private readonly SmartGarageDbContext context;

        /// <summary>
        /// Initializes a new instance of the <see cref="VisitServicesService"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="mapper">The mapper.</param>
        public VisitServicesService(SmartGarageDbContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Gets all visit services.
        /// </summary>
        /// <returns>IQueryable&lt;VisitServices&gt;.</returns>
        public IQueryable<VisitServices> GetAll()
        {
            var allVisitServices = this.context.VisitServices
                            .AsQueryable();

            return allVisitServices;
        }

        /// <summary>
        /// Gets visit services by specified visit identifier.
        /// </summary>
        /// <param name="visitId">The visit identifier.</param>
        /// <returns>IQueryable&lt;VisitServices&gt;.</returns>
        public IQueryable<VisitServices> Get(int visitId)
        {
            var visitServices = this.context.VisitServices
               .Where(vs => vs.VisitId == visitId)
               .AsQueryable();

            return visitServices;
        }

        /// <summary>
        /// Create visit service as an asynchronous operation.
        /// </summary>
        /// <param name="visitId">The visit identifier.</param>
        /// <param name="services">The services.</param>
        /// <returns>A Task representing the asynchronous operation.</returns>
        public async Task CreateAsync(int visitId, ICollection<Service> services)
        {
            ICollection<VisitServices> visitServices = new Collection<VisitServices>();

            foreach (Service s in services)
            {
                visitServices.Add(new VisitServices
                {
                    VisitId = visitId,
                    ServiceId = s.ServiceId
                });
            }

            this.context.VisitServices.AddRange(visitServices);

            await this.context.SaveChangesAsync();
        }

        /// <summary>
        /// Delete by visit identifier as an asynchronous operation.
        /// </summary>
        /// <param name="visitId">The visit identifier.</param>
        /// <returns>A Task representing the asynchronous operation.</returns>
        public async Task CreateAsync(int visitId, ICollection<int> services)
        {
            ICollection<VisitServices> visitServices = new Collection<VisitServices>();

            foreach (var s in services)
            {
                visitServices.Add(new VisitServices
                {
                    VisitId = visitId,
                    ServiceId = s
                });
            }

            this.context.VisitServices.AddRange(visitServices);

            await this.context.SaveChangesAsync();
        }

        public async Task DeleteByVisitIdAsync(int visitId)
        {
            var visitServicesToRemove = await this.context.VisitServices
              .Where(vs => vs.VisitId == visitId)
              .ToListAsync();

            if (visitServicesToRemove.Count > 0)
            {
                this.context.VisitServices.RemoveRange(visitServicesToRemove);

                await this.context.SaveChangesAsync();
            }
        }

        /// <summary>
        /// Delete by service identifier as an asynchronous operation.
        /// </summary>
        /// <param name="serviceId">The service identifier.</param>
        /// <returns>A Task representing the asynchronous operation.</returns>
        public async Task DeleteByServiceIdAsync(int serviceId)
        {
            var visitServicesToRemove = await this.context.VisitServices
              .Where(vs => vs.ServiceId == serviceId)
              .ToListAsync();

            if (visitServicesToRemove.Count > 0)
            {
                this.context.VisitServices.RemoveRange(visitServicesToRemove);

                await this.context.SaveChangesAsync();
            }
        }
    }
}
