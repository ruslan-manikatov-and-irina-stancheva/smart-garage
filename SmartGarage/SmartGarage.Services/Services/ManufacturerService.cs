﻿using System;
using System.Linq;
using SmartGarage.Services.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;
using CustomExceptions.Exceptions;
using Microsoft.EntityFrameworkCore;
using SmartGarage.Data;
using SmartGarage.Data.Models;
using SmartGarage.Services.Contracts;
using AutoMapper;

namespace SmartGarage.Services.Services
{
    /// <summary>
    /// Class ManufacturerService.
    /// Implements the <see cref="SmartGarage.Services.Contracts.IManufacturerService" />
    /// </summary>
    /// <seealso cref="SmartGarage.Services.Contracts.IManufacturerService" />
    public class ManufacturerService : IManufacturerService
    {
        private readonly SmartGarageDbContext context;
        private readonly IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="ManufacturerService"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="mapper">The mapper.</param>
        public ManufacturerService(SmartGarageDbContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        /// <summary>
        /// Get all manufacturers as an asynchronous operation.
        /// </summary>
        /// <returns>A Task&lt;ICollection`1&gt; representing the asynchronous operation.</returns>
        public async Task<ICollection<ManufacturerDTO>> GetAllAsync()
        {
            var manufacturetrDTO = await this.context.Manufacturers
                .Select(manufacturer => mapper.Map<ManufacturerDTO>(manufacturer))
                .ToListAsync();
            return manufacturetrDTO;
        }

        /// <summary>
        /// Get manufacturers by id an asynchronous operation.
        /// </summary>
        /// <param name="manufacturerId">The manufacturer identifier.</param>
        /// <returns>A Task&lt;ManufacturerDTO&gt; representing the asynchronous operation.</returns>
        /// <exception cref="CustomExceptions.Exceptions.ManufacturerNotFoundException"></exception>
        public async Task<ManufacturerDTO> GetAsync(int? manufacturerId)
        {
            var manufacturer = await context.Manufacturers
                .SingleOrDefaultAsync(m => m.ManufacturerId == manufacturerId)             
                ?? throw new ManufacturerNotFoundException();

            var manufacturerDto = mapper.Map<ManufacturerDTO>(manufacturer);
            return manufacturerDto;
        }

        /// <summary>
        /// Create manufacturers an asynchronous operation.
        /// </summary>
        /// <param name="manufacturerDTO">The manufacturer dto.</param>
        /// <returns>A Task&lt;ManufacturerCreateDTO&gt; representing the asynchronous operation.</returns>
        /// <exception cref="System.ArgumentNullException"></exception>
        public async Task<ManufacturerCreateDTO> CreateAsync(ManufacturerCreateDTO manufacturerDTO)
        {
            if(manufacturerDTO.Name == null || manufacturerDTO.Code == null)
            {
                throw new  ArgumentNullException();
            }

            var manufacturer = mapper.Map<Manufacturer>(manufacturerDTO);
            this.context.Manufacturers.Add(manufacturer);
            await this.context.SaveChangesAsync();
            return manufacturerDTO;
        }

        /// <summary>
        /// Update manufacturers an asynchronous operation.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="manufacturerDTO">The manufacturer dto.</param>
        /// <returns>A Task&lt;ManufacturerCreateDTO&gt; representing the asynchronous operation.</returns>
        /// <exception cref="CustomExceptions.Exceptions.ManufacturerNotFoundException"></exception>
        /// <exception cref="System.ArgumentNullException"></exception>
        public async Task<ManufacturerCreateDTO> UpdateAsync(int id, ManufacturerCreateDTO manufacturerDTO)
        {
            var manufacturer = await this.context.Manufacturers
                .FirstOrDefaultAsync(m => m.ManufacturerId == id)
                ?? throw new ManufacturerNotFoundException();

            if(manufacturerDTO.Name == null || manufacturerDTO.Code == null)
            {
                throw new ArgumentNullException();
            }

            manufacturer.Name = manufacturerDTO.Name;
            manufacturer.Code = manufacturerDTO.Code;
            await this.context.SaveChangesAsync();

            var dto = mapper.Map<ManufacturerCreateDTO>(manufacturer);
            return dto;
        }

        /// <summary>
        /// Delete manufacturers an asynchronous operation.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>A Task&lt;System.Boolean&gt; representing the asynchronous operation.</returns>
        /// <exception cref="CustomExceptions.Exceptions.ManufacturerNotFoundException"></exception>
        /// <exception cref="System.ArgumentOutOfRangeException"></exception>
        public async Task<bool> DeleteAsync(int id)
        {
            var manufacturer = await this.context.Manufacturers
                .FirstOrDefaultAsync(m => m.ManufacturerId == id)
                ?? throw new ManufacturerNotFoundException();

            var manufacturerModels = await this.context.Models
                .Where(m => m.ManufacturerId == id).CountAsync();

            if (manufacturerModels > 0)
            {
                throw new ArgumentOutOfRangeException();
            }
            this.context.Manufacturers.Remove(manufacturer);
            await this.context.SaveChangesAsync();

            return true;
        }

    }
}
