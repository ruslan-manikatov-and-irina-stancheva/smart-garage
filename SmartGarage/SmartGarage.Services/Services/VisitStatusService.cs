﻿using AutoMapper;
using CustomExceptions.Exceptions;
using Microsoft.EntityFrameworkCore;
using SmartGarage.Data;
using SmartGarage.Data.Models;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Services.Services
{
    /// <summary>
    /// Class VisitStatusService.
    /// Implements the <see cref="SmartGarage.Services.Contracts.IVisitStatusService" />
    /// </summary>
    /// <seealso cref="SmartGarage.Services.Contracts.IVisitStatusService" />
    public class VisitStatusService : IVisitStatusService
    {
        private readonly SmartGarageDbContext context;
        private readonly IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="VisitStatusService"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="mapper">The mapper.</param>
        public VisitStatusService(SmartGarageDbContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        /// <summary>
        /// Get all visit status as an asynchronous operation.
        /// </summary>
        /// <returns>A Task&lt;ICollection`1&gt; representing the asynchronous operation.</returns>
        public async Task<ICollection<VisitStatusDTO>> GetAllAsync()
        {
            var allVisitStatusesDTO = await this.context.VisitStatuses
                .Select(visitStatus => mapper.Map<VisitStatusDTO>(visitStatus))
                .ToListAsync();

            return allVisitStatusesDTO;
        }

        /// <summary>
        /// Get visit status by given Id as an asynchronous operation.
        /// </summary>
        /// <param name="visitStatusId">The identifier.</param>
        /// <returns>A Task&lt;VisitStatusDTO&gt; representing the asynchronous operation.</returns>
        /// <exception cref="System.ArgumentNullException"></exception>
        public async Task<VisitStatusDTO> GetAsync(int visitStatusId)
        {
            var visitStatus = await this.context.VisitStatuses
                .FirstOrDefaultAsync(v => v.VisitStatusId == visitStatusId)
                ?? throw new ArgumentNullException();

            var visitStatusDTO = mapper.Map<VisitStatusDTO>(visitStatus);

            return visitStatusDTO;
        }

        /// <summary>
        /// Create visit status  as an asynchronous operation.
        /// </summary>
        /// <param name="dto">The dto.</param>
        /// <returns>A Task&lt;VisitStatusDTO&gt; representing the asynchronous operation.</returns>
        public async Task<VisitStatusDTO> CreateAsync(VisitStatusCreateDTO dto)
        {
            var visitStatus = mapper.Map<VisitStatus>(dto);

            this.context.VisitStatuses.Add(visitStatus);

            await this.context.SaveChangesAsync();

            var visitStatusDto = mapper.Map<VisitStatusDTO>(visitStatus);

            return visitStatusDto;
        }

        /// <summary>
        /// Update visit status as an asynchronous operation.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="dto">The dto.</param>
        /// <returns>A Task&lt;VisitStatusDTO&gt; representing the asynchronous operation.</returns>
        /// <exception cref="System.ArgumentNullException"></exception>
        public async Task<VisitStatusDTO> UpdateAsync(int id, VisitStatusDTO dto)
        {
            var visitStatus = await this.context.VisitStatuses
               .FirstOrDefaultAsync(v => v.VisitStatusId == id)
               ?? throw new ArgumentNullException();

            visitStatus.Name = dto.Name;

            await this.context.SaveChangesAsync();

            return dto;
        }

        /// <summary>
        /// Delete visit status as an asynchronous operation.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>A Task representing the asynchronous operation.</returns>
        /// <exception cref="System.ArgumentNullException"></exception>
        /// <exception cref="CustomExceptions.Exceptions.CanNotBeDeleteException"></exception>
        public async Task DeleteAsync(int id)
        {
            var visitStatus = await this.context.VisitStatuses
               .FirstOrDefaultAsync(v => v.VisitStatusId == id)
               ?? throw new ArgumentNullException();

            var visitsByVisitStatus = this.context.Visits
                .Where(v => v.VisitStatusId == id)
                .AsQueryable();

            if (visitsByVisitStatus.Count() > 0)
                throw new CanNotBeDeleteException();
            else
                this.context.VisitStatuses.Remove(visitStatus);

            await this.context.SaveChangesAsync();
        }
    }
}
