﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using SmartGarage.Data;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Services.Services
{
    /// <summary>
    /// Class RoleService.
    /// Implements the <see cref="SmartGarage.Services.Contracts.IRoleService" />
    /// </summary>
    /// <seealso cref="SmartGarage.Services.Contracts.IRoleService" />
    public class RoleService : IRoleService
    {
        private readonly SmartGarageDbContext context;
        private readonly IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="RoleService"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="mapper">The mapper.</param>
        public RoleService(SmartGarageDbContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }
        /// <summary>
        /// Get all user roles as an asynchronous operation.
        /// </summary>
        /// <returns>A Task&lt;IEnumerable`1&gt; representing the asynchronous operation.</returns>
        public async Task<IEnumerable<RoleDTO>> GetAllAsync()
        {
            var role = await context.Roles.ToListAsync();
            var roleDto = new List<RoleDTO>();

            foreach (var item in role)
            {
                var dto = new RoleDTO();
                dto.RoleId = item.Id;
                dto.Name = item.Name;
                roleDto.Add(dto);
            }

            return roleDto;
        }
    }
}
