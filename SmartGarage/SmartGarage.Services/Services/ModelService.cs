﻿using AutoMapper;
using SmartGarage.Data;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTO;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using CustomExceptions.Exceptions;
using SmartGarage.Data.Models;
using System;

namespace SmartGarage.Services.Services
{
    /// <summary>
    /// Class ModelService.
    /// Implements the <see cref="SmartGarage.Services.Contracts.IModelService" />
    /// </summary>
    /// <seealso cref="SmartGarage.Services.Contracts.IModelService" />
    public class ModelService : IModelService
    {
        private readonly SmartGarageDbContext context;
        private readonly IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="ModelService"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="mapper">The mapper.</param>
        public ModelService(SmartGarageDbContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        /// <summary>
        /// Get all vehicle models as an asynchronous operation.
        /// </summary>
        /// <returns>A Task&lt;ICollection`1&gt; representing the asynchronous operation.</returns>
        public async Task<ICollection<CarModelDTO>> GetAllAsync()
        {

            var carModelDto = await this.context.Models
                .Include(m => m.Manufacturer)
                .Select(models => mapper.Map<CarModelDTO>(models))
                .ToListAsync();
            return carModelDto;
        }


        /// <summary>
        /// Get vehicle models by id as an asynchronous operation.
        /// </summary>
        /// <param name="modelId">The model identifier.</param>
        /// <returns>A Task&lt;CarModelDTO&gt; representing the asynchronous operation.</returns>
        /// <exception cref="CustomExceptions.Exceptions.ModelNotFoundException"></exception>
        public async Task<CarModelDTO> GetAsync(int? modelId)
        {
            var carModel = await context.Models
                .Include(m => m.Manufacturer)
                .SingleOrDefaultAsync(m => m.ModelId == modelId)
                ?? throw new ModelNotFoundException();

            var carModelDto = mapper.Map<CarModelDTO>(carModel);
            return carModelDto;
        }

        /// <summary>
        /// Create vehicle models as an asynchronous operation.
        /// </summary>
        /// <param name="CarModelDTO">The car model dto.</param>
        /// <returns>A Task&lt;CarModelCreateDTO&gt; representing the asynchronous operation.</returns>
        /// <exception cref="System.ArgumentNullException"></exception>
        /// <exception cref="CustomExceptions.Exceptions.ManufacturerNotFoundException"></exception>
        public async Task<CarModelCreateDTO> CreateAsync(CarModelCreateDTO CarModelDTO)
        {
            if (CarModelDTO.Name == null || CarModelDTO.Code == null)
            {
                throw new ArgumentNullException();
            }

            var make = await this.context.Manufacturers
                .FirstOrDefaultAsync(m => m.ManufacturerId == CarModelDTO.ManufacturerId)
                ?? throw new ManufacturerNotFoundException();

            var carModel = mapper.Map<Model>(CarModelDTO);
            this.context.Models.Add(carModel);
            await this.context.SaveChangesAsync();
            return CarModelDTO;
        }

        /// <summary>
        /// Update vehicle models as an asynchronous operation.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="modelDTO">The model dto.</param>
        /// <returns>A Task&lt;CarModelCreateDTO&gt; representing the asynchronous operation.</returns>
        /// <exception cref="CustomExceptions.Exceptions.ModelNotFoundException"></exception>
        /// <exception cref="System.ArgumentNullException"></exception>
        public async Task<CarModelCreateDTO> UpdateAsync(int id, CarModelCreateDTO modelDTO)
        {
            var model = await this.context.Models
                .FirstOrDefaultAsync(m => m.ModelId == id)
                ?? throw new ModelNotFoundException();

            if (modelDTO.Name == null || modelDTO.Code == null)
            {
                throw new ArgumentNullException();
            }

            model.Name = modelDTO.Name;
            model.Code = modelDTO.Code;
            model.ManufacturerId = modelDTO.ManufacturerId;
            await this.context.SaveChangesAsync();

            var dto = mapper.Map<CarModelCreateDTO>(model);
            return dto;
        }

        /// <summary>
        /// Delete vehicle models as an asynchronous operation.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>A Task&lt;System.Boolean&gt; representing the asynchronous operation.</returns>
        /// <exception cref="CustomExceptions.Exceptions.ModelNotFoundException"></exception>
        public async Task<bool> DeleteAsync(int id)
        {
            var carModel = await this.context.Models
                .FirstOrDefaultAsync(m => m.ModelId == id)
                ?? throw new ModelNotFoundException();

            this.context.Models.Remove(carModel);
            await this.context.SaveChangesAsync();
            return true;
        }

    }

}
