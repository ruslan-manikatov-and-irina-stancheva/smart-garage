﻿using Microsoft.Extensions.Options;
using SmartGarage.Services.EmailConfigurationModels;
using SmartGarage.Services.Contracts;
using System.Threading.Tasks;
using System.IO;
using MailKit.Security;
using MailKit.Net.Smtp;
using MimeKit;

namespace SmartGarage.Services.Services
{
    /// <summary>
    /// Class EmailService.
    /// Implements the <see cref="SmartGarage.Services.Contracts.IEmailService" />
    /// </summary>
    /// <seealso cref="SmartGarage.Services.Contracts.IEmailService" />
    public class EmailService : IEmailService
    {
        private readonly MailSettings mailSettings;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmailService"/> class.
        /// </summary>
        /// <param name="mailSettings">The mail settings.</param>
        public EmailService(IOptions<MailSettings> mailSettings)
        {
            this.mailSettings = mailSettings.Value;
        }

        /// <summary>
        /// Send email as an asynchronous operation.
        /// </summary>
        /// <param name="mailRequest">The mail request.</param>
        /// <returns>A Task&lt;System.Boolean&gt; representing the asynchronous operation.</returns>
        public async Task<bool> SendEmailAsync(MailRequest mailRequest)
        {
            var email = new MimeMessage();
            email.Sender = MailboxAddress.Parse(mailSettings.Mail);
            email.To.Add(MailboxAddress.Parse(mailRequest.ToEmail));
            email.Subject = mailRequest.Subject;
            var builder = new BodyBuilder();
            if (mailRequest.Attachments != null)
            {
                byte[] fileBytes;
                foreach (var file in mailRequest.Attachments)
                {
                    if (file.Length > 0)
                    {
                        using (var ms = new MemoryStream())
                        {
                            file.CopyTo(ms);
                            fileBytes = ms.ToArray();
                        }
                        builder.Attachments.Add(file.FileName, fileBytes, ContentType.Parse(file.ContentType));
                    }
                }
            }
            builder.HtmlBody = mailRequest.Body;
            email.Body = builder.ToMessageBody();
            using var smtp = new SmtpClient();
            
          
            smtp.Connect(mailSettings.Host, mailSettings.Port, SecureSocketOptions.StartTls);
            smtp.Authenticate(mailSettings.Mail, mailSettings.Password);
            await smtp.SendAsync(email);
            smtp.Disconnect(true);
            return true;
        }

        /// <summary>
        /// Send welcome email as an asynchronous operation to a new user,
        /// with a random generated password.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>Void, only a Task representing the asynchronous operation.</returns>
        public async Task SendWelcomeEmailAsync(WelcomeRequest request)
        {
            //string FilePath = Directory.GetCurrentDirectory() + "\\Templates\\WelcomeTemplate.html";

            string FilePath = Directory.GetCurrentDirectory() + "\\wwwroot\\Templates\\WelcomeTemplate.html";

            StreamReader str = new StreamReader(FilePath);
            string MailText = str.ReadToEnd();

            str.Close();

            MailText = MailText.Replace("[username]", request.UserName)
                .Replace("[email]", request.ToEmail)
                .Replace("[password]", request.Password);


            var email = new MimeMessage();
            email.Sender = MailboxAddress.Parse(mailSettings.Mail);
            email.To.Add(MailboxAddress.Parse(request.ToEmail));
            email.Subject = $"Welcome {request.UserName}";

            var builder = new BodyBuilder();
            builder.HtmlBody = MailText;

            email.Body = builder.ToMessageBody();
            using var smtp = new SmtpClient();

            smtp.Connect(mailSettings.Host, mailSettings.Port, SecureSocketOptions.StartTls);
            smtp.Authenticate(mailSettings.Mail, mailSettings.Password);

            await smtp.SendAsync(email);
            smtp.Disconnect(true);
        }
    }
}
