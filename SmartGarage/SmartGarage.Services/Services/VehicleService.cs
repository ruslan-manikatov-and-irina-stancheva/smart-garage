﻿using AutoMapper;
using CustomExceptions.Exceptions;
using Microsoft.EntityFrameworkCore;
using SmartGarage.Data;
using SmartGarage.Data.Models;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Services.Services
{
    /// <summary>
    /// Class VehicleService.
    /// Implements the <see cref="SmartGarage.Services.Contracts.IVehicleService" />
    /// </summary>
    /// <seealso cref="SmartGarage.Services.Contracts.IVehicleService" />
    public class VehicleService : IVehicleService
    {
        private readonly SmartGarageDbContext context;
        private readonly IUserService userService;
        private readonly IModelService modelService;
        private readonly IVehicleTypeService vehicleTypeService;

        /// <summary>
        /// Initializes a new instance of the <see cref="VehicleService"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="userService">The user service.</param>
        /// <param name="modelService">The model service.</param>
        /// <param name="vehicleTypeService">The vehicle type service.</param>
        public VehicleService(SmartGarageDbContext context, IUserService userService, IModelService modelService, IVehicleTypeService vehicleTypeService)
        {
            this.context = context;
            this.userService = userService;
            this.modelService = modelService;
            this.vehicleTypeService = vehicleTypeService;
        }

        /// <summary>
        /// Get all vehicles as an asynchronous operation.
        /// </summary>
        /// <returns>A Task&lt;ICollection`1&gt; representing the asynchronous operation.</returns>
        public async Task<ICollection<VehicleDTO>> GetAllAsync()
        {
            var allVehiclesDTO = await this.context.Vehicles
                .Select(v => new VehicleDTO
                {
                    VehicleId = v.VehicleId,
                    Vin = v.Vin,
                    RegistrationPlate = v.RegistrationPlate,
                    OwnerId = v.OwnerId,
                    ModelId = v.ModelId,
                    Year = v.Year,
                    CreatedOn = v.CreatedOn,
                    VehicleTypeId = v.VehicleTypeId,
                    VehicleType = v.VehicleType.Name,
                    Visits = v.Visits.Where(visit => visit.VehicleId == v.VehicleId).Select(v => v.VisitId).ToList()
                }).ToListAsync();

            return allVehiclesDTO;
        }

        /// <summary>
        /// Get vehicles by goven vehicle id as an asynchronous operation.
        /// </summary>
        /// <param name="vehicleId">The identifier.</param>
        /// <returns>A Task&lt;VehicleDetailsDTO&gt; representing the asynchronous operation.</returns>
        /// <exception cref="CustomExceptions.Exceptions.NotFoundException"></exception>
        public async Task<VehicleDetailsDTO> GetAsync(int vehicleId)
        {
            var vehicleDTO = await this.context.Vehicles
                .Where(v => v.VehicleId == vehicleId)
                .Select(v => new VehicleDetailsDTO
                {
                    VehicleId = v.VehicleId,
                    Vin = v.Vin,
                    RegistrationPlate = v.RegistrationPlate,
                    OwnerId = v.OwnerId,
                    Owner = v.Owner.UserName,
                    ModelId = v.ModelId,
                    Model = v.Model.Name,
                    Manufacturer = v.Model.Manufacturer.Name,
                    Year = v.Year,
                    CreatedOn = v.CreatedOn,
                    VehicleTypeId = v.VehicleTypeId,
                    VehicleType = v.VehicleType.Name,
                    Visits = v.Visits.Where(visit => visit.VehicleId == v.VehicleId).Select(v => v.VisitId).ToList()
                })
                .FirstOrDefaultAsync()
                ?? throw new NotFoundException(); 

            return vehicleDTO;
        }

        /// <summary>
        /// Gets vehicle by customer id.
        /// </summary>
        /// <param name="customerId">The identifier.</param>
        /// <returns>IQueryable&lt;VehicleDTO&gt;.</returns>
        public IQueryable<VehicleDTO> GetByCustomer(int customerId)
        {
            var customerVehiclesDTO = this.context.Vehicles
                .Where(v => v.OwnerId == customerId)
                .Select(v => new VehicleDTO
                {
                    VehicleId = v.VehicleId,
                    Vin = v.Vin,
                    RegistrationPlate = v.RegistrationPlate,
                    OwnerId = v.OwnerId,
                    ModelId = v.ModelId,
                    Year = v.Year,
                    CreatedOn = v.CreatedOn,
                    VehicleTypeId = v.VehicleTypeId,
                    VehicleType = v.VehicleType.Name,
                    Visits = v.Visits.Where(visit => visit.VehicleId == v.VehicleId).Select(v => v.VisitId).ToList()
                }).AsQueryable();

            return customerVehiclesDTO;
        }

        /// <summary>
        /// Gets vehicle by customer username.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns>IQueryable&lt;VehicleDTO&gt;.</returns>
        public IQueryable<VehicleDTO> GetByCustomer(string name)
        {
            var customerVehiclesDTO = this.context.Vehicles
                .Where(v => v.Owner.UserName.Equals(name))
                .Select(v => new VehicleDTO
                {
                    VehicleId = v.VehicleId,
                    Vin = v.Vin,
                    RegistrationPlate = v.RegistrationPlate,
                    OwnerId = v.OwnerId,
                    ModelId = v.ModelId,
                    Year = v.Year,
                    CreatedOn = v.CreatedOn,
                    VehicleTypeId = v.VehicleTypeId,
                    VehicleType = v.VehicleType.Name,
                    Visits = v.Visits.Where(visit => visit.VehicleId == v.VehicleId).Select(v => v.VisitId).ToList()
                }).AsQueryable();

            return customerVehiclesDTO;
        }

        /// <summary>
        /// Create vehicle as an asynchronous operation.
        /// </summary>
        /// <param name="dto">The dto.</param>
        /// <returns>A Task&lt;VehicleCreateDTO&gt; representing the asynchronous operation.</returns>
        public async Task<VehicleCreateDTO> CreateAsync(VehicleCreateDTO dto)
        {
            string vin = dto.Vin;
            string registrationPlate = dto.RegistrationPlate;
            int ownerId = dto.OwnerId;
            var modelManufacturer = dto.ModelId; 
            DateTime year = dto.Year;
            var vehicleTypeId = dto.VehicleTypeId;

            await this.userService.GetAsync(ownerId); 

            await this.modelService.GetAsync(modelManufacturer);  

            var vehicleType = await this.vehicleTypeService.GetAsync(vehicleTypeId);

            var vehicle = new Vehicle
            {
                Vin = vin,
                RegistrationPlate = registrationPlate,
                OwnerId = ownerId,
                ModelId = modelManufacturer,
                Year = year,
                VehicleTypeId = vehicleType.VehicleTypeId
            };

            this.context.Vehicles.Add(vehicle);

            await this.context.SaveChangesAsync();

            return dto;
        }

        /// <summary>
        /// Update as an asynchronous operation.
        /// </summary>
        /// <param name="vehicleId">The identifier.</param>
        /// <param name="dto">The dto.</param>
        /// <returns>A Task&lt;VehicleCreateDTO&gt; representing the asynchronous operation.</returns>
        /// <exception cref="CustomExceptions.Exceptions.NotFoundException"></exception>
        public async Task<VehicleCreateDTO> UpdateAsync(int vehicleId, VehicleCreateDTO dto)
        {
            var vehicle = await this.context.Vehicles
               .FirstOrDefaultAsync(v => v.VehicleId == vehicleId)
               ?? throw new NotFoundException(); //ArgumentNullException();

            var vin = dto.Vin;
            var registrationPlate = dto.RegistrationPlate;
            var ownerId = dto.OwnerId;
            var modelId = dto.ModelId;
            var year = dto.Year;
            var vehicleTypeId = dto.VehicleTypeId;

            if (vin != null)
                vehicle.Vin = vin;
            if (registrationPlate != null)
                vehicle.RegistrationPlate = registrationPlate;
            if (ownerId > 0)
                vehicle.OwnerId = ownerId;
            if (modelId > 0)
                vehicle.ModelId = modelId;
            if (year != DateTime.MinValue)
                vehicle.Year = year;
            if (vehicleTypeId > 0)
                vehicle.VehicleTypeId = vehicleTypeId;

            await this.context.SaveChangesAsync();

            return dto;
        }

        /// <summary>
        /// Update by customer as an asynchronous operation.
        /// </summary>
        /// <param name="vehicleId">The vehicle identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="dto">The dto.</param>
        /// <returns>A Task&lt;VehicleUpdateByCustomerDTO&gt; representing the asynchronous operation.</returns>
        /// <exception cref="CustomExceptions.Exceptions.NotFoundException"></exception>
        public async Task<VehicleUpdateByCustomerDTO> UpdateByCustomerAsync(int vehicleId, int customerId, VehicleUpdateByCustomerDTO dto)
        {
            var vehicle = await this.context.Vehicles
               .FirstOrDefaultAsync(v => (v.VehicleId == vehicleId) && (v.OwnerId == customerId))
               ?? throw new NotFoundException(); 

            var vin = dto.Vin;
            var registrationPlate = dto.RegistrationPlate;
            var modelId = dto.ManufacturerModel.ModelId;
            var year = dto.Year;
            var vehicleTypeId = dto.VehicleTypeId;

            if (vin != null)
                vehicle.Vin = vin;
            if (registrationPlate != null)
                vehicle.RegistrationPlate = registrationPlate;
            if (modelId > 0)
                vehicle.ModelId = modelId;
            if (year != DateTime.MinValue)
                vehicle.Year = year;
            if (vehicleTypeId > 0)
                vehicle.VehicleTypeId = vehicleTypeId;

            await this.context.SaveChangesAsync();

            return dto;
        }

        /// <summary>
        /// Delete as an asynchronous operation.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>A Task representing the asynchronous operation.</returns>
        /// <exception cref="CustomExceptions.Exceptions.NotFoundException"></exception>
        /// <exception cref="CustomExceptions.Exceptions.CanNotBeDeleteException"></exception>
        public async Task DeleteAsync(int id)
        {
            var vehicle = await this.context.Vehicles
               .FirstOrDefaultAsync(v => v.VehicleId == id)
               ?? throw new NotFoundException(); 

            // check for visits for this vehicle
            var visitsByVehicle = this.context.Visits
                .Where(v => v.VehicleId == vehicle.VehicleId)
                .AsQueryable();

            if (visitsByVehicle.Count() > 0)
                throw new CanNotBeDeleteException();
            else
                vehicle.IsDeleted = true;

            await this.context.SaveChangesAsync();
        }
    }
}
