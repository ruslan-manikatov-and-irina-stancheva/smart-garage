﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using SmartGarage.Data;
using SmartGarage.Data.Models;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTO;
using SmartGarage.Services.EmailConfigurationModels;
using CustomExceptions.Exceptions;

namespace SmartGarage.Services.Services
{
    /// <summary>
    /// Class UserService.
    /// Implements the <see cref="SmartGarage.Services.Contracts.IUserService" />
    /// </summary>
    /// <seealso cref="SmartGarage.Services.Contracts.IUserService" />
    public class UserService : IUserService
    {
        private readonly IMapper mapper;
        private readonly IEmailService emailService;
        private readonly UserManager<User> userManager;
        private readonly SmartGarageDbContext context;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserService"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="mapper">The mapper.</param>
        /// <param name="emailService">The email service.</param>
        /// <param name="userManager">The user manager.</param>
        public UserService(SmartGarageDbContext context, IMapper mapper, IEmailService emailService, UserManager<User> userManager)
        {
            this.context = context;
            this.mapper = mapper;
            this.emailService = emailService;
            this.userManager = userManager;
        }

        /// <summary>
        /// Get all users as an asynchronous operation.
        /// </summary>
        /// <returns>A Task&lt;IEnumerable`1&gt; representing the asynchronous operation.</returns>
        public async Task<IEnumerable<UserGetDTO>> GetAllAsync()
        {
            var usersDTO = await this.context.Users
                .Select(user => mapper.Map<UserGetDTO>(user))
                .ToListAsync();

            return usersDTO;
        }

        /// <summary>
        /// Filters users by role.
        /// </summary>
        /// <param name="role">The role.</param>
        /// <returns>Task&lt;IEnumerable&lt;UserGetDTO&gt;&gt;.</returns>
        public async Task<IEnumerable<UserGetDTO>> FilterUsersByRole(string role)
        {
            var users = await userManager.GetUsersInRoleAsync(role);
            var userDto = users.Select(user => mapper.Map<UserGetDTO>(user));

            return userDto;
        }

        /// <summary>
        /// Filters users by last name.
        /// </summary>
        /// <param name="lastName">The last name.</param>
        /// <returns>Task&lt;IEnumerable&lt;UserGetDTO&gt;&gt;.</returns>
        /// <exception cref="System.ArgumentNullException"></exception>
        public async Task<IEnumerable<UserGetDTO>> FilterUsersByLastName(string lastName)
        {
            var userDTO = await this.context.Users
                .Where(u => u.LastName.Contains(lastName))
                .Select(user => mapper.Map<UserGetDTO>(user))
                .ToListAsync() ?? throw new ArgumentNullException();

            return userDTO;
        }

        /// <summary>
        /// Filters users by email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <returns>Task&lt;IEnumerable&lt;UserGetDTO&gt;&gt;.</returns>
        /// <exception cref="System.ArgumentNullException"></exception>
        public async Task<IEnumerable<UserGetDTO>> FilterUsersByEmail(string email)
        {
            var userDTO = await this.context.Users
                .Where(u => u.Email.Contains(email))
                .Select(user => mapper.Map<UserGetDTO>(user))
                .ToListAsync() ?? throw new ArgumentNullException();

            return userDTO;
        }

        /// <summary>
        /// Filters users by phone.
        /// </summary>
        /// <param name="phone">The phone.</param>
        /// <returns>Task&lt;IEnumerable&lt;UserGetDTO&gt;&gt;.</returns>
        public async Task<IEnumerable<UserGetDTO>> FilterUsersByPhone(string phone)
        {
            var userDTO = await this.context.Users
                .Where(u => u.PhoneNumber.Contains(phone))
                .Select(user => mapper.Map<UserGetDTO>(user))
                .ToListAsync();

            return userDTO;
        }

        /// <summary>
        /// Filters users by vehicle model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns>Task&lt;IEnumerable&lt;UserGetDTO&gt;&gt;.</returns>
        public async Task<IEnumerable<UserGetDTO>> FilterUsersByVehicleModel(string model)
        {
            var userDTO = await this.context.Vehicles
                .Where(v => v.Model.Name == model)
                .Select(v => v.Owner)
                .Select(user => mapper.Map<UserGetDTO>(user))
                .ToListAsync();

            return userDTO;
        }

        /// <summary>
        /// Filters the users by vehicle make.
        /// </summary>
        /// <param name="make">The make.</param>
        /// <returns>IEnumerable&lt;UserGetDTO&gt;.</returns>
        public async Task<IEnumerable<UserGetDTO>> FilterUsersByVehicleMake(string make)
        {
            var userDTO = await this.context.Vehicles
                .Where(v => v.Model.Manufacturer.Name == make)
                .Select(v => v.Owner)
                .Select(user => mapper.Map<UserGetDTO>(user))
                .ToListAsync();

            return userDTO;
        }

        /// <summary>
        /// Filters users by visit in range.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <returns>Task&lt;IEnumerable&lt;UserGetDTO&gt;&gt;.</returns>
        public async Task<IEnumerable<UserGetDTO>> FilterUsersByVisitInRange(DateTime? fromDate, DateTime? toDate)
        {
            var userDTO = await this.context.Visits
                .Where(v => (v.ArriveDate >= fromDate) && v.ArriveDate <= toDate)
                .Select(v => v.Vehicle)
                .Select(v => v.Owner)
                .Select(user => mapper.Map<UserGetDTO>(user))
                .ToListAsync();

            return userDTO;
        }

        /// <summary>
        /// Sorts users by name.
        /// </summary>
        /// <returns>Task&lt;IEnumerable&lt;UserGetDTO&gt;&gt;.</returns>
        public async Task<IEnumerable<UserGetDTO>> SortUsersByName()
        {
            var usersDTO = await this.context.Users
                .OrderBy(u => u.FirstName)
                .ThenBy(u => u.LastName)
                .Select(user => mapper.Map<UserGetDTO>(user))
                .ToListAsync();

            return usersDTO;
        }

        /// <summary>
        /// Sorts users by name visit date.
        /// </summary>
        /// <returns>Task&lt;IEnumerable&lt;UserGetDTO&gt;&gt;.</returns>
        public async Task<IEnumerable<UserGetDTO>> SortUsersByVisitDate()
        {
            var usersDTO = await this.context.Visits
                .OrderBy(v => v.ArriveDate)
                .Select(v => v.Vehicle)
                .Select(v => v.Owner)
                .Select(user => mapper.Map<UserGetDTO>(user))
                .ToListAsync();

            return usersDTO;
        }

        /// <summary>
        /// Get one user as an asynchronous operation.
        /// </summary>
        /// <param name="userId">user id.</param>
        /// <returns>A Task&lt;UserGetDTO&gt; representing the asynchronous operation.</returns>
        /// <exception cref="CustomExceptions.Exceptions.UserNotFoundException"></exception>
        public async Task<UserGetDTO> GetAsync(int? userId)
        {
            var user = await this.context.Users
               .Where(u => u.Id == userId)
               .FirstOrDefaultAsync()
               ?? throw new UserNotFoundException();

            var userDTO = mapper.Map<UserGetDTO>(user);

            return userDTO;
        }

        /// <summary>
        /// Update user as an asynchronous operation.
        /// </summary>
        /// <param name="userId">The identifier.</param>
        /// <param name="modelDTO">The model dto.</param>
        /// <returns>A Task&lt;UserDTO&gt; representing the asynchronous operation.</returns>
        /// <exception cref="CustomExceptions.Exceptions.UserNotFoundException"></exception>
        public async Task<UserDTO> UpdateAsync(int userId, UserDTO modelDTO)
        {
            var model = await this.context.Users
                .FirstOrDefaultAsync(m => m.Id == userId)
                ?? throw new UserNotFoundException();

            model.FirstName = modelDTO.FirstName;
            model.LastName = modelDTO.LastName;
            model.PhoneNumber = modelDTO.PhoneNumber;
            model.Email = modelDTO.Email;
            await this.context.SaveChangesAsync();

            var dto = mapper.Map<UserDTO>(model);
            return dto;
        }

        /// <summary>
        /// Delete user as an asynchronous operation.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>A Task&lt;System.Boolean&gt; representing the asynchronous operation.</returns>
        /// <exception cref="System.ArgumentOutOfRangeException"></exception>
        /// <exception cref="CustomExceptions.Exceptions.UserNotFoundException"></exception>
        public async Task<bool> DeleteAsync(int id)
        {
            var usersVehicle = await this.context.Vehicles
                .Where(v => v.OwnerId == id).CountAsync();

            if (usersVehicle > 0)
            {
                throw new ArgumentOutOfRangeException();
            }


            var user = await this.context.Users
                .FirstOrDefaultAsync(m => m.Id == id)
                ?? throw new UserNotFoundException();
            user.IsDeleted = true;
            
            await this.context.SaveChangesAsync();
            return true;
        }


        /// <summary>
        /// Create as an asynchronous operation. create service add's mutch more properties from DTO to Model
        ///We are not using automapper in this method, because is delicate information, and we dont wont to depend on external sources
        /// </summary>
        /// <param name="userDTO">The user dto.</param>
        /// <returns>A Task&lt;UserCreateDTO&gt; representing the asynchronous operation.</returns>
        /// <exception cref="CustomExceptions.Exceptions.RoleNotFoundException"></exception>
    public async Task<UserCreateDTO> CreateAsync(UserCreateDTO userDTO)
        {
            var user = new User();
            user.UserName = userDTO.Email;
            user.NormalizedUserName = userDTO.Email.ToUpper();
            user.Email = userDTO.Email;
            user.NormalizedEmail = userDTO.Email.ToUpper();
            user.PhoneNumber = userDTO.PhoneNumber;
            user.FirstName = userDTO.FirstName;
            user.LastName = userDTO.LastName;

            //Generate password
            const int passwordSymbols = 8;
            string password = GetRandomAlphanumericString(passwordSymbols);

            //Generate Security stamp
            const int securityStampSymbols = 15;
            user.SecurityStamp = GetRandomAlphanumericString(securityStampSymbols);

            //Hash the password
            var passHasher = new PasswordHasher<User>();
            user.PasswordHash = passHasher.HashPassword(user, password);//or user.security stamp

            //validate if the given User Role Exist in database
            var role = await this.context.Roles
                .FirstOrDefaultAsync(r => r.Id == userDTO.RoleId)
                ?? throw new RoleNotFoundException();


            this.context.Users.Add(user);
            await this.context.SaveChangesAsync();
            BindUserToRole(user, role);
            SendPasswordToNewUser(userDTO.Email, password);
            var userReturnDto = mapper.Map<UserCreateDTO>(user);
            return userReturnDto;
        }

        /// <summary>
        /// Sends password to new user .
        /// </summary>
        /// <param name="email">user's email.</param>
        /// <param name="password">user's password password.</param>
        public void SendPasswordToNewUser(string email, string password)
        {
            var emailData = new WelcomeRequest();
            emailData.UserName = email;
            emailData.ToEmail = email;
            emailData.Password = password;
            this.emailService.SendWelcomeEmailAsync(emailData);
        }

        /// <summary>
        /// Binds user to role.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="role">The role.</param>
        public async void BindUserToRole(User user, Role role )
        {
            var userRole = new IdentityUserRole<int>();
            userRole.UserId = user.Id;
            userRole.RoleId = role.Id;
            this.context.UserRoles.Add(userRole);
            await this.context.SaveChangesAsync();        
        }

        /// <summary>
        /// Binds the user to role.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="roleName">Name of the role.</param>
        /// <exception cref="CustomExceptions.Exceptions.RoleNotFoundException"></exception>
        public async void BindUserToRole(User user, string roleName)
        {
           
            var role = await this.context.Roles
                .FirstOrDefaultAsync(r => r.NormalizedName == roleName.ToUpper())
                ?? throw new RoleNotFoundException();


            var userRole = new IdentityUserRole<int>(); //this method is doing problems
            userRole.UserId = user.Id;
            userRole.RoleId = role.Id;
            this.context.UserRoles.Add(userRole);
            await this.context.SaveChangesAsync();
        }

        /// <summary>
        /// Gets random alphanumeric string for the password, the value is given to get random string to create password.
        /// </summary>
        /// <param name="length">The length.</param>
        /// <returns>System.String.</returns>
        public string GetRandomAlphanumericString(int length)
        {
            const string alphanumericCharacters =
                "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
                "abcdefghijklmnopqrstuvwxyz" +
                "0123456789"                 +
                "@#$%^*&!?*,.<>=+";
            return GetRandomString(length, alphanumericCharacters);
        }

        /// <summary>
        /// Gets the random string for the password.
        /// </summary>
        /// <param name="length">The length of the password.</param>
        /// <param name="characterSet">The character set for the password from method get random alpha numeric string.</param>
        /// <returns>System.String.</returns>
        /// <exception cref="System.ArgumentException">length must not be negative - length</exception>
        /// <exception cref="System.ArgumentException">length is too big - length</exception>
        /// <exception cref="System.ArgumentException">characterSet must not be empty - characterSet</exception>
        /// <exception cref="System.ArgumentNullException">characterSet</exception>
        private static string GetRandomString(int length, IEnumerable<char> characterSet)
        {
            if (length < 0)
                throw new ArgumentException("length must not be negative", "length");
            if (length > int.MaxValue / 8) // 250 million chars ought to be enough for anybody
                throw new ArgumentException("length is too big", "length");
            if (characterSet == null)
                throw new ArgumentNullException("characterSet");
            var characterArray = characterSet.Distinct().ToArray();
            if (characterArray.Length == 0)
                throw new ArgumentException("characterSet must not be empty", "characterSet");

            var bytes = new byte[length * 8];
            new RNGCryptoServiceProvider().GetBytes(bytes);
            var result = new char[length];

            for (int i = 0; i < length; i++)
            {
                ulong value = BitConverter.ToUInt64(bytes, i * 8);
                result[i] = characterArray[value % (uint)characterArray.Length];
            }

            return new string(result);
        }
    }
}
