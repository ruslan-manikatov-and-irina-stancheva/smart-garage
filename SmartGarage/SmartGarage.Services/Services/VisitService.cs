﻿using AutoMapper;
using CustomExceptions.Exceptions;
using Microsoft.EntityFrameworkCore;
using SmartGarage.Data;
using SmartGarage.Data.Models;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Services.Services
{
    /// <summary>
    /// Class VisitService.
    /// Implements the <see cref="SmartGarage.Services.Contracts.IVisitService" />
    /// </summary>
    /// <seealso cref="SmartGarage.Services.Contracts.IVisitService" />
    public class VisitService : IVisitService
    {
        private readonly SmartGarageDbContext context;
        private readonly IMapper mapper;
        private readonly IVisitServicesService visitServicesService;

        /// <summary>
        /// Initializes a new instance of the <see cref="VisitService"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="mapper">The mapper.</param>
        /// <param name="visitServicesService">The visit services service.</param>
        public VisitService(SmartGarageDbContext context, IMapper mapper, IVisitServicesService visitServicesService)
        {
            this.context = context;
            this.mapper = mapper;
            this.visitServicesService = visitServicesService;
        }

        /// <summary>
        /// Get all visits as an asynchronous operation.
        /// </summary>
        /// <returns>A Task&lt;ICollection`1&gt; representing the asynchronous operation.</returns>
        public async Task<ICollection<VisitDTO>> GetAllAsync()
        {
            var allVisitsDTO = await this.context.Visits
                .Select(v => new VisitDTO
                {
                    VisitId = v.VisitId,
                    VehicleId = v.VehicleId,
                    VisitStatus = v.VisitStatus.Name,
                    ArriveDate = v.ArriveDate,
                    DepartureDate = v.DepartureDate,
                }).ToListAsync();

            return allVisitsDTO;
        }

        /// <summary>
        /// Get visit by visit Id as an asynchronous operation.
        /// </summary>
        /// <param name="visitId">The identifier.</param>
        /// <returns>A Task&lt;VisitFullReportDTO&gt; representing the asynchronous operation.</returns>
        /// <exception cref="System.ArgumentNullException"></exception>
        public async Task<VisitFullReportDTO> GetAsync(int visitId)
        {
            var visitDTO = await this.context.Visits
                .Where(v => v.VisitId == visitId)
                .Select(v => new VisitFullReportDTO
                {
                    VisitId = v.VisitId,
                    VisitStatus = v.VisitStatus.Name,
                    ArriveDate = v.ArriveDate,
                    DepartureDate = v.DepartureDate,
                    OwnerId = v.Vehicle.OwnerId,
                    OwnerUsername = v.Vehicle.Owner.UserName,
                    FirstName = v.Vehicle.Owner.FirstName,
                    LastName = v.Vehicle.Owner.LastName,
                    PhoneNumber = v.Vehicle.Owner.PhoneNumber,
                    VehicleId = v.VehicleId,
                    CreatedOn = v.Vehicle.CreatedOn,
                    Vin = v.Vehicle.Vin,
                    RegistrationPlate = v.Vehicle.RegistrationPlate,
                    Model = v.Vehicle.Model.Name,
                    Manufacturer = v.Vehicle.Model.Manufacturer.Name,
                    Year = v.Vehicle.Year,
                    VehicleType = v.Vehicle.VehicleType.Name,
                    VisitServices = v.VisitServices.Where(vs => vs.VisitId == v.VisitId)
                        .Select(sv => new ServiceDTO
                        {
                            ServiceId = sv.ServiceId,
                            Name = sv.Service.Name,
                            Price = sv.Service.Price * v.Vehicle.VehicleType.Coefficient
                        }).ToList(),
                })
                .FirstOrDefaultAsync()
                ?? throw new NotFoundException();

            visitDTO.TotalPrice = visitDTO.VisitServices.Sum(v => v.Price);

            return visitDTO;
        }

        /// <summary>
        /// Create as an asynchronous operation.
        /// </summary>
        /// <param name="dto">The dto.</param>
        /// <returns>A Task&lt;VisitDTO&gt; representing the asynchronous operation.</returns>
        /// <exception cref="System.ArgumentNullException"></exception>
        /// <exception cref="CustomExceptions.Exceptions.WrongDateTimeException"></exception>
        public async Task<VisitDTO> CreateAsync(VisitCreateDTO dto)
        {
            var vehicleCheck = await this.context.Vehicles
                .FirstOrDefaultAsync(v => v.VehicleId == dto.VehicleId)
                ?? throw new NotFoundException();

            if (dto.DepartureDate <= DateTime.Now)
                throw new WrongDateTimeException();

            var visit = mapper.Map<Visit>(dto);

            this.context.Visits.Add(visit);

            await this.context.SaveChangesAsync();

            var visitDto = mapper.Map<VisitDTO>(visit);

            return visitDto;
        }

        public async Task<VisitCreateDTO> CreateAsync(VisitCreateDTO dto, ICollection<int> services)
        {
            var vehicleCheck = await this.context.Vehicles
                .FirstOrDefaultAsync(v => v.VehicleId == dto.VehicleId)
                ?? throw new NotFoundException();

            if (dto.DepartureDate <= DateTime.Now)
                throw new WrongDateTimeException();

            var visit = mapper.Map<Visit>(dto);

            this.context.Visits.Add(visit);

            await this.context.SaveChangesAsync();

            if (services.Count > 0)
                await this.visitServicesService.CreateAsync(visit.VisitId, services);

            return dto;
        }

        public async Task<VisitDTO> UpdateAsync(int visitId, VisitUpdateDTO dto)
        {
            var visit = await this.context.Visits
               .FirstOrDefaultAsync(v => v.VisitId == visitId)
               ?? throw new NotFoundException();

            var vehicleId = dto.VehicleId;
            var visitStatusId = dto.VisitStatusId;
            var departureDate = dto.DepartureDate;
            var visitServices = mapper.Map<ICollection<Service>>(dto.VisitServices);

            if (vehicleId > 0)
                visit.VehicleId = vehicleId;
            if (visitStatusId > 0)
                visit.VisitStatusId = dto.VisitStatusId;
            if (departureDate != DateTime.MinValue)
            {
                if (departureDate <= visit.ArriveDate)
                    throw new WrongDateTimeException();
                else
                    visit.DepartureDate = dto.DepartureDate;
            }
            if (visitServices.Count() > 0)
            {
                if (visit.VisitServices.Count() > 0)
                    await this.visitServicesService.DeleteByVisitIdAsync(visitId);

                await this.visitServicesService.CreateAsync(visitId, visitServices);
            }

            await this.context.SaveChangesAsync();

            var visitDto = mapper.Map<VisitDTO>(visit);

            return visitDto;
        }

        public async Task<VisitUpdateDTO> UpdateAsync(int visitId, VisitUpdateDTO dto, ICollection<int> services)
        {
            var visit = await this.context.Visits
               .FirstOrDefaultAsync(v => v.VisitId == visitId)
               ?? throw new NotFoundException();

            var vehicleId = dto.VehicleId;
            var visitStatusId = dto.VisitStatusId;
            var departureDate = dto.DepartureDate;

            if (vehicleId > 0)
                visit.VehicleId = vehicleId;
            if (visitStatusId > 0)
                visit.VisitStatusId = visitStatusId;
            if (departureDate != DateTime.MinValue)
            {
                if (departureDate <= visit.ArriveDate)
                    throw new WrongDateTimeException();
                else
                    visit.DepartureDate = departureDate;
            }

            await this.visitServicesService.DeleteByVisitIdAsync(visitId);

            if (services.Count > 0)
                await this.visitServicesService.CreateAsync(visitId, services);

            return dto;
        }

        /// <summary>
        /// Delete visit as an asynchronous operation.
        /// </summary>
        /// <param name="visitId">The identifier.</param>
        /// <returns>A Task representing the asynchronous operation.</returns>
        /// <exception cref="System.ArgumentNullException"></exception>
        public async Task DeleteAsync(int visitId)
        {
            var visit = await this.context.Visits
               .FirstOrDefaultAsync(v => v.VisitId == visitId)
               ?? throw new NotFoundException();

            visit.IsDeleted = true;

            await this.context.SaveChangesAsync();
        }
    }
}
