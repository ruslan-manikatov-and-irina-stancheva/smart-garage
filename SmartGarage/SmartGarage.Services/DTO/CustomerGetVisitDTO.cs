﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartGarage.Services.DTO
{
    public class CustomerGetVisitDTO
    {
        public  CustomerGetVisitDTO() 
        {
            this.ServiceDTOs = new HashSet<ServiceDTO>();
        }
        public CustomerGetVisitDTO(int customerId, string firstName, string lastName, string email,
            string vehicleModelName, string VehicleVin, string vehicleType, int visitId, 
            DateTime arivalDate, string visitStatusName, decimal vehicleTypeCoefficient, 
            ICollection<ServiceDTO> serviceDTOs, decimal visitCost)
        {

            this.CustomerId = customerId;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Email = email;

            this.VehicleModelName = vehicleModelName;
            this.VehicleVin = VehicleVin;
            this.vehicleType = vehicleType;

            this.VisitId = visitId;
            this.ArriveDate = arivalDate;
            this.VisitStatusName = visitStatusName;
            this.VehicleTypeCoefficient = vehicleTypeCoefficient;

            this.ServiceDTOs = new List<ServiceDTO>();
            this.ServiceDTOs = serviceDTOs;

            this.VisitCost = visitCost;
            this.VisitCurrencyPrice = "EUR";
        }
        //Constructor with passing and the new currency
        public CustomerGetVisitDTO(int customerId, string firstName, string lastName, string email,
            string vehicleModelName, string VehicleVin, string vehicleType, int visitId,
            DateTime arivalDate, string visitStatusName, decimal vehicleTypeCoefficient,
            ICollection<ServiceDTO> serviceDTOs, decimal visitCost, string currency)
        {

            this.CustomerId = customerId;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Email = email;

            this.VehicleModelName = vehicleModelName;
            this.VehicleVin = VehicleVin;
            this.vehicleType = vehicleType;
            this.VehicleTypeCoefficient = vehicleTypeCoefficient;

            this.VisitId = visitId;
            this.ArriveDate = arivalDate;
            this.VisitStatusName = visitStatusName;

            this.ServiceDTOs = new List<ServiceDTO>();
            this.ServiceDTOs = serviceDTOs;

            this.VisitCost = visitCost;
            this.VisitCurrencyPrice = currency;
        }
        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }

        public string VehicleModelName { get; set; }
        public string VehicleVin { get; set; }
        public string vehicleType { get; set; }

        public int VisitId { get; set; }
        public DateTime ArriveDate { get; set; }
        public string VisitStatusName { get; set; }
        public string VisitCurrencyPrice { get; set; }
        public decimal VisitCost { get; set; }

        public decimal VehicleTypeCoefficient { get; set; }

        public ICollection<ServiceDTO> ServiceDTOs { get; set; }
        


        /*public ICollection<int> ServicesId { get; set; }
        public ICollection<string> ServiceName { get; set; }
        public ICollection<float> ServicePrice { get; set; }*/

    }
}
