﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartGarage.Services.DTO
{
    public class ForgottenPasswordDataDTO
    {
        public string Email { get; set; }
        public string ValidationCode { get; set; }
        public string NewPassword { get; set; }
        public string RepeatNewPassword { get; set; }

    }
}
