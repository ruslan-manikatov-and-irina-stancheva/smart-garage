﻿using SmartGarage.Data.Models;
using System;
using System.Collections.Generic;

namespace SmartGarage.Services.DTO
{
    public class VisitDTO
    {
        public VisitDTO() 
        {
            this.VisitServices = new HashSet<ServiceDTO>();
        }

        public int VisitId { get; set; }
        public int VehicleId { get; set; }
        public string VisitStatus { get; set; }
        public DateTime ArriveDate { get; set; }
        public DateTime DepartureDate { get; set; }
        public ICollection<ServiceDTO> VisitServices { get; set; }
    }
}
