﻿using SmartGarage.Data.Models;
using System;
using System.Collections.Generic;

namespace SmartGarage.Services.DTO
{
    public class VisitCreateDTO
    {
        public VisitCreateDTO()
        {
            this.VisitServices = new HashSet<ServiceDTO>();
        }

        public int VehicleId { get; set; }
        public DateTime DepartureDate { get; set; }
        //public ICollection<VisitServices> VisitServices { get; set; }
        public ICollection<ServiceDTO> VisitServices { get; set; }
    }
}
