﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SmartGarage.Services.DTO
{
    public class VehicleDetailsDTO
    {
        public VehicleDetailsDTO()
        {
            this.Visits = new List<int>();
        }

        public int VehicleId { get; set; }
        public string Vin { get; set; }
        public string RegistrationPlate { get; set; }
        public int OwnerId { get; set; }
        public string Owner { get; set; }
        public int ModelId { get; set; }
        public string Model { get; set; }
        public string Manufacturer { get; set; }

        //[DisplayFormat(DataFormatString = "{0:MM/yyyy}")]
        public DateTime Year { get; set; }
        public DateTime CreatedOn { get; set; }
        public string VehicleType { get; set; }
        public int VehicleTypeId { get; set; }
        public ICollection<int> Visits { get; set; }
    }
}
