﻿using SmartGarage.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SmartGarage.Services.DTO
{
    public class CustomerGetAllServicesDTO
    {
        public CustomerGetAllServicesDTO()
        {

        }
        public CustomerGetAllServicesDTO(ICollection<Service> service, int visitId, DateTime dateTime,
            string visitStatus, string model, string vin, string registrationPlate)
        {
            this.ServicesId = new List<int>();
            this.ServiceName = new List<string>();
            this.ServicePrice = new List<decimal>();

            //input another DTO here to make it more clean to see
            this.ServicesId = service.Select(s => s.ServiceId).ToList();
            this.ServiceName = service.Select(s => s.Name).ToList();
            this.ServicePrice = service.Select(s => s.Price).ToList();

            this.VisitId = visitId;
            this.VisitStatusName = visitStatus;
            this.ArriveDate = ArriveDate;
            this.VehicleModelName = model;
            this.VehicleVin = vin;
            this.ArriveDate = dateTime;
            this.VehicleRegistrationPlate = registrationPlate;
        }
        public int VisitId { get; set; }
        public DateTime ArriveDate { get; set; }
        public string VisitStatusName { get; set; }
        public string VehicleModelName { get; set; }
        public string VehicleVin { get; set; }
        public string VehicleRegistrationPlate { get; set; }
        public ICollection<decimal> ServicePrice { get; set; }
        public ICollection<int> ServicesId { get; set; }
        public ICollection<string> ServiceName { get; set; }
    }
}
