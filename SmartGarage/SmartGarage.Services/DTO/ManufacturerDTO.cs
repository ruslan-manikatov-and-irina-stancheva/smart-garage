﻿namespace SmartGarage.Services.DTO
{
    public class ManufacturerDTO
    {
        public int ManufacturerId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }
}
