﻿using System;

namespace SmartGarage.Services.DTO
{
    public class CustomerVehiclesDTO
    {
        public int VehicleId { get; set; }
        public string RegistrationPlate { get; set; }
        public string Vin { get; set; }
        public DateTime VehicleYear { get; set; }
        public string ModelName { get; set; }
        public string ModelManufacturerName { get; set; }
    }
}
