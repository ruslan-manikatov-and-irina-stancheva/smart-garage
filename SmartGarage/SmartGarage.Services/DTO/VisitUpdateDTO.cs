﻿using SmartGarage.Data.Models;
using System;
using System.Collections.Generic;

namespace SmartGarage.Services.DTO
{
    public class VisitUpdateDTO
    {
        public VisitUpdateDTO()
        {
            this.VisitServices = new HashSet<ServiceDTO>();
        }

        public int VisitId { get; set; }
        public int VehicleId { get; set; }
        public string RegistrationPlate { get; set; }
        public int VisitStatusId { get; set; }
        public string VisitStatus { get; set; }
        public DateTime DepartureDate { get; set; }
        public ICollection<ServiceDTO> VisitServices { get; set; }
    }
}
