﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartGarage.Services.DTO
{
    public class VehicleUpdateDTO
    {
        public string Vin { get; set; }
        public string RegistrationPlate { get; set; }
        public int OwnerId { get; set; }
        public CarModelDTO ManufacturerModel { get; set; }
        public DateTime Year { get; set; }
        public string VehicleType { get; set; }
    }
}
