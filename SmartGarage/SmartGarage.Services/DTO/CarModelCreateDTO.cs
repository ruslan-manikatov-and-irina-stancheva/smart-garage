﻿namespace SmartGarage.Services.DTO
{
    public class CarModelCreateDTO
    {
        public int ModelId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public int ManufacturerId { get; set; }
    }
}
