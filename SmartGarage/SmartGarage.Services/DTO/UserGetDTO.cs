﻿using System;

namespace SmartGarage.Services.DTO
{
    public class UserGetDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime CreatedOn { get; set; }
        public string Email {get; set;}
    }
}

