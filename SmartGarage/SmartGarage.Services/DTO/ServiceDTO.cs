﻿using SmartGarage.Data.Models;
using System.Collections.Generic;

namespace SmartGarage.Services.DTO
{
    public class ServiceDTO
    {
        public ServiceDTO() 
        {
            this.VehicleTypes = new List<int>();
            this.Currency = "EUR";
        }

        public int ServiceId { get; set; }
        public string Name { get; set; }
        public ICollection<int> VehicleTypes { get; set; }
        public decimal Price { get; set; }
        public string Currency { get; set; }
    }
}
