﻿namespace SmartGarage.Services.DTO
{
    public class VisitStatusDTO
    {
        public int VisitStatusId { get; set; }
        public string Name { get; set; }
    }
}
