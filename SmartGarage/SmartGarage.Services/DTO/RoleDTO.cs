﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartGarage.Services.DTO
{
    public class RoleDTO
    {
        public int RoleId { get; set; }
        public string Name { get; set; }
    }
}
