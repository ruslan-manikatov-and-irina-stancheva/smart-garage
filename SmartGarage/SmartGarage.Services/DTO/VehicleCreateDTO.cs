﻿using System;

namespace SmartGarage.Services.DTO
{
    public class VehicleCreateDTO
    {
        public int VehicleId { get; set; }
        public string Vin { get; set; }
        public string RegistrationPlate { get; set; }
        public int OwnerId { get; set; }
        public int ModelId { get; set; }
        public DateTime Year { get; set; }
        public int VehicleTypeId { get; set; }
    }
}
