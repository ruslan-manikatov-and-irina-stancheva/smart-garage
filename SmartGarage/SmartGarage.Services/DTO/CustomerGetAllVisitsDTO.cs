﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartGarage.Services.DTO
{
    public class CustomerGetAllVisitsDTO
    {

        public CustomerGetAllVisitsDTO()
        {
            this.CustomerGetVisitDTOs = new HashSet<CustomerGetVisitDTO>();
        }

        public decimal FullPrice { get; set; }
        public string Currency { get; set; }
        public ICollection<CustomerGetVisitDTO> CustomerGetVisitDTOs { get; set; }
    }
}
