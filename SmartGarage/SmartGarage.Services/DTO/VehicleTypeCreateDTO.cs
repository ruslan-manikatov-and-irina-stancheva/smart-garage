﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartGarage.Services.DTO
{
    public class VehicleTypeCreateDTO
    {
        public string Name { get; set; }
        public decimal Coefficient { get; set; }
    }
}
