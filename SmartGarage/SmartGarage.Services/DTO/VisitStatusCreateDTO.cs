﻿namespace SmartGarage.Services.DTO
{
    public class VisitStatusCreateDTO
    {
        public string Name { get; set; }
    }
}
