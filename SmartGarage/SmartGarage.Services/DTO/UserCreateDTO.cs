﻿namespace SmartGarage.Services.DTO
{
    public class UserCreateDTO
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public int RoleId { get; set; }
    }
}
