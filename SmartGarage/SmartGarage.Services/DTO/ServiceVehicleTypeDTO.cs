﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartGarage.Services.DTO
{
    public class ServiceVehicleTypeDTO
    {
        public ServiceVehicleTypeDTO()
        {
            this.Currency = "EUR";
        }

        public int ServiceId { get; set; }
        public string Name { get; set; }
        public string VehicleType { get; set; }
        public decimal Price { get; set; }
        public string Currency { get; set; }
    }
}
