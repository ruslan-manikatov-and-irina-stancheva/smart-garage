﻿namespace SmartGarage.Services.DTO
{
    public class VehicleTypeDTO
    {
        public int VehicleTypeId { get; set; }
        public string Name { get; set; }
        public decimal Coefficient { get; set; }
    }
}
