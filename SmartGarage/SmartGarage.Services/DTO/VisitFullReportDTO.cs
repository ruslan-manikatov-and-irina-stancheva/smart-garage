﻿using System;
using System.Collections.Generic;

namespace SmartGarage.Services.DTO
{
    public class VisitFullReportDTO
    {
        public VisitFullReportDTO()
        {
            this.VisitServices = new HashSet<ServiceDTO>();
            this.Currency = "EUR";
        }

        public int VisitId { get; set; }
        public string VisitStatus { get; set; }
        public DateTime ArriveDate { get; set; }
        public DateTime DepartureDate { get; set; }

        public int OwnerId { get; set; }
        public string OwnerUsername { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }

        public int VehicleId { get; set; }
        public DateTime CreatedOn { get; set; }
        public string Vin { get; set; }
        public string RegistrationPlate { get; set; }
        public string Model { get; set; }
        public string Manufacturer { get; set; }
        public DateTime Year { get; set; }
        public string VehicleType { get; set; }

        public ICollection<ServiceDTO> VisitServices { get; set; }
        public decimal TotalPrice { get; set; }
        public string Currency { get; set; }
    }
}
