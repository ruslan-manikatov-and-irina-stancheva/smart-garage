﻿

namespace SmartGarage.Services.DTO
{
    public class PDFVisitDto
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string Currency { get; set; }
    }
}
