﻿namespace SmartGarage.Services.DTO
{
    public class ManufacturerCreateDTO
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }
}
