﻿using System.Collections.Generic;

namespace SmartGarage.Services.DTO
{
    public class ServiceUpdateDTO
    {
        public ServiceUpdateDTO()
        {
            this.VehicleTypes = new HashSet<VehicleTypeDTO>();
        }

        public int ServiceId { get; set; }
        public string Name { get; set; }
        public ICollection<VehicleTypeDTO> VehicleTypes { get; set; }
        public decimal Price { get; set; }
    }
}
