﻿using System.Collections.Generic;

namespace SmartGarage.Services.DTO
{
    public class ServiceCreateDTO
    {
        public ServiceCreateDTO()
        {
            this.VehicleTypes = new HashSet<VehicleTypeDTO>();
        }

        public string Name { get; set; }
        public ICollection<VehicleTypeDTO> VehicleTypes { get; set; }
        public decimal Price { get; set; }
    }
}
