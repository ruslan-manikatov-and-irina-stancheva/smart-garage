﻿namespace SmartGarage.Services.DTO
{
    public class CarModelDTO
    {
        public int ModelId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public int ManufacturerId { get; set; }
        public string ManufacturerName { get; set; }
    }
}