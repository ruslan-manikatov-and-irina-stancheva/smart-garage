﻿using System.Collections.Generic;
using SmartGarage.Data.Models;
using SmartGarage.Services.DTO;
using AutoMapper;

namespace SmartGarage.Services.DTOMappers
{
    public class DTOMapper : Profile
    {
        public DTOMapper()
        {
            CreateMap<Manufacturer, ManufacturerDTO>().ReverseMap();
            CreateMap<ManufacturerCreateDTO, Manufacturer>().ReverseMap();
            CreateMap<IEnumerable<Manufacturer>, IEnumerable<ManufacturerDTO>>();
            

            CreateMap<CarModelDTO, Model>().ReverseMap();
            CreateMap<Model, CarModelCreateDTO>().ReverseMap();
            CreateMap<User, UserGetDTO>().ReverseMap();
            CreateMap<User, UserDTO>().ReverseMap();
            CreateMap<User, UserCreateDTO>().ReverseMap();
            CreateMap<Vehicle, CustomerVehiclesDTO>().ReverseMap();
            CreateMap<CustomerGetAllServicesDTO, Service>().ReverseMap();

            CreateMap<Vehicle, UserGetDTO>().ReverseMap();

            CreateMap<Model, CarModelDTO>().ReverseMap();
            CreateMap<Model, CarModelCreateDTO>().ReverseMap();

            CreateMap<Manufacturer, ManufacturerDTO>().ReverseMap();
            CreateMap<Manufacturer, ManufacturerCreateDTO>().ReverseMap();
            //CreateMap<IEnumerable<Manufacturer>, IEnumerable<ManufacturerDTO>>(); // unnecessarily

            CreateMap<Service, ServiceDTO>().ReverseMap();
            CreateMap<Service, ServiceVehicleTypeDTO>().ReverseMap();
            CreateMap<Service, ServiceCreateDTO>().ReverseMap();
            CreateMap<ServiceDTO,PDFVisitDto>().ReverseMap();

            CreateMap<VehicleType, VehicleTypeDTO>().ReverseMap();
            CreateMap<VehicleType, VehicleTypeCreateDTO>().ReverseMap();

            CreateMap<Visit, VisitDTO>();
            CreateMap<VisitCreateDTO, Visit>();

            CreateMap<VisitStatus, VisitStatusDTO>().ReverseMap();
            CreateMap<VisitStatus, VisitStatusCreateDTO>().ReverseMap();

            CreateMap<Model, CarModelDTO>().ReverseMap();
            CreateMap<Model, CarModelCreateDTO>().ReverseMap();

            CreateMap<Vehicle, UserGetDTO>().ReverseMap();

            CreateMap<Role, RoleDTO>().ReverseMap();
        }
    }
}
