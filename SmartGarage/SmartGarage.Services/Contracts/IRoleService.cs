﻿using SmartGarage.Services.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SmartGarage.Services.Contracts
{
    public interface IRoleService
    {
        /// <summary>
        /// Gets all user roles asynchronous.
        /// </summary>
        /// <returns>Task&lt;IEnumerable&lt;RoleDTO&gt;&gt;.</returns>
        Task<IEnumerable<RoleDTO>> GetAllAsync();
    }
}
