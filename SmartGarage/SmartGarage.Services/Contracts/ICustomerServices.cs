﻿using SmartGarage.Services.DTO;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SmartGarage.Services.Contracts
{
    /// <summary>
    /// Interface ICustomerServices
    /// </summary>
    public interface ICustomerServices
    {
        /// <summary>
        /// Gets all vehicles.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <returns>Customer dto with all vehicles for a customer</returns>
        Task<ICollection<CustomerVehiclesDTO>> GetAllVehicles(string username);
        /// <summary>
        /// Gets all services.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <returns>Customer dto with all services for a customer</returns>
        Task<ICollection<CustomerGetAllServicesDTO>> GetAllServices(string username);
        /// <summary>
        /// Filters the services by registration plate.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="vin">The vin.</param>
        /// <returns>Task&lt;ICollection&lt;CustomerGetAllServicesDTO&gt;&gt;.</returns>
        Task<ICollection<CustomerGetAllServicesDTO>> FilterServicesByRegPlate(string username, string vin);
        /// <summary>
        /// Filters the services by date.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="date">The date.</param>
        /// <returns>Task&lt;ICollection&lt;CustomerGetAllServicesDTO&gt;&gt;.</returns>
        Task<ICollection<CustomerGetAllServicesDTO>> FilterServicesByDate(string username, DateTime? date);
        /// <summary>
        /// Filters the services by registration plate and date.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="redPlate">The red plate.</param>
        /// <param name="date">The date.</param>
        /// <returns>Task&lt;ICollection&lt;CustomerGetAllServicesDTO&gt;&gt;.</returns>
        Task<ICollection<CustomerGetAllServicesDTO>> FilterServicesByRegPlateAndDate(string username, string redPlate, DateTime? date);
        /// <summary>
        /// Gets the visit report by identifier.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="visitId">The visit identifier.</param>
        /// <returns>Task&lt;CustomerGetVisitDTO&gt;.</returns>
        Task<CustomerGetVisitDTO> GetVisitReportById(string username, int? visitId);
        /// <summary>
        /// Gets the visit report by identifier and currency.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="visitId">The visit identifier.</param>
        /// <param name="currency">The currency.</param>
        /// <returns>Task&lt;CustomerGetVisitDTO&gt;.</returns>
        Task<CustomerGetVisitDTO> GetVisitReportByIdAndCurrency(string username, int? visitId, string currency);
        /// <summary>
        /// Gets all visits report.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="currency">The currency.</param>
        /// <returns>Task&lt;CustomerGetAllVisitsDTO&gt;.</returns>
        Task<CustomerGetAllVisitsDTO> GetAllVisitsReport(string username, string currency);
    }
}
