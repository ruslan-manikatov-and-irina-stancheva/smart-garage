﻿using SmartGarage.Services.EmailConfigurationModels;
using System.Threading.Tasks;

namespace SmartGarage.Services.Contracts
{
    /// <summary>
    /// Interface IEmailService
    /// </summary>
    public interface IEmailService
    {
        /// <summary>
        /// Sends email asynchronous to given email.
        /// </summary>
        /// <param name="mailRequest">The mail request.</param>
        /// <returns>Task&lt;System.Boolean&gt;.</returns>
        Task<bool> SendEmailAsync(MailRequest mailRequest);
        /// <summary>
        /// Sends welcome email asynchronous.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>Task.</returns>
        Task SendWelcomeEmailAsync(WelcomeRequest request);

    }
}
