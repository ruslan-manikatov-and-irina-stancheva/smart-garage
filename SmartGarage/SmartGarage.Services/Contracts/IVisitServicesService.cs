﻿using SmartGarage.Data.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Services.Contracts
{
    public interface IVisitServicesService
    {
        /// <summary>
        /// Gets all visit services .
        /// </summary>
        /// <returns>IQueryable&lt;VisitServices&gt;.</returns>
        IQueryable<VisitServices> GetAll();
        /// <summary>
        /// Gets visit services by specified visit identifier.
        /// </summary>
        /// <param name="visitId">The visit identifier.</param>
        /// <returns>IQueryable&lt;VisitServices&gt;.</returns>
        IQueryable<VisitServices> Get(int visitId);
        /// <summary>
        /// Creates visit services  asynchronous.
        /// </summary>
        /// <param name="visitId">The visit identifier.</param>
        /// <param name="services">The services.</param>
        /// <returns>Task.</returns>
        Task CreateAsync(int visitId, ICollection<Service> services);
        Task CreateAsync(int visitId, ICollection<int> services);
        Task DeleteByVisitIdAsync(int visitId);
        /// <summary>
        /// Deletes visit services  by service identifier asynchronous.
        /// </summary>
        /// <param name="serviceId">The service identifier.</param>
        /// <returns>Task.</returns>
        Task DeleteByServiceIdAsync(int serviceId);

        //Task<IEnumerable<VisitServices>> GetAllAsync();
        //Task<IEnumerable<VisitServices>> GetAsync(int visitId);
        //Task CreateAsync(int visitId, ICollection<Service> services);
        //Task DeleteByVisitIdAsync(int visitId);
        //Task DeleteByServiceIdAsync(int serviceId);
    }
}
