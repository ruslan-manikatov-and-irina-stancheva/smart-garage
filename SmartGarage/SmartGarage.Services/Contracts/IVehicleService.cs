﻿using SmartGarage.Services.DTO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Services.Contracts
{
   
    public interface IVehicleService
    {
        /// <summary>
        /// Gets all vehicle asynchronous.
        /// </summary>
        /// <returns>Task&lt;ICollection&lt;VehicleDTO&gt;&gt;.</returns>
        Task<ICollection<VehicleDTO>> GetAllAsync();
        /// <summary>
        /// Gets vehicle by id asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Task&lt;VehicleDetailsDTO&gt;.</returns>
        Task<VehicleDetailsDTO> GetAsync(int id);
        /// <summary>
        /// Gets vehicle by customer id.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>IQueryable&lt;VehicleDTO&gt;.</returns>
        IQueryable<VehicleDTO> GetByCustomer(int id);
        /// <summary>
        /// Gets vehicle by customer username.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns>IQueryable&lt;VehicleDTO&gt;.</returns>
        IQueryable<VehicleDTO> GetByCustomer(string name);
        /// <summary>
        /// Creates vehicle asynchronous.
        /// </summary>
        /// <param name="dto">The dto.</param>
        /// <returns>Task&lt;VehicleCreateDTO&gt;.</returns>
        Task<VehicleCreateDTO> CreateAsync(VehicleCreateDTO dto);
        /// <summary>
        /// Updates vehicle asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="dto">The dto.</param>
        /// <returns>Task&lt;VehicleCreateDTO&gt;.</returns>
        Task<VehicleCreateDTO> UpdateAsync(int id, VehicleCreateDTO dto);
        /// <summary>
        /// Updates vehicle by customer dto asynchronous.
        /// </summary>
        /// <param name="vehicleId">The vehicle identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="dto">The dto.</param>
        /// <returns>Task&lt;VehicleUpdateByCustomerDTO&gt;.</returns>
        Task<VehicleUpdateByCustomerDTO> UpdateByCustomerAsync(int vehicleId, int customerId, VehicleUpdateByCustomerDTO dto);
        /// <summary>
        /// Deletes vehicle asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Task.</returns>
        Task DeleteAsync(int id);
    }
}
