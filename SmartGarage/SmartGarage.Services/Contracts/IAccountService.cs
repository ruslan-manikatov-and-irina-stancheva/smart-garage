﻿using SmartGarage.Services.AuthenticateModels;
using SmartGarage.Services.DTO;
using System.Threading.Tasks;

namespace SmartGarage.Services.Contracts
{
    /// <summary>
    /// Interface IAccountService
    /// </summary>
    public interface IAccountService
    {
        /// <summary>
        /// Authenticates user in database.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns>Task&lt;AuthenticateResponse&gt;.</returns>
        Task<AuthenticateResponse> AuthenticateAsync(AuthenticateRequest model);
        /// <summary>
        /// Sends the forgotten password email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <returns>Task&lt;System.String&gt;.</returns>
        Task<string> SendForgottenPasswordEmail(string email);
        /// <summary>
        /// Updates the forgotten password.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns>Task&lt;System.Boolean&gt;.</returns>
        Task<bool> UpdateForgottenPassword(ForgottenPasswordDataDTO model);

    }
}
