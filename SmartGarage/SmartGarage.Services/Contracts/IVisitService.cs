﻿using SmartGarage.Services.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SmartGarage.Services.Contracts
{
    public interface IVisitService
    {
        /// <summary>
        /// Gets all visits asynchronous.
        /// </summary>
        /// <returns>Task&lt;ICollection&lt;VisitDTO&gt;&gt;.</returns>
        Task<ICollection<VisitDTO>> GetAllAsync();
        /// <summary>
        /// Gets visits by given id asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Task&lt;VisitFullReportDTO&gt;.</returns>
        Task<VisitFullReportDTO> GetAsync(int id);
        /// <summary>
        /// Creates the asynchronous.
        /// </summary>
        /// <param name="dto">The dto.</param>
        /// <returns>Task&lt;VisitDTO&gt;.</returns>
        Task<VisitDTO> CreateAsync(VisitCreateDTO dto);
        Task<VisitCreateDTO> CreateAsync(VisitCreateDTO dto, ICollection<int> services);
        Task<VisitDTO> UpdateAsync(int id, VisitUpdateDTO dto);
        Task<VisitUpdateDTO> UpdateAsync(int visitId, VisitUpdateDTO dto, ICollection<int> services);
        /// <summary>
        /// Deletes visits asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Task.</returns>
        Task DeleteAsync(int id);
    }
}
