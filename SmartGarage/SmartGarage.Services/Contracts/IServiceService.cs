﻿using SmartGarage.Services.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SmartGarage.Services.Contracts
{
    public interface IServiceService
    {
        /// <summary>
        /// Gets all services asynchronous.
        /// </summary>
        /// <returns>Task&lt;ICollection&lt;ServiceDTO&gt;&gt;.</returns>
        Task<ICollection<ServiceDTO>> GetAllAsync();
        /// <summary>
        /// Gets services by id asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Task&lt;ServiceUpdateDTO&gt;.</returns>
        Task<ServiceUpdateDTO> GetAsync(int id);
        /// <summary>
        /// Gets service and type asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Task&lt;ICollection&lt;ServiceVehicleTypeDTO&gt;&gt;.</returns>
        Task<ICollection<ServiceVehicleTypeDTO>> GetServiceAndTypeAsync(int id);
        /// <summary>
        /// Gets services by name and price asynchronous.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="price">The price.</param>
        /// <returns>Task&lt;ICollection&lt;ServiceDTO&gt;&gt;.</returns>
        Task<ICollection<ServiceDTO>> GetByNameAndPriceAsync(string name, decimal price);
        /// <summary>
        /// Gets services by name asynchronous.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns>Task&lt;ICollection&lt;ServiceDTO&gt;&gt;.</returns>
        Task<ICollection<ServiceDTO>> GetByNameAsync(string name);
        /// <summary>
        /// Gets services by price asynchronous.
        /// </summary>
        /// <param name="price">The price.</param>
        /// <returns>Task&lt;ICollection&lt;ServiceDTO&gt;&gt;.</returns>
        Task<ICollection<ServiceDTO>> GetByPriceAsync(decimal price);
        /// <summary>
        /// Creates services asynchronous.
        /// </summary>
        /// <param name="dto">The dto.</param>
        /// <returns>Task&lt;ServiceCreateDTO&gt;.</returns>
        Task<ServiceCreateDTO> CreateAsync(ServiceCreateDTO dto);
        /// <summary>
        /// Creates services asynchronous.
        /// </summary>
        /// <param name="dto">The dto.</param>
        /// <param name="types">The types.</param>
        /// <returns>Task&lt;ServiceCreateDTO&gt;.</returns>
        Task<ServiceCreateDTO> CreateAsync(ServiceCreateDTO dto, List<int> types);
        /// <summary>
        /// Updates services asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="dto">The dto.</param>
        /// <returns>Task&lt;ServiceUpdateDTO&gt;.</returns>
        Task<ServiceUpdateDTO> UpdateAsync(int id, ServiceUpdateDTO dto);
        /// <summary>
        /// Updates services asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="dto">The dto.</param>
        /// <param name="types">The types.</param>
        /// <returns>Task&lt;ServiceUpdateDTO&gt;.</returns>
        Task<ServiceUpdateDTO> UpdateAsync(int id, ServiceUpdateDTO dto, List<int> types);
        Task DeleteAsync(int id);
    }
}
