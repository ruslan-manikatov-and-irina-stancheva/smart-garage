﻿using Microsoft.AspNetCore.Mvc;
using SmartGarage.Services.DTO;


namespace SmartGarage.Services.Contracts
{
    public interface IPdfGeneratorService
    {
        /// <summary>
        /// Generates PDF for one visit.
        /// </summary>
        /// <param name="customerDTO">The customer dto.</param>
        /// <returns>FileStreamResult.</returns>
        public FileStreamResult GeneratePdfForOneVisit(CustomerGetVisitDTO customerDTO);
        /// <summary>
        /// Generates PDF for all visits of customer.
        /// </summary>
        /// <param name="customerDTO">The customer dto.</param>
        /// <returns>FileStreamResult.</returns>
        public FileStreamResult GeneratePdfForAllVisitsOfCustomer(CustomerGetAllVisitsDTO customerDTO);
    }
}
