﻿using SmartGarage.Data.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Services.Contracts
{
    public interface IServiceVehicleTypesService
    {
        /// <summary>
        /// Gets all ServiceVehicleTypes
        /// </summary>
        /// <returns>IQueryable&lt;ServiceVehicleTypes&gt;.</returns>
        IQueryable<ServiceVehicleTypes> GetAll();
        /// <summary>
        /// Gets ServiceVehicleTypes specified service identifier.
        /// </summary>
        /// <param name="serviceId">The service identifier.</param>
        /// <returns>IQueryable&lt;ServiceVehicleTypes&gt;.</returns>
        IQueryable<ServiceVehicleTypes> Get(int serviceId);
        /// <summary>
        /// Creates ServiceVehicleTypes asynchronous.
        /// </summary>
        /// <param name="serviceId">The service identifier.</param>
        /// <param name="vehicleTypes">The vehicle types.</param>
        /// <returns>Task.</returns>
        Task CreateAsync(int serviceId, List<int> vehicleTypes);
        /// <summary>
        /// Deletes ServiceVehicleTypes by service identifier asynchronous.
        /// </summary>
        /// <param name="serviceId">The service identifier.</param>
        /// <returns>Void.</returns>
        Task DeleteByServiceIdAsync(int serviceId);
        /// <summary>
        /// Deletes ServiceVehicleTypes by vehicle type identifier asynchronous.
        /// </summary>
        /// <param name="vehicleTypeId">The vehicle type identifier.</param>
        /// <returns>Void.</returns>
        Task DeleteByVehicleTypeIdAsync(int vehicleTypeId);
    }
}
