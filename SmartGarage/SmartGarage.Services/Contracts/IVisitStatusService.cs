﻿using SmartGarage.Services.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SmartGarage.Services.Contracts
{
    /// <summary>
    /// Interface IVisitStatusService
    /// </summary>
    public interface IVisitStatusService
    {
        /// <summary>
        /// Gets visit statuses asynchronous.
        /// </summary>
        /// <returns>Task&lt;ICollection&lt;VisitStatusDTO&gt;&gt;.</returns>
        Task<ICollection<VisitStatusDTO>> GetAllAsync();
        /// <summary>
        /// Gets visit statuses asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Task&lt;VisitStatusDTO&gt;.</returns>
        Task<VisitStatusDTO> GetAsync(int id);
        /// <summary>
        /// Creates visit statuses asynchronous.
        /// </summary>
        /// <param name="dto">The dto.</param>
        /// <returns>Task&lt;VisitStatusDTO&gt;.</returns>
        Task<VisitStatusDTO> CreateAsync(VisitStatusCreateDTO dto);
        /// <summary>
        /// Updates visit statuses asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="dto">The dto.</param>
        /// <returns>Task&lt;VisitStatusDTO&gt;.</returns>
        Task<VisitStatusDTO> UpdateAsync(int id, VisitStatusDTO dto);
        /// <summary>
        /// Deletes visit statuses asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Task.</returns>
        Task DeleteAsync(int id);

        //Task<IEnumerable<VisitStatusDTO>> GetAllAsync();
        //Task<VisitStatusDTO> GetAsync(int id);
        //Task<VisitStatusDTO> CreateAsync(VisitStatusCreateDTO dto);
        //Task<VisitStatusDTO> UpdateAsync(int id, VisitStatusCreateDTO dto);
        //Task DeleteAsync(int id);
    }
}
