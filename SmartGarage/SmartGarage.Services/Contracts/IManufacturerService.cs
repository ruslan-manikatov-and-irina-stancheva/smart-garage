﻿using SmartGarage.Services.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SmartGarage.Services.Contracts
{
    public interface IManufacturerService
    {
        /// <summary>
        /// Gets all manufacturers asynchronous.
        /// </summary>
        /// <returns>Task&lt;ICollection&lt;ManufacturerDTO&gt;&gt;.</returns>
        Task<ICollection<ManufacturerDTO>> GetAllAsync();
        /// <summary>
        /// Gets manufacturers by given Id asynchronous.
        /// </summary>
        /// <param name="manufacturerId">The manufacturer identifier.</param>
        /// <returns>Task&lt;ManufacturerDTO&gt;.</returns>
        Task<ManufacturerDTO> GetAsync(int? manufacturerId);
        /// <summary>
        /// Creates manufacturers asynchronous.
        /// </summary>
        /// <param name="manufacturerDTO">The manufacturer dto.</param>
        /// <returns>Task&lt;ManufacturerCreateDTO&gt;.</returns>
        Task<ManufacturerCreateDTO> CreateAsync(ManufacturerCreateDTO manufacturerDTO);
        /// <summary>
        /// Updates manufacturers asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="manufacturerDTO">The manufacturer dto.</param>
        /// <returns>Task&lt;ManufacturerCreateDTO&gt;.</returns>
        Task<ManufacturerCreateDTO> UpdateAsync(int id, ManufacturerCreateDTO manufacturerDTO);
        /// <summary>
        /// Deletes manufacturers asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Task&lt;System.Boolean&gt;.</returns>
        Task<bool> DeleteAsync(int id);


    }
}
