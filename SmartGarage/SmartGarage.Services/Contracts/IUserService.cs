﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SmartGarage.Data.Models;
using SmartGarage.Services.DTO;

namespace SmartGarage.Services.Contracts
{
    public interface IUserService
    {
        /// <summary>
        /// Gets all users asynchronous.
        /// </summary>
        /// <returns>Task&lt;IEnumerable&lt;UserGetDTO&gt;&gt;.</returns>
        Task<IEnumerable<UserGetDTO>> GetAllAsync();
        /// <summary>
        /// Gets users by id asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Task&lt;UserGetDTO&gt;.</returns>
        Task<UserGetDTO> GetAsync(int? id);
        /// <summary>
        /// Updates users asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="modelDTO">The model dto.</param>
        /// <returns>Task&lt;UserDTO&gt;.</returns>
        Task<UserDTO> UpdateAsync(int id, UserDTO modelDTO);
        /// <summary>
        /// Deletes users asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Task&lt;System.Boolean&gt;.</returns>
        Task<bool> DeleteAsync(int id);
        /// <summary>
        /// Creates users asynchronous.
        /// </summary>
        /// <param name="userDTO">The user dto.</param>
        /// <returns>Task&lt;UserCreateDTO&gt;.</returns>
        Task<UserCreateDTO> CreateAsync(UserCreateDTO userDTO);
        /// <summary>
        /// Filters users by last name.
        /// </summary>
        /// <param name="lastName">The last name.</param>
        /// <returns>Task&lt;IEnumerable&lt;UserGetDTO&gt;&gt;.</returns>
        Task<IEnumerable<UserGetDTO>> FilterUsersByLastName(string lastName);
        /// <summary>
        /// Filters users by email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <returns>Task&lt;IEnumerable&lt;UserGetDTO&gt;&gt;.</returns>
        Task<IEnumerable<UserGetDTO>> FilterUsersByEmail(string email);
        /// <summary>
        /// Filters users by phone.
        /// </summary>
        /// <param name="phone">The phone.</param>
        /// <returns>Task&lt;IEnumerable&lt;UserGetDTO&gt;&gt;.</returns>
        Task<IEnumerable<UserGetDTO>> FilterUsersByPhone(string phone);
        /// <summary>
        /// Filters users by vehicle make.
        /// </summary>
        /// <param name="regPlate">The reg plate.</param>
        /// <returns>Task&lt;IEnumerable&lt;UserGetDTO&gt;&gt;.</returns>
        Task<IEnumerable<UserGetDTO>> FilterUsersByVehicleMake(string regPlate);
        /// <summary>
        /// Filters users by vehicle model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns>Task&lt;IEnumerable&lt;UserGetDTO&gt;&gt;.</returns>
        Task<IEnumerable<UserGetDTO>> FilterUsersByVehicleModel(string model);
        /// <summary>
        /// Filters users by visit in range.
        /// </summary>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <returns>Task&lt;IEnumerable&lt;UserGetDTO&gt;&gt;.</returns>
        Task<IEnumerable<UserGetDTO>> FilterUsersByVisitInRange(DateTime? fromDate, DateTime? toDate);
        /// <summary>
        /// Filters users by role.
        /// </summary>
        /// <param name="role">The role.</param>
        /// <returns>Task&lt;IEnumerable&lt;UserGetDTO&gt;&gt;.</returns>
        Task<IEnumerable<UserGetDTO>> FilterUsersByRole(string role);
        /// <summary>
        /// Sorts users by name.
        /// </summary>
        /// <returns>Task&lt;IEnumerable&lt;UserGetDTO&gt;&gt;.</returns>
        Task<IEnumerable<UserGetDTO>> SortUsersByName();
        /// <summary>
        /// Sorts users by name visit date.
        /// </summary>
        /// <returns>Task&lt;IEnumerable&lt;UserGetDTO&gt;&gt;.</returns>
        Task<IEnumerable<UserGetDTO>> SortUsersByVisitDate();

        /// <summary>
        /// Sends password to new user.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="password">The password.</param>
        void SendPasswordToNewUser(string email, string password);
        /// <summary>
        /// Binds user to role.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="role">The role.</param>
        void BindUserToRole(User user, Role role);
        /// <summary>
        /// Gets random alphanumeric string.
        /// </summary>
        /// <param name="length">The length.</param>
        /// <returns>System.String.</returns>
        string GetRandomAlphanumericString(int length);

        /// <summary>
        /// Binds the user to role.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="roleName">Name of the role.</param>
        void BindUserToRole(User user, string roleName);
    }
}
