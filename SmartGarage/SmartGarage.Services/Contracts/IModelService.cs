﻿using SmartGarage.Services.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SmartGarage.Services.Contracts
{

    public interface IModelService
    {
        /// <summary>
        /// Gets all vehicle models asynchronous.
        /// </summary>
        /// <returns>Task&lt;ICollection&lt;CarModelDTO&gt;&gt;.</returns>
        Task<ICollection<CarModelDTO>> GetAllAsync();
        /// <summary>
        /// Gets vehicle models by id asynchronous.
        /// </summary>
        /// <param name="modelId">The model identifier.</param>
        /// <returns>Task&lt;CarModelDTO&gt;.</returns>
        Task<CarModelDTO> GetAsync(int? modelId);
        /// <summary>
        /// Creates vehicle models asynchronous.
        /// </summary>
        /// <param name="CarModelDTO">The car model dto.</param>
        /// <returns>Task&lt;CarModelCreateDTO&gt;.</returns>
        Task<CarModelCreateDTO> CreateAsync(CarModelCreateDTO CarModelDTO);
        /// <summary>
        /// Updates vehicle models asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="modelDTO">The model dto.</param>
        /// <returns>Task&lt;CarModelCreateDTO&gt;.</returns>
        Task<CarModelCreateDTO> UpdateAsync(int id, CarModelCreateDTO modelDTO);
        /// <summary>
        /// Deletes vehicle models by id asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Task&lt;System.Boolean&gt;.</returns>
        Task<bool> DeleteAsync(int id);
    }
}
