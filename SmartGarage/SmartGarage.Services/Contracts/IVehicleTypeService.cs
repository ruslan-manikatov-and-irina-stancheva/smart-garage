﻿using SmartGarage.Services.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SmartGarage.Services.Contracts
{
    public interface IVehicleTypeService
    {
        /// <summary>
        /// Gets vehycle types asynchronous.
        /// </summary>
        /// <returns>Task&lt;ICollection&lt;VehicleTypeDTO&gt;&gt;.</returns>
        Task<ICollection<VehicleTypeDTO>> GetAllAsync();
        /// <summary>
        /// Gets all vehycle types asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Task&lt;VehicleTypeDTO&gt;.</returns>
        Task<VehicleTypeDTO> GetAsync(int id);
        /// <summary>
        /// Gets vehycle types selected by list of types id asynchronous.
        /// </summary>
        /// <param name="ids">The ids.</param>
        /// <returns>Task&lt;ICollection&lt;VehicleTypeDTO&gt;&gt;.</returns>
        Task<ICollection<VehicleTypeDTO>> GetSelectedAsync(ICollection<int> ids);
        /// <summary>
        /// Creates vehycle types asynchronous.
        /// </summary>
        /// <param name="dto">The dto.</param>
        /// <returns>Task&lt;VehicleTypeDTO&gt;.</returns>
        Task<VehicleTypeDTO> CreateAsync(VehicleTypeCreateDTO dto);
        /// <summary>
        /// Updates vehycle types asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="dto">The dto.</param>
        /// <returns>Task&lt;VehicleTypeDTO&gt;.</returns>
        Task<VehicleTypeDTO> UpdateAsync(int id, VehicleTypeDTO dto);
        /// <summary>
        /// Deletes vehycle types asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Task.</returns>
        Task DeleteAsync(int id);
    }
}
