﻿using SmartGarage.Services.DTO;
using System.Threading.Tasks;

namespace SmartGarage.Services.Contracts
{

    public interface ICurrencyService
    {
        /// <summary>
        /// Converts the currency.
        /// </summary>
        /// <param name="currency">The currency.</param>
        /// <param name="service">The service.</param>
        /// <returns>ServiceDTO with the converted currency;.</returns>
        Task<ServiceDTO> ConvertCurrency(string currency, ServiceDTO service);

    }
}
