﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartGarage.Services.AuthenticateModels
{
    public class AppSettings
    {
        public string Secret { get; set; }
    }
}
