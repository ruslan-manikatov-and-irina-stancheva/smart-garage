﻿using SmartGarage.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace SmartGarage.Services.AuthenticateModels
{
    public class AuthenticateResponse
    {
        public AuthenticateResponse(User user, string jwtToken)
        {
            this.Id = user.Id;
            this.FirstName = user.FirstName;
            this.LastName = user.LastName;
            this.Username = user.UserName;            
            this.JwtToken = jwtToken;
            //this.RefreshToken = refreshToken;
          
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string JwtToken { get; set; }



        /*        [JsonIgnore] // refresh token is returned in http only cookie
                public string RefreshToken { get; set; }*/
    }
}
