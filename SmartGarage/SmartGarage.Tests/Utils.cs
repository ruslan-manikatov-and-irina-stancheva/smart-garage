﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using SmartGarage.Data;
using SmartGarage.Data.Models;
using SmartGarage.Services.DTO;
using SmartGarage.Services.DTOMappers;
using Microsoft.AspNetCore.Identity;
using Moq;
using System;

namespace SmartGarage.Tests
{
    public class Utils
    {
        
        public static DbContextOptions<SmartGarageDbContext> GetOptions(string databaseName)
        {
            return new DbContextOptionsBuilder<SmartGarageDbContext>()
                .UseInMemoryDatabase(databaseName)
                .Options;
        }

        public static MapperConfiguration GetConfigurationMapper()
        {
            var myProfile = new DTOMapper();
            return new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
        }

        public static SmartGarageDbContext GetFakeDatabase(string databaseName)
        {
            var context = new SmartGarageDbContext(GetOptions(databaseName));

            context.Manufacturers.AddRange(GetManufacturers());
            context.Models.AddRange(GetModels());
            context.Users.AddRange(GetUsers());
            context.Vehicles.AddRange(GetVehicles());
            context.Visits.AddRange(GetVisits());
            context.Roles.AddRange(GetRoles());
            context.Services.AddRange(GetServices());
            context.VisitServices.AddRange(GetVisitServices());
            context.VisitStatuses.AddRange(GetVisitStatuses());
            context.VehicleTypes.AddRange(GetVehicleTypes());
            context.ServiceVehicleTypes.AddRange(GetServiceVehicleType());

            context.SaveChanges();
            return context;
        }

        public static ICollection<Manufacturer> GetManufacturers()
        {
            return new List<Manufacturer>
            {
               new Manufacturer
                {
                    ManufacturerId = 1,
                    Name = "Audi",
                    Code = "AUDI"
                },
               new Manufacturer
                {
                    ManufacturerId = 2,
                    Name = "BMW",
                    Code = "BMW"
                },
               new Manufacturer
                {
                    ManufacturerId = 3,
                    Name = "Volkswagen",
                    Code = "VW"
                },
               new Manufacturer
                {
                    ManufacturerId = 4,
                    Name = "Volvo",
                    Code = "VOLVO"
                },
               new Manufacturer
                {
                    ManufacturerId = 5,
                    Name = "Mazda",
                    Code = "MAZDA"
                },
            };
        }
        public static ICollection<Model> GetModels()
        {
            return new List<Model>
            {
                new Model
                {
                    ModelId = 1,
                    ManufacturerId = 1,
                    Name = "A6",
                    Code = "A6"
                },
                new Model
                {
                    ModelId = 2,
                    ManufacturerId = 2,
                    Name = "MPower",
                    Code = "MP"
                },
                new Model
                {
                    ModelId = 3,
                    ManufacturerId = 3,
                    Name = "Gold",
                    Code = "GOLF"
                },
            };
        }
        public static ICollection<User> GetUsers()
        {
            return new List<User>
            {
                new User
                {
                    Id = 1,
                    UserName = "ruslan@smartgarage.com",
                    NormalizedUserName = "RUSLAN@SMARTGARAGE.COM",
                    Email = "ruslan@smartgarage.com",
                    NormalizedEmail = "RUSLAN@SMARTGARAGE.COM",
                    PasswordHash = "fargawrgwr",
                    PhoneNumber = "0887123213",
                    FirstName = "Ruslan",
                    LastName = "Manikatov",
                    SecurityStamp = "justSecurityStamp",
                },

                new User
                {
                    Id = 2,
                    UserName = "irinan@smartgarage.com",
                    NormalizedUserName = "IRINA@SMARTGARAGE.COM",
                    Email = "irina@smartgarage.com",
                    NormalizedEmail = "IRINA@SMARTGARAGE.COM",
                    PasswordHash = "NLuiohBLuh7LHFYI",
                    PhoneNumber = "0887123993",
                    FirstName = "Irina",
                    LastName = "Stancheva",
                    SecurityStamp = "anothersecuritystamp",
                },
                new User
                {
                    Id = 3,
                    UserName = "patrik@smartgarage.com",
                    NormalizedUserName = "PATRIK@SMARTGARAGE.COM",
                    Email = "patrik@smartgarage.com",
                    NormalizedEmail = "PATRIK@SMARTGARAGE.COM",
                    PasswordHash = "NLlisnefohBLuh7LHFYI",
                    PhoneNumber = "0187903993",
                    FirstName = "Patrik",
                    LastName = "Pentev",
                    SecurityStamp = "thirdsecuRityStamp",
                },
            };
        }
        public static ICollection<Vehicle> GetVehicles()
        {
            return new List<Vehicle>
            {
                new Vehicle
                {
                    VehicleId = 1,
                    Vin = "fsldfjAcf45fdsfdf",
                    RegistrationPlate = "CB1254XB",
                    OwnerId = 1,
                    ModelId = 1,
                    Year = DateTime.Now,
                    CreatedOn = DateTime.Now,
                    VehicleTypeId = 1,

                },
                new Vehicle
                {
                    VehicleId = 2,
                    Vin = "fsPkXjAcf45fdsfdf",
                    RegistrationPlate = "H1299XB",
                    OwnerId = 1,
                    ModelId = 2,
                    Year = DateTime.Now,
                    CreatedOn = DateTime.Now,
                    VehicleTypeId = 1,
                },
                new Vehicle
                {
                    VehicleId = 3,
                    Vin = "fsldfjAcf45fOlSdf",
                    RegistrationPlate = "CB0001BB",
                    OwnerId = 2,
                    ModelId = 3,
                    Year = DateTime.Now,
                    CreatedOn = DateTime.Now,
                    VehicleTypeId = 1,
                },
                new Vehicle
                {
                    VehicleId = 4,
                    Vin = "fslLpNAcf45fOlSdf",
                    RegistrationPlate = "CB0971BB",
                    OwnerId = 2,
                    ModelId = 3,
                    Year = DateTime.Now,
                    CreatedOn = DateTime.Now,
                    VehicleTypeId = 1,
                }
            };
        }
        public static ICollection<Visit> GetVisits()
        {
            return new List<Visit>
            {
                new Visit
                {
                    VisitId = 1,
                    VehicleId = 1,
                    VisitStatusId = 1,
                    ArriveDate = DateTime.Now,
                    DepartureDate = DateTime.Now,
                },
                new Visit
                {
                    VisitId = 2,
                    VehicleId = 2,
                    VisitStatusId = 1,
                    ArriveDate = DateTime.Now,
                    DepartureDate = DateTime.Now,
                },
                new Visit
                {
                    VisitId = 3,
                    VehicleId = 3,
                    VisitStatusId = 2,
                    ArriveDate = DateTime.Now,
                    DepartureDate = DateTime.Now,
                },
            };
        }
        public static ICollection<Role> GetRoles()
        {
            return new List<Role>
            {
                new Role
                {
                    Id =1,
                    Name = "customer",
                    NormalizedName = "CUSTOMER",
                },
                new Role
                {
                    Id =2,
                    Name = "employee",
                    NormalizedName = "EMPLOYEE",
                },
                new Role
                {
                    Id =3,
                    Name = "golden customer",
                    NormalizedName = "GOLDEN CUSTOMER",
                },
            };
        }
        public static ICollection<Service> GetServices()
        {
            return new List<Service>
            {
                new Service
                {
                    ServiceId =1,
                    Name = "Chnage Filters",
                    Price = 30,
                },
                new Service
                {
                    ServiceId =2,
                    Name = "Chnage Oil",
                    Price = 40,
                },
                new Service
                {
                    ServiceId =3,
                    Name = "Battery replacement",
                    Price = 77,
                },
                new Service
                {
                    ServiceId =4,
                    Name = "Antifreeze added",
                    Price = 33,
                },
                new Service
                {
                    ServiceId =5,
                    Name = "Wheels aligned/balanced",
                    Price = 22,
                },
            };
        }
        public static ICollection<VisitServices> GetVisitServices()
        {
            return new List<VisitServices>
            {
                new VisitServices
                {
                    VisitId = 1,
                    ServiceId = 1,
                },
                new VisitServices
                {
                    VisitId = 1,
                    ServiceId = 2,
                },
                new VisitServices
                {
                    VisitId = 2,
                    ServiceId = 3,
                },
            };
        }
        public static ICollection<VisitStatus> GetVisitStatuses()
        {
            return new List<VisitStatus>
            {
                new VisitStatus
                {
                   VisitStatusId =1,
                   Name = "Resived",
                },
                new VisitStatus
                {
                   VisitStatusId =2,
                   Name = "Preparing",
                },
                new VisitStatus
                {
                   VisitStatusId =3,
                   Name = "Ready For Pick Up",
                },
                new VisitStatus
                {
                   VisitStatusId =4,
                   Name = "Test VisitStatus",
                },
            };
        }
        public static ICollection<VehicleType> GetVehicleTypes()
        {
            return new List<VehicleType>
            {
                new VehicleType
                {
                   VehicleTypeId =1,
                   Name = "Sedan",
                   Coefficient = 1,
                },
                new VehicleType
                {
                   VehicleTypeId =2,
                   Name = "Geep",
                   Coefficient = 2,
                },
                new VehicleType
                {
                   VehicleTypeId =3,
                   Name = "Truck",
                   Coefficient = 3,
                },
                new VehicleType
                {
                   VehicleTypeId =4,
                   Name = "Test For Delete",
                   Coefficient = 4,
                },
            };
        }
        public static ICollection<ServiceVehicleTypes> GetServiceVehicleType()
        {
            return new List<ServiceVehicleTypes>
            {
                new ServiceVehicleTypes
                {
                    ServiceId = 1,
                    VehicleTypeId =1,
                },
                new ServiceVehicleTypes
                {
                    ServiceId = 1,
                    VehicleTypeId =2,
                },
                new ServiceVehicleTypes
                {
                    ServiceId = 1,
                    VehicleTypeId =3,
                },
                new ServiceVehicleTypes
                {
                    ServiceId = 2,
                    VehicleTypeId =1,
                },
            };
        }



        public static ICollection<ManufacturerCreateDTO> GetManufacturerCreateDTO()
            {
                return new List<ManufacturerCreateDTO>
            {
                new ManufacturerCreateDTO
                {
                   Name = "Test",
                   Code = "TEST"
                },

                new ManufacturerCreateDTO
                {
                    Name = "BMW",
                    Code = "BMW"
                },
                new ManufacturerCreateDTO
                {
                    Name = "Volkswagen",
                    Code = "VW"
                },

            };

            }
        public static CarModelDTO GetCarModelDto()
            {
                return new CarModelDTO
                {
                    ManufacturerId = 1,
                    ManufacturerName = "Audi",
                    ModelId = 1,
                    Name = "A6",
                    Code = "A6"
                };
            }
        public static CarModelCreateDTO GetCarModelCreateDto()
            {
                return new CarModelCreateDTO
                {
                    ManufacturerId = 1,
                    Name = "AudiA6",
                    Code = "AUDIA6"
                };
            }

        }
    }
