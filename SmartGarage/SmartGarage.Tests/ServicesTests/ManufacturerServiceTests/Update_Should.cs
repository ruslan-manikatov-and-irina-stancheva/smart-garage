﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AutoMapper;
using CustomExceptions.Exceptions;
using SmartGarage.Data;
using SmartGarage.Services.DTO;
using SmartGarage.Services.Services;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServicesTests.ManufacturerServiceTests
{
    [TestClass]
    public class Update_Should
    {//[UnitOfWork_StateUnderTest_ExpectedBehavior] naming convention
        [TestMethod]
        public void ShouldUpdateManufacturerByGivenId_WhenPassedCorrectParameters()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ShouldUpdateManufacturerByGivenId_WhenPassedCorrectParameters));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new ManufacturerService(actContext, mapper);
                //make = manufacturer
                var makeDTO = Utils.GetManufacturerCreateDTO().First();
                int makeId = 1;
                var result = sut.UpdateAsync(makeId, makeDTO);
                var espected = actContext.Manufacturers.First(x => x.ManufacturerId == makeId);

                //Assert
                Assert.AreEqual(espected.Name, makeDTO.Name);
                Assert.AreEqual(typeof(ManufacturerCreateDTO), result.Result.GetType());
            }
        }

        [TestMethod]
        public async Task PassInvalidId_ShouldThrowManufacturerNotFound()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(PassInvalidId_ShouldThrowManufacturerNotFound));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new ManufacturerService(actContext, mapper);
                //make = manufacturer
                var makeDTO = Utils.GetManufacturerCreateDTO().First();
                int makeId = -1;

                //Assert
                await Assert.ThrowsExceptionAsync<ManufacturerNotFoundException>(async ()=> await sut.UpdateAsync(makeId, makeDTO));
            }
        }

        [TestMethod]
        public async Task PassNullParametersForName_ShouldThrowArgumentNull()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(PassNullParametersForName_ShouldThrowArgumentNull));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new ManufacturerService(actContext, mapper);
                //make = manufacturer
                var makeDTO = Utils.GetManufacturerCreateDTO().First();
                makeDTO.Name = null;
                int makeId = 1;

                //Assert
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(async() => await sut.UpdateAsync(makeId, makeDTO));
            }
        }

        [TestMethod]
        public async Task PassNullParametersForCode_ShouldThrowArgumentNull()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(PassNullParametersForCode_ShouldThrowArgumentNull));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new ManufacturerService(actContext, mapper);
                //make = manufacturer
                var makeDTO = Utils.GetManufacturerCreateDTO().First();
                makeDTO.Code = null;
                int makeId = 1;

                //Assert
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(async() => await sut.UpdateAsync(makeId, makeDTO));
            }
        }
    }
}
