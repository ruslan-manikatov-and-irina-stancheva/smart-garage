﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SmartGarage.Data;
using SmartGarage.Services.Services;
using System.Linq;

namespace SmartGarage.Tests.ServicesTests.ManufacturerServiceTests
{
    [TestClass]
    public class Get_Should
    {
        [TestMethod]
        public void Manufacturer_GetAllData_ShouldReturnCorrectCountOfManufactures()
        {
            var options = Utils.GetOptions(nameof(Manufacturer_GetAllData_ShouldReturnCorrectCountOfManufactures));
            var mapper = new Mock<IMapper>();

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.SaveChanges();
            }

            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new ManufacturerService(actContext, mapper.Object);
                var result = sut.GetAllAsync();
                var expected = actContext.Manufacturers
                    .ToList();

                //Assert
                Assert.AreEqual(expected.Count(), result.Result.Count());
            }

        }
        [TestMethod]
        public void Manufacturer_GetById_ShouldReturnCorrectData()
        {
            var options = Utils.GetOptions(nameof(Manufacturer_GetById_ShouldReturnCorrectData));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
 
            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {

                var sut = new ManufacturerService(actContext, mapper);
                var result = sut.GetAsync(1);

                var expected = actContext.Manufacturers
                    .ToList().First();

                //Assert
                Assert.AreEqual(expected.ManufacturerId, result.Result.ManufacturerId);
                Assert.AreEqual(expected.Name, result.Result.Name);
            }
        }

    }
}
