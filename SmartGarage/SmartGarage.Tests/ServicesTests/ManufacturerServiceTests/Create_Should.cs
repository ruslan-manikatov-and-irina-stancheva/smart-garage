﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;
using SmartGarage.Data;
using SmartGarage.Services.DTO;
using SmartGarage.Services.Services;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServicesTests.ManufacturerServiceTests
{
    //[UnitOfWork_StateUnderTest_ExpectedBehavior] naming convention

    [TestClass]
    public class Create_Should
    {
        [TestMethod]
        public void Manufacturer_CreateNew_ShouldAddOneInDatabase()
        {
            var options = Utils.GetOptions(nameof(Manufacturer_CreateNew_ShouldAddOneInDatabase));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            using (var arrangeContext = new SmartGarageDbContext(options))
            {

                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.SaveChanges();

            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new ManufacturerService(actContext, mapper);

                var expected = actContext.Manufacturers.Count() + 1;

                var manufacturerDTO = Utils.GetManufacturerCreateDTO();
                var result = sut.CreateAsync(manufacturerDTO.First());

                //Assert
                Assert.AreEqual(expected, actContext.Manufacturers.Count());
            }

        }

        [TestMethod]
        public async Task Manufacturer_PassNullParametersToName_ShouldThrowNullException()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Manufacturer_PassNullParametersToName_ShouldThrowNullException));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());

            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new ManufacturerService(actContext, mapper);

                var manufacturerDTO = new ManufacturerCreateDTO();
                manufacturerDTO.Name = null;
                manufacturerDTO.Code = "Test";
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(async() => await sut.CreateAsync(manufacturerDTO));
            }

        }

        [TestMethod]
        public async Task Manufacturer_PassNullParametersToCode_ShouldThrowNullException()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Manufacturer_PassNullParametersToCode_ShouldThrowNullException));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());

            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new ManufacturerService(actContext, mapper);

                var manufacturerDTO = new ManufacturerCreateDTO();
                manufacturerDTO.Name = "Test";
                manufacturerDTO.Code = null;
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(async() => await sut.CreateAsync(manufacturerDTO));
            }
        }
    }
}
