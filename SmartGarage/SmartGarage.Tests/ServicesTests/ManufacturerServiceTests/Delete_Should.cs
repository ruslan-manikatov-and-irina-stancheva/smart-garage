﻿using AutoMapper;
using CustomExceptions.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SmartGarage.Data;
using SmartGarage.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServicesTests.ManufacturerServiceTests
{
    [TestClass]
    public class Delete_Should
    {
        [TestMethod]
        public void ShouldDeleteManufacturer_WhenPassedCorrectManufacturerIdWithNoModelReference()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ShouldDeleteManufacturer_WhenPassedCorrectManufacturerIdWithNoModelReference));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            using (var arrangeContext = new SmartGarageDbContext(options))
            {

                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new ManufacturerService(actContext, mapper);
                var expected = actContext.Manufacturers.Count() - 1;

                int makeId = 4;
                var result = sut.DeleteAsync(makeId);
                actContext.SaveChanges();

                var given = actContext.Manufacturers.Count();

                //Assert
                Assert.AreEqual(expected, given);
                Assert.AreEqual(true, result.Result);
            }
        }

        [TestMethod]
        public async Task ShouldThrowManufacturerNotFound_IfPassedIncorectId()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ShouldThrowManufacturerNotFound_IfPassedIncorectId));
            //IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var mapper = new Mock<IMapper>();

            using (var arrangeContext = new SmartGarageDbContext(options))
            {

                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new ManufacturerService(actContext, mapper.Object);

                int makeId = -1;
                //Assert
               await Assert.ThrowsExceptionAsync<ManufacturerNotFoundException>(async() => await sut.DeleteAsync(makeId));
            }
        }

        [TestMethod]
        public async Task ShouldArgumentOutOfRangeException_IfPassedManufacturerWithModels()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ShouldArgumentOutOfRangeException_IfPassedManufacturerWithModels));
            //IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var mapper = new Mock<IMapper>();

            using (var arrangeContext = new SmartGarageDbContext(options))
            {

                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new ManufacturerService(actContext, mapper.Object);

                int makeId = 1;
                //Assert
                await Assert.ThrowsExceptionAsync<ArgumentOutOfRangeException>(async () => await sut.DeleteAsync(makeId));
            }

        }
    }
}
