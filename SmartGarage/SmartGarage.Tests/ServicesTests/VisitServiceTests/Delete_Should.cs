﻿using AutoMapper;
using CustomExceptions.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SmartGarage.Data;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServicesTests.VisitServiceTests
{
    [TestClass]
    public class Delete_Should
    {
        [TestMethod]
        public void ShouldDeleteVisit_WhenPassedCorrectVisitId()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ShouldDeleteVisit_WhenPassedCorrectVisitId));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var visitService = new Mock<IVisitServicesService>();

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.Services.AddRange(Utils.GetServices());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.VisitStatuses.AddRange(Utils.GetVisitStatuses());
                arrangeContext.Vehicles.AddRange(Utils.GetVehicles());
                arrangeContext.Visits.AddRange(Utils.GetVisits());
                arrangeContext.VisitServices.AddRange(Utils.GetVisitServices());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new VisitService(actContext, mapper, visitService.Object);
                var expected = actContext.Visits.Count() - 1;

                int serviceId = 3;
                var result = sut.DeleteAsync(serviceId);
                actContext.SaveChanges();

                var given = actContext.Visits.Count();

                //Assert
                Assert.AreEqual(expected, given);
            }
        }

        [TestMethod]
        public async Task ShouldThrowArgumentNull_IfPassedIncorectVisitId()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ShouldThrowArgumentNull_IfPassedIncorectVisitId));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var visitService = new Mock<IVisitServicesService>();

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Services.AddRange(Utils.GetServices());
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.ServiceVehicleTypes.AddRange(Utils.GetServiceVehicleType());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new VisitService(actContext, mapper, visitService.Object);

                int visitId = -1;
                //Assert
                await Assert.ThrowsExceptionAsync<NotFoundException>(async () => await sut.DeleteAsync(visitId));
            }
        }
    }
}
