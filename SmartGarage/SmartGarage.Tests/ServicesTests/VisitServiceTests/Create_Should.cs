﻿using AutoMapper;
using CustomExceptions.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SmartGarage.Data;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTO;
using SmartGarage.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServicesTests.VisitServiceTests
{
    [TestClass]
    public class Create_Should
    {
        [TestMethod]
        public void Visits_CreateNew_ShouldAddOneInDatabase()
        {
            var options = Utils.GetOptions(nameof(Visits_CreateNew_ShouldAddOneInDatabase));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var visitService = new Mock<IVisitServicesService>();

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.Services.AddRange(Utils.GetServices());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.VisitStatuses.AddRange(Utils.GetVisitStatuses());
                arrangeContext.Vehicles.AddRange(Utils.GetVehicles());
                arrangeContext.Visits.AddRange(Utils.GetVisits());
                arrangeContext.VisitServices.AddRange(Utils.GetVisitServices());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new VisitService(actContext, mapper, visitService.Object);
                var visitDto = new VisitCreateDTO();
                visitDto.VehicleId = 1;
                visitDto.DepartureDate = DateTime.MaxValue;


                var expected = actContext.Visits.Count() + 1;
                var result = sut.CreateAsync(visitDto);

                //Assert
                Assert.AreEqual(expected, actContext.Visits.Count());
            }
        }
        [TestMethod]
        public async Task Visit_PassIncorectVehicleId_ShouldThrowArgumentNull()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Visit_PassIncorectVehicleId_ShouldThrowArgumentNull));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var visitService = new Mock<IVisitServicesService>();
            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.Services.AddRange(Utils.GetServices());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.VisitStatuses.AddRange(Utils.GetVisitStatuses());
                arrangeContext.Vehicles.AddRange(Utils.GetVehicles());
                arrangeContext.Visits.AddRange(Utils.GetVisits());
                arrangeContext.VisitServices.AddRange(Utils.GetVisitServices());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new VisitService(actContext, mapper, visitService.Object);
                var visitDto = new VisitCreateDTO();
                visitDto.VehicleId = -1;
                visitDto.DepartureDate = DateTime.MaxValue;


                await Assert.ThrowsExceptionAsync<NotFoundException>(async () => await sut.CreateAsync(visitDto));
            }
        }

        [TestMethod]
        public async Task Visit_PassIDateLessThanDateTimeNow_ShouldThrowWrongDateTime()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Visit_PassIDateLessThanDateTimeNow_ShouldThrowWrongDateTime));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var visitService = new Mock<IVisitServicesService>();
            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.Services.AddRange(Utils.GetServices());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.VisitStatuses.AddRange(Utils.GetVisitStatuses());
                arrangeContext.Vehicles.AddRange(Utils.GetVehicles());
                arrangeContext.Visits.AddRange(Utils.GetVisits());
                arrangeContext.VisitServices.AddRange(Utils.GetVisitServices());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new VisitService(actContext, mapper, visitService.Object);
                var visitDto = new VisitCreateDTO();
                visitDto.VehicleId = 1;
                visitDto.DepartureDate = DateTime.MinValue;


                await Assert.ThrowsExceptionAsync<WrongDateTimeException>(async () => await sut.CreateAsync(visitDto));
            }
        }
    }
}
