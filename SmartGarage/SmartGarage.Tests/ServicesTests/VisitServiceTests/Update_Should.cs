﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SmartGarage.Data;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTO;
using SmartGarage.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartGarage.Tests.ServicesTests.VisitServiceTests
{
    [TestClass]
    public class Update_Should
    {
        [TestMethod]
        public void ShouldUpdateVisitByGivenId_WhenPassedCorrectParameters()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ShouldUpdateVisitByGivenId_WhenPassedCorrectParameters));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var visitService = new Mock<IVisitServicesService>();

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.Services.AddRange(Utils.GetServices());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.VisitStatuses.AddRange(Utils.GetVisitStatuses());
                arrangeContext.Vehicles.AddRange(Utils.GetVehicles());
                arrangeContext.Visits.AddRange(Utils.GetVisits());
                arrangeContext.VisitServices.AddRange(Utils.GetVisitServices());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new VisitService(actContext, mapper, visitService.Object);
                var visitDto = new VisitUpdateDTO();
                visitDto.VehicleId = 1;
                visitDto.DepartureDate = DateTime.MaxValue;

                var visitId = 1;
                var result = sut.UpdateAsync(visitId, visitDto);
                var expected = actContext.Visits
                    .First(v => v.VisitId == visitId);

                //Assert
                Assert.AreEqual(expected.VehicleId, result.Result.VehicleId);
            }
        }
    }
}
