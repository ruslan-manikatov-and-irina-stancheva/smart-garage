﻿using AutoMapper;
using CustomExceptions.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SmartGarage.Data;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServicesTests.VisitServiceTests
{
    [TestClass]
    public class Get_Should
    {
        [TestMethod]
        public void Visit_GetAllData_ShouldReturnCorrectCount()
        {
            var options = Utils.GetOptions(nameof(Visit_GetAllData_ShouldReturnCorrectCount));
            var mapper = new Mock<IMapper>();
            var visitService = new Mock<IVisitServicesService>();

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.VisitServices.AddRange(Utils.GetVisitServices());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new VisitService(actContext, mapper.Object, visitService.Object);
                var result = sut.GetAllAsync();

                var expected = actContext.Visits
                    .ToList();

                //Assert
                Assert.AreEqual(expected.Count(), result.Result.Count());
            }
        }

        [TestMethod]
        public void GetByVisitId_ShouldReturnCorrectData()
        {
            var options = Utils.GetOptions(nameof(GetByVisitId_ShouldReturnCorrectData));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var visitService = new Mock<IVisitServicesService>();
            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.Services.AddRange(Utils.GetServices());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.VisitStatuses.AddRange(Utils.GetVisitStatuses());
                arrangeContext.Vehicles.AddRange(Utils.GetVehicles());
                arrangeContext.Visits.AddRange(Utils.GetVisits());
                arrangeContext.VisitServices.AddRange(Utils.GetVisitServices());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new VisitService(actContext, mapper, visitService.Object);
                var VisitId = 1;
                var result = sut.GetAsync(VisitId);

                var expected = actContext.Visits
                    .First(v => v.VisitId == VisitId);

                //Assert
                Assert.AreEqual(expected.VisitId, result.Result.VisitId);
            }
        }

        [TestMethod]
        public async Task PassInvalidVisitId_ShouldThrowArgumentNull()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(PassInvalidVisitId_ShouldThrowArgumentNull));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var visitService = new Mock<IVisitServicesService>();

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.Services.AddRange(Utils.GetServices());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.VisitStatuses.AddRange(Utils.GetVisitStatuses());
                arrangeContext.Vehicles.AddRange(Utils.GetVehicles());
                arrangeContext.Visits.AddRange(Utils.GetVisits());
                arrangeContext.VisitServices.AddRange(Utils.GetVisitServices());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new VisitService(actContext, mapper, visitService.Object);
                var VisitId = -1;
         
                //Assert
                await Assert.ThrowsExceptionAsync<NotFoundException>(async () => await sut.GetAsync(VisitId));
            }
        }
    }
}
