﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data;
using SmartGarage.Services.Services;
using System.Linq;

namespace SmartGarage.Tests.ServicesTests.ServiceVehicleTypesServiceTests
{
    [TestClass]
    public class Get_Should
    {
        [TestMethod]
        public void ServiceVehicleType_GetAllData_ShouldReturnCorrectCount()
        {
            var options = Utils.GetOptions(nameof(ServiceVehicleType_GetAllData_ShouldReturnCorrectCount));

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.ServiceVehicleTypes.AddRange(Utils.GetServiceVehicleType());
                arrangeContext.SaveChanges();
            }

            //Act      
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new ServiceVehicleTypesService(actContext);
                var result = sut.GetAll();
                var expected = actContext.ServiceVehicleTypes
                    .ToList();

                //Assert
                Assert.AreEqual(expected.Count(), result.Count());
            }
        }

        [TestMethod]
        public void ServiceVehicleType_GetById_ShouldReturnCorrectData()
        {
            var options = Utils.GetOptions(nameof(ServiceVehicleType_GetById_ShouldReturnCorrectData));

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.ServiceVehicleTypes.AddRange(Utils.GetServiceVehicleType());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new ServiceVehicleTypesService(actContext);
                int ServicevehicleTypeId = 1;

                var result = sut.Get(ServicevehicleTypeId);

                var expected = actContext.ServiceVehicleTypes
                    .First(v => v.VehicleTypeId == ServicevehicleTypeId);

                //Assert
                Assert.AreEqual(expected.ServiceId, result.First().ServiceId);
            }
        }
    }
}
