﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data;
using SmartGarage.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartGarage.Tests.ServicesTests.ServiceVehicleTypesServiceTests
{
    [TestClass]
    public class Delete_Should
    {
        [TestMethod]
        public void ShouldDeleteServiceVehicleTypes_WhenPassedCorrectServiceId()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ShouldDeleteServiceVehicleTypes_WhenPassedCorrectServiceId));
            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.ServiceVehicleTypes.AddRange(Utils.GetServiceVehicleType());
                arrangeContext.Services.AddRange(Utils.GetServices());
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new ServiceVehicleTypesService(actContext);
                var expected = actContext.ServiceVehicleTypes.Count() - 1;
                int serviceId = 2; //no references to it
                var result = sut.DeleteByServiceIdAsync(serviceId);
                actContext.SaveChanges();

                var given = actContext.ServiceVehicleTypes.Count();
                //Assert
                Assert.AreEqual(expected, given);
            }
        }

        [TestMethod]
        public void ShouldDeleteServiceVehicleTypes_WhenPassedCorrectVehicleTypeId()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ShouldDeleteServiceVehicleTypes_WhenPassedCorrectVehicleTypeId));
            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Services.AddRange(Utils.GetServices());
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.ServiceVehicleTypes.AddRange(Utils.GetServiceVehicleType());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new ServiceVehicleTypesService(actContext);
                var expected = actContext.ServiceVehicleTypes.Count()-1;
                int vehicleTypeId = 2; //no references to it
                var result = sut.DeleteByVehicleTypeIdAsync(vehicleTypeId);
                actContext.SaveChanges();

                var given = actContext.ServiceVehicleTypes.Count();
                //Assert
                Assert.AreEqual(expected, given);
            }
        }
    }
}
