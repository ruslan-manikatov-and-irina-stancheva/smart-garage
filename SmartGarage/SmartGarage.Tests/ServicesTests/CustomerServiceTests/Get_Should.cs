﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SmartGarage.Data;
using SmartGarage.Data.Models;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTO;
using SmartGarage.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServicesTests.CustomerServiceTests
{
    [TestClass]
    public class Get_Should
    {
        [TestMethod]
        public void Customer_GetAllDVehicles_ShouldReturnCorrectCount()
        {
            var options = Utils.GetOptions(nameof(Customer_GetAllDVehicles_ShouldReturnCorrectCount));
            var currencyService = new Mock<ICurrencyService>();
            
            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.Vehicles.AddRange(Utils.GetVehicles());
                arrangeContext.Visits.AddRange(Utils.GetVisits());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new CustomerServices(actContext,currencyService.Object);
                var user = actContext.Users.First();

                var result = sut.GetAllVehicles(user.UserName);

                var expected = actContext.Vehicles
                    .Where(v => v.Owner.UserName == user.UserName)
                    .ToList();

                //Assert
                Assert.AreEqual(expected.Count(), result.Result.Count());
                Assert.AreEqual(expected.First().RegistrationPlate, result.Result.First().RegistrationPlate);
            }
        }

        [TestMethod]
        public async Task Customer_GetAllServices_ShouldReturnCorrectCount()//add services
        {
            var options = Utils.GetOptions(nameof(Customer_GetAllServices_ShouldReturnCorrectCount));
            var currencyService = new Mock<ICurrencyService>();

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.Services.AddRange(Utils.GetServices());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.VisitStatuses.AddRange(Utils.GetVisitStatuses());
                arrangeContext.Vehicles.AddRange(Utils.GetVehicles());
                arrangeContext.Visits.AddRange(Utils.GetVisits());
                arrangeContext.VisitServices.AddRange(Utils.GetVisitServices());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new CustomerServices(actContext, currencyService.Object);
                var user = actContext.Users.First();

                var result = sut.GetAllServices(user.UserName);

                var expected = await actContext.Visits
                .Where(v => v.Vehicle.Owner.UserName == user.UserName)
                .ToListAsync();

                //Assert
                Assert.AreEqual(expected.Count(), result.Result.Count());
            }
        }

        [TestMethod]
        public async Task Customer_FilterServicesByRegPlate_ShouldReturnCorrectCount()//add services
        {
            var options = Utils.GetOptions(nameof(Customer_FilterServicesByRegPlate_ShouldReturnCorrectCount));
            var currencyService = new Mock<ICurrencyService>();

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.Services.AddRange(Utils.GetServices());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.VisitStatuses.AddRange(Utils.GetVisitStatuses());
                arrangeContext.Vehicles.AddRange(Utils.GetVehicles());
                arrangeContext.Visits.AddRange(Utils.GetVisits());
                arrangeContext.VisitServices.AddRange(Utils.GetVisitServices());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new CustomerServices(actContext, currencyService.Object);
                var user = actContext.Users.First();
                var regPlate = actContext.Vehicles.First().RegistrationPlate;

                var result = sut.FilterServicesByRegPlate(user.UserName, regPlate);

                var expected = await actContext.Visits
                .Where(v => v.Vehicle.Owner.UserName == user.UserName && (v.Vehicle.RegistrationPlate == regPlate))
                .ToListAsync();

                //Assert
                Assert.AreEqual(expected.Count(), result.Result.Count());
            }
        }

        [TestMethod]
        public async Task Customer_FilterServicesByDate_ShouldReturnCorrectCount()//add services
        {
            var options = Utils.GetOptions(nameof(Customer_FilterServicesByDate_ShouldReturnCorrectCount));
            var currencyService = new Mock<ICurrencyService>();

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.Services.AddRange(Utils.GetServices());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.VisitStatuses.AddRange(Utils.GetVisitStatuses());
                arrangeContext.Vehicles.AddRange(Utils.GetVehicles());
                arrangeContext.Visits.AddRange(Utils.GetVisits());
                arrangeContext.VisitServices.AddRange(Utils.GetVisitServices());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new CustomerServices(actContext, currencyService.Object);
                var user = actContext.Users.First();
                var visitdate = actContext.Visits.First().ArriveDate;

                var result = sut.FilterServicesByDate(user.UserName, visitdate);

                var expected = await actContext.Visits
                .Where(v => v.Vehicle.Owner.UserName == user.UserName && (v.ArriveDate == visitdate))
                .ToListAsync();

                //Assert
                Assert.AreEqual(expected.Count(), result.Result.Count());
            }
        }

        [TestMethod]
        public async Task Customer_FilterServicesByRegPlateAndDate_ShouldReturnCorrectCount()//add services
        {
            var options = Utils.GetOptions(nameof(Customer_FilterServicesByRegPlateAndDate_ShouldReturnCorrectCount));
            var currencyService = new Mock<ICurrencyService>();

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.Services.AddRange(Utils.GetServices());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.VisitStatuses.AddRange(Utils.GetVisitStatuses());
                arrangeContext.Vehicles.AddRange(Utils.GetVehicles());
                arrangeContext.Visits.AddRange(Utils.GetVisits());
                arrangeContext.VisitServices.AddRange(Utils.GetVisitServices());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new CustomerServices(actContext, currencyService.Object);
                var user = actContext.Users.First();
                var visit = actContext.Visits.First();
                var vehicle = actContext.Vehicles.First();

                var result = sut.FilterServicesByRegPlateAndDate(user.UserName, vehicle.RegistrationPlate, visit.ArriveDate);

                var expected = await actContext.Visits
                .Where(v => v.Vehicle.Owner.UserName == user.UserName
                && (v.ArriveDate == visit.ArriveDate)
                && (v.Vehicle.RegistrationPlate == vehicle.RegistrationPlate))
                .ToListAsync();

                //Assert
                Assert.AreEqual(expected.Count(), result.Result.Count());
            }
        }

        [TestMethod]
        public async Task Customer_GetVisitReportById_ShouldReturnCorrectData()//add services
        {
            var options = Utils.GetOptions(nameof(Customer_GetVisitReportById_ShouldReturnCorrectData));
            var currencyService = new Mock<ICurrencyService>();

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.Services.AddRange(Utils.GetServices());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.VisitStatuses.AddRange(Utils.GetVisitStatuses());
                arrangeContext.Vehicles.AddRange(Utils.GetVehicles());
                arrangeContext.Visits.AddRange(Utils.GetVisits());
                arrangeContext.VisitServices.AddRange(Utils.GetVisitServices());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new CustomerServices(actContext, currencyService.Object);
                var user = actContext.Users.First();
                //var visit = actContext.Visits.First();
                int visitId = 1;
                var result = sut.GetVisitReportById(user.UserName, visitId);

                var expected = await actContext.Visits
                .Where(v => v.VisitId == visitId)
                .ToListAsync();

                //Assert
                Assert.AreEqual(expected.First().VisitId, result.Result.VisitId);
            }
        }
        [TestMethod]
        public async Task PassInvalidCustomerId_ShouldThrowUserArgumentNull()//add services
        {
            var options = Utils.GetOptions(nameof(PassInvalidCustomerId_ShouldThrowUserArgumentNull));
            var currencyService = new Mock<ICurrencyService>();

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.Services.AddRange(Utils.GetServices());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.VisitStatuses.AddRange(Utils.GetVisitStatuses());
                arrangeContext.Vehicles.AddRange(Utils.GetVehicles());
                arrangeContext.Visits.AddRange(Utils.GetVisits());
                arrangeContext.VisitServices.AddRange(Utils.GetVisitServices());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new CustomerServices(actContext, currencyService.Object);
                var user = actContext.Users.First();
                //var visit = actContext.Visits.First();
                int visitId = -1;

                var result = sut.GetVisitReportById(user.UserName, visitId);

                //Assert
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(async () => await sut.GetVisitReportById(user.UserName, visitId));
            }
        }

        [TestMethod]
        public async Task Customer_GetVisitReportByIaAndCurrency_ShouldReturnCorrectData()//add services
        {
            var options = Utils.GetOptions(nameof(Customer_GetVisitReportByIaAndCurrency_ShouldReturnCorrectData));
            var currencyService = new Mock<ICurrencyService>();

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.Services.AddRange(Utils.GetServices());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.VisitStatuses.AddRange(Utils.GetVisitStatuses());
                arrangeContext.Vehicles.AddRange(Utils.GetVehicles());
                arrangeContext.Visits.AddRange(Utils.GetVisits());
                arrangeContext.VisitServices.AddRange(Utils.GetVisitServices());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new CustomerServices(actContext, currencyService.Object);
                var user = actContext.Users.First();
                //var visit = actContext.Visits.First();
                int visitId = 1;
                var result = sut.GetVisitReportByIdAndCurrency(user.UserName, visitId, "bgn");

                var expected = await actContext.Visits
                .Where(v => v.VisitId == visitId)
                .ToListAsync();

                //Assert
                Assert.AreEqual(expected.First().VisitId, result.Result.VisitId);
            }
        }

        [TestMethod]
        public async Task Customer_GetAllVisitsReport_ShouldReturnCorrectData()//add services
        {
            var options = Utils.GetOptions(nameof(Customer_GetAllVisitsReport_ShouldReturnCorrectData));
            var currencyService = new Mock<ICurrencyService>();

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.Services.AddRange(Utils.GetServices());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.VisitStatuses.AddRange(Utils.GetVisitStatuses());
                arrangeContext.Vehicles.AddRange(Utils.GetVehicles());
                arrangeContext.Visits.AddRange(Utils.GetVisits());
                arrangeContext.VisitServices.AddRange(Utils.GetVisitServices());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new CustomerServices(actContext, currencyService.Object);
                var user = actContext.Users.First();
                //var visit = actContext.Visits.First();
                int visitId = 1;
                var result = sut.GetAllVisitsReport(user.UserName, "EUR");

                var expected = await actContext.Visits
                .Where(v => v.VisitId == visitId)
                .ToListAsync();

                //Assert
                Assert.AreEqual(expected.First().VisitId, result.Result.CustomerGetVisitDTOs.First().VisitId);
            }
        }

        [TestMethod]
        public async Task GetAllVisitsReport_ShouldReturnEuroCurrencyIfPassNullForCurrency()//add services
        {
            var options = Utils.GetOptions(nameof(GetAllVisitsReport_ShouldReturnEuroCurrencyIfPassNullForCurrency));
            var currencyService = new Mock<ICurrencyService>();

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.Services.AddRange(Utils.GetServices());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.VisitStatuses.AddRange(Utils.GetVisitStatuses());
                arrangeContext.Vehicles.AddRange(Utils.GetVehicles());
                arrangeContext.Visits.AddRange(Utils.GetVisits());
                arrangeContext.VisitServices.AddRange(Utils.GetVisitServices());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new CustomerServices(actContext, currencyService.Object);
                var user = actContext.Users.First();

                int visitId = 1;
                var result = sut.GetAllVisitsReport(user.UserName, null);

                var expected = await actContext.Visits
                .Where(v => v.VisitId == visitId)
                .ToListAsync();

                //Assert
                Assert.AreEqual(expected.First().VisitId, result.Result.CustomerGetVisitDTOs.First().VisitId);
                Assert.AreEqual("EUR", result.Result.Currency);
            }
        }
    }
}
