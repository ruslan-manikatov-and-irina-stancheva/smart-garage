﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SmartGarage.Data;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTO;
using SmartGarage.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartGarage.Tests.ServicesTests.VehicleTypeServiceTests
{
    [TestClass]
    public class Create_Should
    {
        [TestMethod]
        public void VehicleType_CreateNew_ShouldAddOneInDatabase()
        {
            var options = Utils.GetOptions(nameof(VehicleType_CreateNew_ShouldAddOneInDatabase));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var serviceVehicleTypesService = new Mock<IServiceVehicleTypesService>();

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new VehicleTypeService(actContext, mapper, serviceVehicleTypesService.Object);
                var expected = actContext.VehicleTypes.Count() + 1;
                
                var vehicleType = new VehicleTypeCreateDTO();
                vehicleType.Name = "New VisitStatus";
                vehicleType.Coefficient = 1;
                var result = sut.CreateAsync(vehicleType);

                //Assert
                Assert.AreEqual(expected, actContext.VehicleTypes.Count());
            }
        }

    }
}
