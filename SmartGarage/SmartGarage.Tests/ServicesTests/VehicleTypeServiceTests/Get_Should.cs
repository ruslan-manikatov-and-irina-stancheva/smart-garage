﻿using AutoMapper;
using CustomExceptions.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SmartGarage.Data;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServicesTests.VehicleTypeServiceTests
{
    [TestClass]
    public class Get_Should
    {

        [TestMethod]
        public void VehicleType_GetAllData_ShouldReturnCorrectCount()
        {
            var options = Utils.GetOptions(nameof(VehicleType_GetAllData_ShouldReturnCorrectCount));
            var mapper = new Mock<IMapper>();
            var serviceVehicleTypesService = new Mock<IServiceVehicleTypesService>();

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.SaveChanges();
            }

            //Act      
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new VehicleTypeService(actContext, mapper.Object, serviceVehicleTypesService.Object);
                var result = sut.GetAllAsync();
                var expected = actContext.VehicleTypes
                    .ToList();

                //Assert
                Assert.AreEqual(expected.Count(), result.Result.Count);
            }
        }

        [TestMethod] 
        public void VehicleType_GetById_ShouldReturnCorrectData()
        {
            var options = Utils.GetOptions(nameof(VehicleType_GetById_ShouldReturnCorrectData));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var serviceVehicleTypesService = new Mock<IServiceVehicleTypesService>();

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new VehicleTypeService(actContext, mapper, serviceVehicleTypesService.Object);
                int vehicleTypeId = 1;

                var result = sut.GetAsync(vehicleTypeId);

                var expected = actContext.VehicleTypes
                    .First(v => v.VehicleTypeId == vehicleTypeId);

                //Assert
                Assert.AreEqual(expected.Name, result.Result.Name);
            }
        }

        [TestMethod]
        public async Task PassNutExistingVehicleTypeId_ShouldThrowArgumentNull()
        {
            var options = Utils.GetOptions(nameof(PassNutExistingVehicleTypeId_ShouldThrowArgumentNull));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var serviceVehicleTypesService = new Mock<IServiceVehicleTypesService>();

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new VehicleTypeService(actContext, mapper, serviceVehicleTypesService.Object);
                int vehicleTypeId = -1;

                var result = sut.GetAsync(vehicleTypeId);

                //Assert
                await Assert.ThrowsExceptionAsync<NotFoundException>(async () => await sut.GetAsync(vehicleTypeId));
            }
        }
    }
}
