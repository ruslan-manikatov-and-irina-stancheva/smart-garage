﻿using AutoMapper;
using CustomExceptions.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SmartGarage.Data;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServicesTests.VehicleTypeServiceTests
{
    [TestClass]
    public class Delete_Should
    {
        [TestMethod]
        public void ShouldDeleteVehicleTypes_WhenPassedCorrectIdWithNoVisitsReferenceReference()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ShouldDeleteVehicleTypes_WhenPassedCorrectIdWithNoVisitsReferenceReference));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var serviceVehicleTypesService = new Mock<IServiceVehicleTypesService>();
            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new VehicleTypeService(actContext, mapper, serviceVehicleTypesService.Object);
                var expected = actContext.VehicleTypes.Count() - 1;
                int vehicleTypeId = 4; //no references to it
                var result = sut.DeleteAsync(vehicleTypeId);
                actContext.SaveChanges();

                var given = actContext.VehicleTypes.Count();
                //Assert
                Assert.AreEqual(expected, given);
            }
        }

        [TestMethod]
        public async Task ShouldThrowArgumentNull_IfPassedIncorectVehicleTypeId()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ShouldThrowArgumentNull_IfPassedIncorectVehicleTypeId));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var serviceVehicleTypesService = new Mock<IServiceVehicleTypesService>();
            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new VehicleTypeService(actContext, mapper, serviceVehicleTypesService.Object);
                int vehicleTypeId = -1; //no references to it
                //Assert
                await Assert.ThrowsExceptionAsync<NotFoundException>(async () => await sut.DeleteAsync(vehicleTypeId));
            }
        }
        [TestMethod]
        public async Task ShouldThrowCanNotBeDelete_IfPassedVehicleTypeIdWithReferingVehicles()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ShouldThrowCanNotBeDelete_IfPassedVehicleTypeIdWithReferingVehicles));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var serviceVehicleTypesService = new Mock<IServiceVehicleTypesService>();
            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.Vehicles.AddRange(Utils.GetVehicles());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new VehicleTypeService(actContext, mapper, serviceVehicleTypesService.Object);
                int vehicleTypeId = 1; //no references to it
                //Assert
                await Assert.ThrowsExceptionAsync<CanNotBeDeleteException>(async () => await sut.DeleteAsync(vehicleTypeId));
            }
        }
    }
}
