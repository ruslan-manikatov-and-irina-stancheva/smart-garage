﻿using AutoMapper;
using CustomExceptions.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SmartGarage.Data;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTO;
using SmartGarage.Services.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServicesTests.VehicleTypeServiceTests
{
    [TestClass]
    public class Update_Should
    {
        [TestMethod]
        public void ShouldUpdateVehicleTypeByGivenId_WhenPassedCorrectParameters()
        {
            var options = Utils.GetOptions(nameof(ShouldUpdateVehicleTypeByGivenId_WhenPassedCorrectParameters));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var serviceVehicleTypesService = new Mock<IServiceVehicleTypesService>();

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new VehicleTypeService(actContext, mapper, serviceVehicleTypesService.Object);
                var vehicleType = new VehicleTypeDTO();
                vehicleType.VehicleTypeId = 1;
                vehicleType.Name = "TestType";
                vehicleType.Coefficient = 1;
                int vehicleUpdateId = 1;
                var result = sut.UpdateAsync(vehicleUpdateId, vehicleType);

                var espected = actContext.VehicleTypes.First(x => x.VehicleTypeId == vehicleUpdateId);
                //Assert
                Assert.AreEqual(espected.Name, result.Result.Name);
            }
        }

        [TestMethod]
        public async Task PassInvalidVehicleTypeId_ShouldThrowArgumentNull()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(PassInvalidVehicleTypeId_ShouldThrowArgumentNull));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var serviceVehicleTypesService = new Mock<IServiceVehicleTypesService>();

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new VehicleTypeService(actContext, mapper, serviceVehicleTypesService.Object);
                var vehicleType = new VehicleTypeDTO();
                vehicleType.VehicleTypeId = 1;
                vehicleType.Name = "TestType";
                vehicleType.Coefficient = 1;
                int vehicleUpdateId = -1;

                //Assert
                await Assert.ThrowsExceptionAsync<NotFoundException>(async () => await sut.UpdateAsync(vehicleUpdateId, vehicleType));
            }
        }
    }
}
