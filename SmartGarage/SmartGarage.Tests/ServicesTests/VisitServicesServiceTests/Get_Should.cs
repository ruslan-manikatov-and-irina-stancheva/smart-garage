﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data;
using SmartGarage.Services.Services;
using System.Linq;


namespace SmartGarage.Tests.ServicesTests.VisitServicesServiceTests
{
    [TestClass]
    public class Get_Should
    {

        [TestMethod]
        public void VisitServices_GetAllData_ShouldReturnCorrectCount()
        {
            var options = Utils.GetOptions(nameof(VisitServices_GetAllData_ShouldReturnCorrectCount));
            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.VisitServices.AddRange(Utils.GetVisitServices());
                arrangeContext.SaveChanges();
            }

            //Act      
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new VisitServicesService(actContext);
                var result = sut.GetAll().ToList();
                var expected = actContext.VisitServices
                    .ToList();

                //Assert
                Assert.AreEqual(expected.Count(), result.Count());
            }
        }

        [TestMethod]
        public void VisitService_GetById_ShouldReturnCorrectData()
        {
            var options = Utils.GetOptions(nameof(VisitService_GetById_ShouldReturnCorrectData));

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.VisitServices.AddRange(Utils.GetVisitServices());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new VisitServicesService(actContext);
                int visitId = 1;

                var result = sut.Get(visitId)
                    .First();

                var expected = actContext.VisitServices
                    .First(v => v.VisitId == visitId);

                //Assert
                Assert.AreEqual(expected.ServiceId, result.ServiceId);
            }
        }
    }
}
