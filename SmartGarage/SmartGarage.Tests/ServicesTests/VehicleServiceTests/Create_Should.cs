﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SmartGarage.Data;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTO;
using SmartGarage.Services.Services;
using System;
using System.Linq;

namespace SmartGarage.Tests.ServicesTests.VehicleServiceTests
{
    [TestClass]
    public class Create_Should
    {
        [TestMethod]
        public void Vehicle_CreateNew_ShouldAddOneInDatabase()
        {
            var options = Utils.GetOptions(nameof(Vehicle_CreateNew_ShouldAddOneInDatabase));

            var userService = new Mock<IUserService>();
            var modelService = new Mock<IModelService>();
            var vehicleTypeService = new Mock<IVehicleTypeService>();

            var vehicleDto = new VehicleCreateDTO();
            vehicleDto.VehicleId = 1;
            vehicleDto.Vin = "niauenaBYpdoaneyc";
            vehicleDto.RegistrationPlate = "H8808TM";
            vehicleDto.OwnerId = 1;
            vehicleDto.ModelId = 1;
            vehicleDto.Year = DateTime.Now;
            vehicleDto.VehicleTypeId = 1;

           

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.Visits.AddRange(Utils.GetVisits());
                arrangeContext.Vehicles.AddRange(Utils.GetVehicles());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new VehicleService(actContext, userService.Object, modelService.Object, vehicleTypeService.Object);

                var expected = actContext.Vehicles.Count();
                var result = sut.CreateAsync(vehicleDto);
                //Assert
                Assert.AreEqual(expected, actContext.Vehicles.Count());
            }
        }

    }
}
