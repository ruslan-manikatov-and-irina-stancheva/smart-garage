﻿using CustomExceptions.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SmartGarage.Data;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTO;
using SmartGarage.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServicesTests.VehicleServiceTests
{
    [TestClass]
    public class Update_Should
    {

        [TestMethod]
        public void ShouldUpdateVehicleByGivenId_WhenPassedCorrectParameters()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ShouldUpdateVehicleByGivenId_WhenPassedCorrectParameters));

            var userService = new Mock<IUserService>();
            var modelService = new Mock<IModelService>();
            var vehicleTypeService = new Mock<IVehicleTypeService>();
            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.Visits.AddRange(Utils.GetVisits());
                arrangeContext.Vehicles.AddRange(Utils.GetVehicles());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new VehicleService(actContext, userService.Object, modelService.Object, vehicleTypeService.Object);

                var vehicleDto = new VehicleCreateDTO();
                vehicleDto.VehicleId = 1;
                vehicleDto.Vin = "niauenaBYpdoaneyc";
                vehicleDto.RegistrationPlate = "H8808TM";
                vehicleDto.OwnerId = 1;
                vehicleDto.ModelId = 1;
                vehicleDto.Year = DateTime.Now;
                vehicleDto.VehicleTypeId = 1;

                var vehicleUpdateId = 1;
                var result = sut.UpdateAsync(vehicleUpdateId, vehicleDto);
                var expected = actContext.Vehicles.First(x => x.VehicleId == vehicleUpdateId);

                //Assert
                Assert.AreEqual(expected.Vin, result.Result.Vin);
                Assert.AreEqual(typeof(VehicleCreateDTO), result.Result.GetType());
            }
        }
        [TestMethod]
        public async Task PassNullIncorectVehicleId_ShouldThrowArgumentNull()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(PassNullIncorectVehicleId_ShouldThrowArgumentNull));
            var userService = new Mock<IUserService>();
            var modelService = new Mock<IModelService>();
            var vehicleTypeService = new Mock<IVehicleTypeService>();

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.Vehicles.AddRange(Utils.GetVehicles());
                arrangeContext.Visits.AddRange(Utils.GetVisits());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new VehicleService(actContext, userService.Object, modelService.Object, vehicleTypeService.Object);

                var vehicleUpdateId = -1;
                //Assert
                await Assert.ThrowsExceptionAsync<NotFoundException>(async () => await sut.UpdateAsync(vehicleUpdateId, new VehicleCreateDTO()));
            }
        }

        [TestMethod]
        public void ShouldUpdateVehicleByVehicleIdAndUserId_WhenPassedCorrectParameters()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ShouldUpdateVehicleByVehicleIdAndUserId_WhenPassedCorrectParameters));

            var userService = new Mock<IUserService>();
            var modelService = new Mock<IModelService>();
            var vehicleTypeService = new Mock<IVehicleTypeService>();
            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.Visits.AddRange(Utils.GetVisits());
                arrangeContext.Vehicles.AddRange(Utils.GetVehicles());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new VehicleService(actContext, userService.Object, modelService.Object, vehicleTypeService.Object);

                var vehicleDto = new VehicleUpdateByCustomerDTO();
                vehicleDto.Vin = "niauenaBYpdoaneyc";
                vehicleDto.RegistrationPlate = "H8808TM";
                vehicleDto.Year = DateTime.Now;
                vehicleDto.VehicleTypeId = 1;
                vehicleDto.ManufacturerModel = new CarModelDTO();

                int customerId = 1;
                int vehicleUpdateId = 1;
                var result = sut.UpdateByCustomerAsync(vehicleUpdateId, customerId, vehicleDto);
                var expected = actContext.Vehicles.First(x => x.VehicleId == vehicleUpdateId);

                //Assert
                Assert.AreEqual(expected.Vin, result.Result.Vin);
                Assert.AreEqual(typeof(VehicleUpdateByCustomerDTO), result.Result.GetType());
            }
        }

        [TestMethod]
        public async Task PassNullIncorectVehicleIdAndCustomerId_ShouldThrowArgumentNull()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(PassNullIncorectVehicleIdAndCustomerId_ShouldThrowArgumentNull));
            var userService = new Mock<IUserService>();
            var modelService = new Mock<IModelService>();
            var vehicleTypeService = new Mock<IVehicleTypeService>();

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.Vehicles.AddRange(Utils.GetVehicles());
                arrangeContext.Visits.AddRange(Utils.GetVisits());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new VehicleService(actContext, userService.Object, modelService.Object, vehicleTypeService.Object);

                var vehicleUpdateId = -1;
                var userId = -1;
                //Assert
                await Assert.ThrowsExceptionAsync<NotFoundException>(async () => await sut.UpdateByCustomerAsync(vehicleUpdateId, userId, new VehicleUpdateByCustomerDTO()));
            }
        }
    }
}
