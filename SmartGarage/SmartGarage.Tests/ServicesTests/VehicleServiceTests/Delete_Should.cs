﻿using CustomExceptions.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SmartGarage.Data;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServicesTests.VehicleServiceTests
{
    [TestClass]
    public class Delete_Should
    {
        [TestMethod]
        public void ShouldDeleteVehicle_WhenPassedCorrectUserAndNoVisitsToThisVehicle()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ShouldDeleteVehicle_WhenPassedCorrectUserAndNoVisitsToThisVehicle));
            var userService = new Mock<IUserService>();
            var modelService = new Mock<IModelService>();
            var vehicleTypeService = new Mock<IVehicleTypeService>();
            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.Visits.AddRange(Utils.GetVisits());
                arrangeContext.Vehicles.AddRange(Utils.GetVehicles());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new VehicleService(actContext, userService.Object, modelService.Object, vehicleTypeService.Object);
                var expected = actContext.Vehicles.Count() - 1;

                int vehicleId = 4;
                var result = sut.DeleteAsync(vehicleId);
                actContext.SaveChanges();

                var given = actContext.Vehicles.Count();

                //Assert
                Assert.AreEqual(expected, given);
            }
        }

        [TestMethod]
        public async Task ShouldThrowArgumentNull_IfPassedIncorecVehicletId()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ShouldThrowArgumentNull_IfPassedIncorecVehicletId));
            var userService = new Mock<IUserService>();
            var modelService = new Mock<IModelService>();
            var vehicleTypeService = new Mock<IVehicleTypeService>();
           
            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.Visits.AddRange(Utils.GetVisits());
                arrangeContext.Vehicles.AddRange(Utils.GetVehicles());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new VehicleService(actContext, userService.Object, modelService.Object, vehicleTypeService.Object);

                int vehicleId = -1;
                //Assert
                await Assert.ThrowsExceptionAsync<NotFoundException>(async () => await sut.DeleteAsync(vehicleId));
            }
        }

        [TestMethod]
        public async Task ShouldThrowCanNotBeDelete_IfPassedVehicletIdWithReferenceToVisits()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ShouldThrowCanNotBeDelete_IfPassedVehicletIdWithReferenceToVisits));
            var userService = new Mock<IUserService>();
            var modelService = new Mock<IModelService>();
            var vehicleTypeService = new Mock<IVehicleTypeService>();

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.Visits.AddRange(Utils.GetVisits());
                arrangeContext.Vehicles.AddRange(Utils.GetVehicles());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new VehicleService(actContext, userService.Object, modelService.Object, vehicleTypeService.Object);

                int vehicleId = 1;
                //Assert
                await Assert.ThrowsExceptionAsync<CanNotBeDeleteException>(async () => await sut.DeleteAsync(vehicleId));
            }
        }
    }
}
