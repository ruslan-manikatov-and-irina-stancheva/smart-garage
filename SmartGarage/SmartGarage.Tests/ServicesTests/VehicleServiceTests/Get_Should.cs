﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SmartGarage.Data;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartGarage.Tests.ServicesTests.VehicleServiceTests
{
    [TestClass]
    public class Get_Should
    {
        [TestMethod]
        public void Vehicle_GetAllData_ShouldReturnCorrectCount()
        {
            var options = Utils.GetOptions(nameof(Vehicle_GetAllData_ShouldReturnCorrectCount));

            var userService = new Mock<IUserService>();
            var modelService = new Mock<IModelService>();
            var vehicleTypeService = new Mock<IVehicleTypeService>();

            using (var arrangeContext = new SmartGarageDbContext(options))
            {

                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.Visits.AddRange(Utils.GetVisits());
                arrangeContext.Vehicles.AddRange(Utils.GetVehicles());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new VehicleService(actContext, userService.Object, modelService.Object, vehicleTypeService.Object);
                var result = sut.GetAllAsync();

                var expected = actContext.Vehicles
                    .ToList();

                //Assert
                Assert.AreEqual(expected.Count(), result.Result.Count());
            }
        }

        [TestMethod]
        public void Vehicle_GetByVehicleId_ShouldReturnCorrectData()
        {
            var options = Utils.GetOptions(nameof(Vehicle_GetByVehicleId_ShouldReturnCorrectData));

            var userService = new Mock<IUserService>();
            var modelService = new Mock<IModelService>();
            var vehicleTypeService = new Mock<IVehicleTypeService>();

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.Visits.AddRange(Utils.GetVisits());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.Vehicles.AddRange(Utils.GetVehicles());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new VehicleService(actContext, userService.Object, modelService.Object, vehicleTypeService.Object);
                int vehicleId = 1;
                var result = sut.GetAsync(vehicleId);

                var expected = actContext.Vehicles
                    .First(v => v.VehicleId == vehicleId);

                //Assert
                Assert.AreEqual(expected.Vin, result.Result.Vin);
                Assert.AreEqual(expected.RegistrationPlate, result.Result.RegistrationPlate);
            }
        }

        [TestMethod]
        public void Vehicle_GetByCustomerId_ShouldReturnCorrectData()
        {
            var options = Utils.GetOptions(nameof(Vehicle_GetByCustomerId_ShouldReturnCorrectData));

            var userService = new Mock<IUserService>();
            var modelService = new Mock<IModelService>();
            var vehicleTypeService = new Mock<IVehicleTypeService>();

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.Visits.AddRange(Utils.GetVisits());
                arrangeContext.Vehicles.AddRange(Utils.GetVehicles());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new VehicleService(actContext, userService.Object, modelService.Object, vehicleTypeService.Object);
                int ownerId = 1;
                var result = sut.GetByCustomer(ownerId);

                var expected = actContext.Vehicles
                    .Where(v => v.Owner.Id == ownerId);

                //Assert
                Assert.AreEqual(expected.First().Vin, result.First().Vin);
            }
        }
    }
}
