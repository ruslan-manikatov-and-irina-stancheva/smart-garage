﻿using AutoMapper;
using CustomExceptions.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data;
using SmartGarage.Services.DTO;
using SmartGarage.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServicesTests.ModelServiceTests
{
    [TestClass]
    public class Update_Should
    {
        [TestMethod]
        public void ShouldUpdateModelByGivenId_WhenPassedCorrectParameters()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ShouldUpdateModelByGivenId_WhenPassedCorrectParameters));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new ModelService(actContext, mapper);
                //make = manufacturer
                var makeDTO = Utils.GetCarModelCreateDto();
                int makeId = 1;
                var result = sut.UpdateAsync(makeId, makeDTO);
                var expected = actContext.Models.First(x => x.ManufacturerId == makeId);

                //Assert
                Assert.AreEqual(expected.Name, makeDTO.Name);
                Assert.AreEqual(typeof(CarModelCreateDTO), result.Result.GetType());
            }
        }

        [TestMethod]
        public async Task PassInvalidIdForMake_ShouldThrowManufacturerNotFound()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(PassInvalidIdForMake_ShouldThrowManufacturerNotFound));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new ManufacturerService(actContext, mapper);
                //make = manufacturer
                var makeDTO = Utils.GetManufacturerCreateDTO().First();
                int makeId = -1;

                //Assert
                await Assert.ThrowsExceptionAsync<ManufacturerNotFoundException>(async () => await sut.UpdateAsync(makeId, makeDTO));
            }
        }

        [TestMethod]
        public async Task PassNullForName_ShouldThrowManufacturerNotFound()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(PassNullForName_ShouldThrowManufacturerNotFound));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new ManufacturerService(actContext, mapper);
                //make = manufacturer
                var makeDTO = Utils.GetManufacturerCreateDTO().First();
                makeDTO.Name = null;
                int makeId = 1;


                //Assert
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(async () => await sut.UpdateAsync(makeId, makeDTO));
            }
        }

        [TestMethod]
        public async Task PassNullForCode_ShouldThrowManufacturerNotFound()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(PassNullForCode_ShouldThrowManufacturerNotFound));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new ManufacturerService(actContext, mapper);
                //make = manufacturer
                var makeDTO = new ManufacturerCreateDTO();
                makeDTO.Name = "Audi";
                makeDTO.Code = null;
                int makeId = 1;
                //Assert
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(async () => await sut.UpdateAsync(makeId, makeDTO));
            }
        }
    }
}
