﻿using AutoMapper;
using CustomExceptions.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data;
using SmartGarage.Services.DTO;
using SmartGarage.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServicesTests.ModelServiceTests
{
    [TestClass]
    public class Create_Should
    {
        [TestMethod]
        public void Model_CreateNew_ShouldAddOneInDatabase()
        {
            var options = Utils.GetOptions(nameof(Model_CreateNew_ShouldAddOneInDatabase));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new ModelService(actContext, mapper);

                var expected = actContext.Models.Count() + 1;

                var modelDto = Utils.GetCarModelCreateDto();
                var result = sut.CreateAsync(modelDto);

                //Assert
                Assert.AreEqual(expected, actContext.Models.Count());
            }
        }

        [TestMethod]
        public async Task Model_PassNullParametersToName_ShouldThrowNullException()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Model_PassNullParametersToName_ShouldThrowNullException));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());

            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new ModelService(actContext, mapper);

                var modelDTO = new CarModelCreateDTO();
                modelDTO.Name = null;
                modelDTO.Code = "Test";
                modelDTO.ManufacturerId = 1;
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(async () => await sut.CreateAsync(modelDTO));
            }

        }
        [TestMethod]
        public async Task Model_PassNullParametersToCode_ShouldThrowNullException()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Model_PassNullParametersToName_ShouldThrowNullException));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());

            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new ModelService(actContext, mapper);

                var modelDTO = new CarModelCreateDTO();
                modelDTO.Name = "Test";
                modelDTO.Code = null;
                modelDTO.ManufacturerId = 1;
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(async () => await sut.CreateAsync(modelDTO));
            }
        }
        [TestMethod]
        public async Task Model_PassNullParametersToMakeId_ShouldThrowManufacturerNotFound()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Model_PassNullParametersToMakeId_ShouldThrowManufacturerNotFound));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new ModelService(actContext, mapper);

                var modelDTO = Utils.GetCarModelCreateDto();
                modelDTO.ManufacturerId = -1;
                await Assert.ThrowsExceptionAsync<ManufacturerNotFoundException>(async () => await sut.CreateAsync(modelDTO));
            }

        }
    }
}
