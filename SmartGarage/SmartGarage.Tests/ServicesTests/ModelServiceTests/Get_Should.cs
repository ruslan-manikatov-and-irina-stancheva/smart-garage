﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using AutoMapper;
using Moq;
using SmartGarage.Services.Services;
using SmartGarage.Data;

namespace SmartGarage.Tests.ServicesTests.ModelServiceTests
{
    [TestClass]
    public class Get_Should
    {
       
        [TestMethod]
       
        public void Model_GetAllData_ShouldReturnCorrectCount()     
        {      
            var options = Utils.GetOptions(nameof(Model_GetAllData_ShouldReturnCorrectCount));      
            var mapper = new Mock<IMapper>();       
            using (var arrangeContext = new SmartGarageDbContext(options))       
            {      
                arrangeContext.Models.AddRange(Utils.GetModels());      
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());      
                arrangeContext.SaveChanges();
            }
       
            //Act      
            using (var actContext = new SmartGarageDbContext(options))       
            {       
                var sut = new ModelService(actContext, mapper.Object);    
                var result = sut.GetAllAsync();
                var expected = actContext.Models
                    .ToList();

                //Assert
       
                Assert.AreEqual(expected.Count(), result.Result.Count);  
            }

        }

        [TestMethod]
        public void Model_GetById_ShouldReturnCorrectData()
        {
            var options = Utils.GetOptions(nameof(Model_GetById_ShouldReturnCorrectData));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new ModelService(actContext, mapper);
                var result = sut.GetAsync(1);
                var expected = actContext.Models
                    .ToList().First();
                //Assert
                Assert.AreEqual(expected.ManufacturerId, result.Result.ManufacturerId);
                Assert.AreEqual(expected.ModelId, result.Result.ModelId);
                Assert.AreEqual(expected.Name, result.Result.Name);
            }
        }

    }
}
