﻿using AutoMapper;
using CustomExceptions.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SmartGarage.Data;
using SmartGarage.Services.Services;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServicesTests.ModelServiceTests
{
    [TestClass]
    public class Delete_Should
    {
        [TestMethod]
        public void ShouldDeleteCarModel_WhenPassedCorrectId()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ShouldDeleteCarModel_WhenPassedCorrectId));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            using (var arrangeContext = new SmartGarageDbContext(options))
            {

                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new ModelService(actContext, mapper);
                var expected = actContext.Models.Count() - 1;

                int carModelId = 3;
                var result = sut.DeleteAsync(carModelId);
                actContext.SaveChanges();

                var given = actContext.Models.Count();

                //Assert
                Assert.AreEqual(expected, given);
                Assert.AreEqual(true, result.Result);
            }
        }
        
        [TestMethod]
        public async Task ShouldThrowModelNotFound_IfPassedIncorectId()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ShouldThrowModelNotFound_IfPassedIncorectId));
            //IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var mapper = new Mock<IMapper>();

            using (var arrangeContext = new SmartGarageDbContext(options))
            {

                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new ModelService(actContext, mapper.Object);

                int carModelId = -1;
                //Assert
                await Assert.ThrowsExceptionAsync<ModelNotFoundException>(async () => await sut.DeleteAsync(carModelId));
            }

        }
    }
}
