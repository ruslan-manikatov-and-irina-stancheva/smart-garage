﻿using AutoMapper;
using CustomExceptions.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SmartGarage.Data;
using SmartGarage.Data.Models;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.Services;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServicesTests.ServiceServiceTests
{
    [TestClass]
    public class Get_Should
    {
        [TestMethod]
        public void Service_GetAllData_ShouldReturnCorrectCount()
        {
            var options = Utils.GetOptions(nameof(Service_GetAllData_ShouldReturnCorrectCount));
            var mapper = new Mock<IMapper>();
            var serviceVehicleTypes = new Mock<IServiceVehicleTypesService>();
            
            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Services.AddRange(Utils.GetServices());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new ServiceService(actContext, mapper.Object, serviceVehicleTypes.Object);
                var result = sut.GetAllAsync();

                var expected = actContext.Services
                    .ToList();

                //Assert
                Assert.AreEqual(expected.Count(), result.Result.Count());
            }
        }

        [TestMethod]
        public void GetByServiceId_ShouldReturnCorrectData()
        {
            var options = Utils.GetOptions(nameof(GetByServiceId_ShouldReturnCorrectData));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var ServiceVeicleTypes = new Mock<IServiceVehicleTypesService>();
            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Services.AddRange(Utils.GetServices());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new ServiceService(actContext, mapper, ServiceVeicleTypes.Object);
                var ServiceId = 1;
                var result = sut.GetAsync(ServiceId);

                var expected = actContext.Services
                    .First(s => s.ServiceId == ServiceId);

                //Assert
                Assert.AreEqual(expected.Name, result.Result.Name);
                Assert.AreEqual(expected.Price, result.Result.Price);
            }
        }

        [TestMethod]
        public async Task PassInvalidServiceId_ShouldThrowUserNotFound()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(PassInvalidServiceId_ShouldThrowUserNotFound));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var ServiceVeicleTypes = new Mock<IServiceVehicleTypesService>();

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Services.AddRange(Utils.GetServices());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new ServiceService(actContext, mapper, ServiceVeicleTypes.Object);
                //make = manufacturer
                var serviceId = -1;

                //Assert
                await Assert.ThrowsExceptionAsync<NotFoundException>(async () => await sut.GetAsync(serviceId));
            }
        }

        [TestMethod]
        public void GetByServiceId_ShouldReturnServiceAndType()
        {
            var options = Utils.GetOptions(nameof(GetByServiceId_ShouldReturnServiceAndType));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var ServiceVeicleTypes = new Mock<IServiceVehicleTypesService>();
            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Services.AddRange(Utils.GetServices());
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.ServiceVehicleTypes.AddRange(Utils.GetServiceVehicleType());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new ServiceService(actContext, mapper, ServiceVeicleTypes.Object);
                var ServiceId = 1;
                var result = sut.GetServiceAndTypeAsync(ServiceId);

                var expected = actContext.Services
                    .First(s => s.ServiceId == ServiceId);

                //Assert
                Assert.AreEqual(expected.Name, result.Result.First().Name);
                Assert.AreEqual(expected.Price, result.Result.First().Price);
            }
        }

        [TestMethod]
        public void GetByNameAndPrice_ShouldReturnCorectData()
        {
            var options = Utils.GetOptions(nameof(GetByNameAndPrice_ShouldReturnCorectData));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var ServiceVeicleTypes = new Mock<IServiceVehicleTypesService>();
            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Services.AddRange(Utils.GetServices());
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.ServiceVehicleTypes.AddRange(Utils.GetServiceVehicleType());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new ServiceService(actContext, mapper, ServiceVeicleTypes.Object);
                

                var expected = actContext.Services
                    .First(s => s.ServiceId == 1);

                var result = sut.GetByNameAndPriceAsync(expected.Name, expected.Price);

                //Assert
                Assert.AreEqual(expected.Name, result.Result.First().Name);
                Assert.AreEqual(expected.Price, result.Result.First().Price);
            }
        }
        [TestMethod]
        public void GetByName_ShouldReturnCorectData()
        {
            var options = Utils.GetOptions(nameof(GetByName_ShouldReturnCorectData));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var ServiceVeicleTypes = new Mock<IServiceVehicleTypesService>();
            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Services.AddRange(Utils.GetServices());
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.ServiceVehicleTypes.AddRange(Utils.GetServiceVehicleType());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new ServiceService(actContext, mapper, ServiceVeicleTypes.Object);


                var expected = actContext.Services
                    .First(s => s.ServiceId == 1);

                var result = sut.GetByNameAsync(expected.Name);

                //Assert
                Assert.AreEqual(expected.Name, result.Result.First().Name);
                Assert.AreEqual(expected.Price, result.Result.First().Price);
            }
        }

        [TestMethod]
        public void GetByPrice_ShouldReturnCorectData()
        {
            var options = Utils.GetOptions(nameof(GetByPrice_ShouldReturnCorectData));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var ServiceVeicleTypes = new Mock<IServiceVehicleTypesService>();
            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Services.AddRange(Utils.GetServices());
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.ServiceVehicleTypes.AddRange(Utils.GetServiceVehicleType());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new ServiceService(actContext, mapper, ServiceVeicleTypes.Object);


                var expected = actContext.Services
                    .First(s => s.ServiceId == 1);

                var result = sut.GetByPriceAsync(expected.Price);

                //Assert
                Assert.AreEqual(expected.Name, result.Result.First().Name);
                Assert.AreEqual(expected.Price, result.Result.First().Price);
            }
        }
    }
}
