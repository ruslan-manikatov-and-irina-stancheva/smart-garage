﻿using AutoMapper;
using CustomExceptions.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SmartGarage.Data;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTO;
using SmartGarage.Services.Services;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServicesTests.ServiceServiceTests
{
    [TestClass]
    public class Update_Should
    {

        [TestMethod]
        public void Service_UpdateNew_ShouldReturnCorectData()
        {
            var options = Utils.GetOptions(nameof(Service_UpdateNew_ShouldReturnCorectData));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var ServiceVeicleTypes = new Mock<IServiceVehicleTypesService>();

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Services.AddRange(Utils.GetServices());
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.ServiceVehicleTypes.AddRange(Utils.GetServiceVehicleType());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new ServiceService(actContext, mapper, ServiceVeicleTypes.Object);
                var serviceDto = new ServiceUpdateDTO();
                serviceDto.ServiceId = 1;
                serviceDto.Name = "Test";
                serviceDto.Price = 10;
                var result = sut.UpdateAsync(1, serviceDto);
                var expected = actContext.Services.First(s => s.ServiceId == 1);

                //Assert
                Assert.AreEqual(expected.Name, result.Result.Name);
                Assert.AreEqual(expected.Price, result.Result.Price);
            }
        }

        [TestMethod]
        public async Task PassIncorectServiceId_ShouldThrowNotFound()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(PassIncorectServiceId_ShouldThrowNotFound));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var ServiceVeicleTypes = new Mock<IServiceVehicleTypesService>();

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Services.AddRange(Utils.GetServices());
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.ServiceVehicleTypes.AddRange(Utils.GetServiceVehicleType());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new ServiceService(actContext, mapper, ServiceVeicleTypes.Object);
                var serviceDto = new ServiceUpdateDTO();
                serviceDto.ServiceId = 1;
                serviceDto.Name = "Test";
                serviceDto.Price = 10;
                var result = sut.UpdateAsync(1, serviceDto);
                var expected = actContext.Services.First(s => s.ServiceId == 1);

                var serviceUpdateId = -1;
                //Assert
                await Assert.ThrowsExceptionAsync<NotFoundException>(async () => await sut.UpdateAsync(serviceUpdateId, serviceDto));
            }
        }

        [TestMethod]
        public void Service_UpdateNewOverload_ShouldReturnCorectData()
        {
            var options = Utils.GetOptions(nameof(Service_UpdateNewOverload_ShouldReturnCorectData));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var ServiceVeicleTypes = new Mock<IServiceVehicleTypesService>();

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Services.AddRange(Utils.GetServices());
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.ServiceVehicleTypes.AddRange(Utils.GetServiceVehicleType());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new ServiceService(actContext, mapper, ServiceVeicleTypes.Object);
                var serviceDto = new ServiceUpdateDTO();
                serviceDto.ServiceId = 1;
                serviceDto.Name = "Test";
                serviceDto.Price = 10;
                List<int> types = new List<int>() { 1, 2, 3};

                var result = sut.UpdateAsync(1, serviceDto, types);
                var expected = actContext.Services.First(s => s.ServiceId == 1);

                //Assert
                Assert.AreEqual(expected.Name, result.Result.Name);
                Assert.AreEqual(expected.Price, result.Result.Price);
            }
        }

        [TestMethod]
        public async Task PassIncorectServiceIdToOverload_ShouldThrowNotFound()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(PassIncorectServiceIdToOverload_ShouldThrowNotFound));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var ServiceVeicleTypes = new Mock<IServiceVehicleTypesService>();

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Services.AddRange(Utils.GetServices());
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.ServiceVehicleTypes.AddRange(Utils.GetServiceVehicleType());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new ServiceService(actContext, mapper, ServiceVeicleTypes.Object);
                var serviceDto = new ServiceUpdateDTO();
                serviceDto.ServiceId = 1;
                serviceDto.Name = "Test";
                serviceDto.Price = 10;
                var result = sut.UpdateAsync(1, serviceDto);
                var expected = actContext.Services.First(s => s.ServiceId == 1);
                List<int> types = new List<int>() { 1, 2, 3 };

                var serviceUpdateId = -1;
                //Assert
                await Assert.ThrowsExceptionAsync<NotFoundException>(async () => await sut.UpdateAsync(serviceUpdateId, serviceDto, types));
            }
        }
    }
}
