﻿using AutoMapper;
using CustomExceptions.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SmartGarage.Data;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServicesTests.ServiceServiceTests
{
    [TestClass]
    public class Delete_Should
    {
        [TestMethod]
        public void ShouldDeleteService_WhenPassedCorrectServiceId()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ShouldDeleteService_WhenPassedCorrectServiceId));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var ServiceVeicleTypes = new Mock<IServiceVehicleTypesService>();

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Services.AddRange(Utils.GetServices());
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.ServiceVehicleTypes.AddRange(Utils.GetServiceVehicleType());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new ServiceService(actContext, mapper, ServiceVeicleTypes.Object);
                var expected = actContext.Services.Count() - 1;

                int serviceId = 3;
                var result = sut.DeleteAsync(serviceId);
                actContext.SaveChanges();

                var given = actContext.Services.Count();

                //Assert
                Assert.AreEqual(expected, given);
            }
        }
        [TestMethod]
        public async Task ShouldThrowNotFound_IfPassedIncorectId()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ShouldThrowNotFound_IfPassedIncorectId));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var ServiceVeicleTypes = new Mock<IServiceVehicleTypesService>();

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Services.AddRange(Utils.GetServices());
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.ServiceVehicleTypes.AddRange(Utils.GetServiceVehicleType());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new ServiceService(actContext, mapper, ServiceVeicleTypes.Object);

                int serviceId = -1;
                //Assert
                await Assert.ThrowsExceptionAsync<NotFoundException>(async () => await sut.DeleteAsync(serviceId));
            }
        }
    }
}
