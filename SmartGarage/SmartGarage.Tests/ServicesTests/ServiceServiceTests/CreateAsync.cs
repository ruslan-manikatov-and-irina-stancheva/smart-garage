﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SmartGarage.Data;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTO;
using SmartGarage.Services.Services;
using System.Collections.Generic;
using System.Linq;

namespace SmartGarage.Tests.ServicesTests.ServiceServiceTests
{
    [TestClass]
    public class CreateAsync
    {
        [TestMethod]
        public void Service_CreateNew_ShouldAddOneInDatabase()
        {
            var options = Utils.GetOptions(nameof(Service_CreateNew_ShouldAddOneInDatabase));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var ServiceVeicleTypes = new Mock<IServiceVehicleTypesService>();

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Services.AddRange(Utils.GetServices());
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.ServiceVehicleTypes.AddRange(Utils.GetServiceVehicleType());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new ServiceService(actContext, mapper, ServiceVeicleTypes.Object);
                var serviceDto = new ServiceCreateDTO();
                serviceDto.Name = "Test";
                serviceDto.Price = 10;

                var expected = actContext.Services.Count() + 1;
                var result = sut.CreateAsync(serviceDto);

                //Assert
                Assert.AreEqual(expected, actContext.Services.Count());
            }
        }

        [TestMethod]
        public void Service_CreateNewOverload_ShouldAddOneInDatabase()
        {
            var options = Utils.GetOptions(nameof(Service_CreateNewOverload_ShouldAddOneInDatabase));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var ServiceVeicleTypes = new Mock<IServiceVehicleTypesService>();

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Services.AddRange(Utils.GetServices());
                arrangeContext.VehicleTypes.AddRange(Utils.GetVehicleTypes());
                arrangeContext.ServiceVehicleTypes.AddRange(Utils.GetServiceVehicleType());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new ServiceService(actContext, mapper, ServiceVeicleTypes.Object);
                var serviceDto = new ServiceCreateDTO();

                serviceDto.Name = "Test";
                serviceDto.Price = 10;
                List<int> types = new List<int>() { 1, 2 };

                var expected = actContext.Services.Count() + 1;
                var result = sut.CreateAsync(serviceDto, types);

                //Assert
                Assert.AreEqual(expected, actContext.Services.Count());
            }
        }
    }
}
