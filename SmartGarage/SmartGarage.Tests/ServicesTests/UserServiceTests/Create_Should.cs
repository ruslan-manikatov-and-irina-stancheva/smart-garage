﻿using Microsoft.AspNetCore.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data;
using SmartGarage.Data.Models;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTO;
using SmartGarage.Services.Services;
using System.Linq;
using AutoMapper;
using Moq;

namespace SmartGarage.Tests.ServicesTests.UserServiceTests
{
    [TestClass]
    public class Create_Should
    {
        [TestMethod]
        public void User_CreateNew_ShouldAddOneInDatabase()
        {
            var options = Utils.GetOptions(nameof(User_CreateNew_ShouldAddOneInDatabase));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var emailService = new Mock<IEmailService>();
            var store = new Mock<IUserStore<User>>();
            Mock<UserManager<User>> userManager = new Mock<UserManager<User>>(store.Object, null, null, null, null, null, null, null, null);


            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Roles.AddRange(Utils.GetRoles());
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.Vehicles.AddRange(Utils.GetVehicles());
                arrangeContext.Visits.AddRange(Utils.GetVisits());            
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new UserService(actContext, mapper, emailService.Object, userManager.Object);
                var userDto = new UserCreateDTO();
                userDto.Email = "TestEmail";
                userDto.FirstName = "FirstTest";
                userDto.LastName = "LastTest";
                userDto.PhoneNumber = "0999099111";
                userDto.RoleId = 1;

                var expected = actContext.Users.Count() + 1;
                var result = sut.CreateAsync(userDto);

                //Assert
                Assert.AreEqual(expected, actContext.Users.Count());
            }
        }
    }
}
