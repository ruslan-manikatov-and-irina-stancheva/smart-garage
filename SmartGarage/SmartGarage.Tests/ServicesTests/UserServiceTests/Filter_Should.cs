﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SmartGarage.Data;
using SmartGarage.Data.Models;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTO;
using SmartGarage.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServicesTests.UserServiceTests
{
    [TestClass]
    public class Filter_Should
    {
        [TestMethod]
        public async Task FilterByLastName_ShouldReturnCorrectCount()
        {
            var options = Utils.GetOptions(nameof(FilterByLastName_ShouldReturnCorrectCount));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var emailService = new Mock<IEmailService>();
            var store = new Mock<IUserStore<User>>();
            Mock<UserManager<User>> userManager = new Mock<UserManager<User>>(store.Object, null, null, null, null, null, null, null, null);

            using (var arrangeContext = new SmartGarageDbContext(options))
            {

                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new UserService(actContext, mapper, emailService.Object, userManager.Object);
                var filterParam = "manikatov";
                var result = sut.FilterUsersByLastName(filterParam);

                var expected = await actContext.Users
               .Where(u => u.LastName.Contains(filterParam))
               .ToListAsync();
                //Assert
                Assert.AreEqual(expected.Count(), result.Result.Count());
            }
        }

        [TestMethod]
        public async Task FilterByEmail_ShouldReturnCorrectCount()
        {
            var options = Utils.GetOptions(nameof(FilterByEmail_ShouldReturnCorrectCount));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var emailService = new Mock<IEmailService>();
            var store = new Mock<IUserStore<User>>();
            Mock<UserManager<User>> userManager = new Mock<UserManager<User>>(store.Object, null, null, null, null, null, null, null, null);

            using (var arrangeContext = new SmartGarageDbContext(options))
            {

                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new UserService(actContext, mapper, emailService.Object, userManager.Object);
                var filterParam = "ruslan@smartgarage";
                var result = sut.FilterUsersByEmail(filterParam);

                var expected = await actContext.Users
               .Where(u => u.Email.Contains(filterParam))
               .ToListAsync();
                //Assert
                Assert.AreEqual(expected.Count(), result.Result.Count());
            }
        }

        [TestMethod]
        public async Task FilterByPhone_ShouldReturnCorrectCount()
        {
            var options = Utils.GetOptions(nameof(FilterByPhone_ShouldReturnCorrectCount));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var emailService = new Mock<IEmailService>();
            var store = new Mock<IUserStore<User>>();
            Mock<UserManager<User>> userManager = new Mock<UserManager<User>>(store.Object, null, null, null, null, null, null, null, null);

            using (var arrangeContext = new SmartGarageDbContext(options))
            {

                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new UserService(actContext, mapper, emailService.Object, userManager.Object);
                var filterParam = "0887123213";
                var result = sut.FilterUsersByPhone(filterParam);

                var expected = await actContext.Users
               .Where(u => u.PhoneNumber.Contains(filterParam))
               .ToListAsync();
                //Assert
                Assert.AreEqual(expected.Count(), result.Result.Count());
            }
        }

        [TestMethod]
        public async Task FilterByVehicleModel_ShouldReturnCorrectCount()
        {
            var options = Utils.GetOptions(nameof(FilterByVehicleModel_ShouldReturnCorrectCount));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var emailService = new Mock<IEmailService>();
            var store = new Mock<IUserStore<User>>();
            Mock<UserManager<User>> userManager = new Mock<UserManager<User>>(store.Object, null, null, null, null, null, null, null, null);

            using (var arrangeContext = new SmartGarageDbContext(options))
            {

                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.Vehicles.AddRange(Utils.GetVehicles());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new UserService(actContext, mapper, emailService.Object, userManager.Object);
                var filterParam = "A6";
                var result = sut.FilterUsersByVehicleModel(filterParam);


                var expected = await actContext.Vehicles
                .Where(v => v.Model.Name == filterParam)
                .Select(v => v.Owner)
                .ToListAsync();

                //Assert
                Assert.AreEqual(expected.Count(), result.Result.Count());
            }
        }

        [TestMethod]
        public async Task FilterByVehicleMake_ShouldReturnCorrectCount()
        {
            var options = Utils.GetOptions(nameof(FilterByVehicleMake_ShouldReturnCorrectCount));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var emailService = new Mock<IEmailService>();
            var store = new Mock<IUserStore<User>>();
            Mock<UserManager<User>> userManager = new Mock<UserManager<User>>(store.Object, null, null, null, null, null, null, null, null);

            using (var arrangeContext = new SmartGarageDbContext(options))
            {

                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.Vehicles.AddRange(Utils.GetVehicles());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new UserService(actContext, mapper, emailService.Object, userManager.Object);
                var filterParam = "Audi";
                var result = sut.FilterUsersByVehicleMake(filterParam);


                var expected = await actContext.Vehicles
                .Where(v => v.Model.Manufacturer.Name == filterParam)
                .Select(v => v.Owner)
                .ToListAsync();

                //Assert
                Assert.AreEqual(expected.Count(), result.Result.Count());
            }
        }

        [TestMethod]
        public async Task FilterByVisitRange_ShouldReturnCorrectCount()
        {
            var options = Utils.GetOptions(nameof(FilterByVisitRange_ShouldReturnCorrectCount));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var emailService = new Mock<IEmailService>();
            var store = new Mock<IUserStore<User>>();
            Mock<UserManager<User>> userManager = new Mock<UserManager<User>>(store.Object, null, null, null, null, null, null, null, null);

            using (var arrangeContext = new SmartGarageDbContext(options))
            {

                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.Vehicles.AddRange(Utils.GetVehicles());
                arrangeContext.Visits.AddRange(Utils.GetVisits());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new UserService(actContext, mapper, emailService.Object, userManager.Object);
                var filterParamStartDate = DateTime.Now;
                var filterParamEndDate = DateTime.Now;
                var result = sut.FilterUsersByVisitInRange(filterParamStartDate, filterParamEndDate);

                var expected = await actContext.Visits
                .Where(v => (v.ArriveDate >= filterParamStartDate) && v.ArriveDate <= filterParamEndDate)
                .Select(v => v.Vehicle)
                .Select(v => v.Owner)
                .Select(user => mapper.Map<UserGetDTO>(user))
                .ToListAsync();
                //Assert
                Assert.AreEqual(expected.Count(), result.Result.Count());
            }
        }

        [TestMethod]
        public void SortUsersByName_ShouldReturnCorrectDataForFirst()
        {
            var options = Utils.GetOptions(nameof(SortUsersByName_ShouldReturnCorrectDataForFirst));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var emailService = new Mock<IEmailService>();
            var store = new Mock<IUserStore<User>>();
            Mock<UserManager<User>> userManager = new Mock<UserManager<User>>(store.Object, null, null, null, null, null, null, null, null);

            using (var arrangeContext = new SmartGarageDbContext(options))
            {

                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.Vehicles.AddRange(Utils.GetVehicles());
                arrangeContext.Visits.AddRange(Utils.GetVisits());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new UserService(actContext, mapper, emailService.Object, userManager.Object);
                var result = sut.SortUsersByName();

                var expected = actContext.Users
                .OrderBy(u => u.FirstName)
                .ThenBy(u => u.LastName)
                .ToListAsync();

                //Assert
                Assert.AreEqual(expected.Result.First().FirstName, result.Result.First().FirstName);
                Assert.AreEqual(expected.Result.First().Email, result.Result.First().Email);
            }
        }

        [TestMethod]
        public void SortVisitDate_ShouldReturnCorrectDataForFirst()
        {
            var options = Utils.GetOptions(nameof(SortVisitDate_ShouldReturnCorrectDataForFirst));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var emailService = new Mock<IEmailService>();
            var store = new Mock<IUserStore<User>>();
            Mock<UserManager<User>> userManager = new Mock<UserManager<User>>(store.Object, null, null, null, null, null, null, null, null);

            using (var arrangeContext = new SmartGarageDbContext(options))
            {

                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.Vehicles.AddRange(Utils.GetVehicles());
                arrangeContext.Visits.AddRange(Utils.GetVisits());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new UserService(actContext, mapper, emailService.Object, userManager.Object);
                var result = sut.SortUsersByVisitDate();

                var expected = actContext.Visits
                .OrderBy(v => v.ArriveDate)
                .Select(v => v.Vehicle)
                .Select(v => v.Owner)
                .Select(user => mapper.Map<UserGetDTO>(user))
                .ToListAsync();

                //Assert
                Assert.AreEqual(expected.Result.First().FirstName, result.Result.First().FirstName);
                Assert.AreEqual(expected.Result.First().Email, result.Result.First().Email);
            }
        }
    }
}
