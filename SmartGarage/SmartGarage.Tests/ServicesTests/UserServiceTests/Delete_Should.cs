﻿using AutoMapper;
using CustomExceptions.Exceptions;
using Microsoft.AspNetCore.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SmartGarage.Data;
using SmartGarage.Data.Models;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServicesTests.UserServiceTests
{
    [TestClass]
    public class Delete_Should
    {
        [TestMethod]
        public void ShouldDeleteUser_WhenPassedCorrectUserAndNoVehiclesPointToThisUser()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ShouldDeleteUser_WhenPassedCorrectUserAndNoVehiclesPointToThisUser));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var emailService = new Mock<IEmailService>();
            var store = new Mock<IUserStore<User>>();
            Mock<UserManager<User>> userManager = new Mock<UserManager<User>>(store.Object, null, null, null, null, null, null, null, null);

            using (var arrangeContext = new SmartGarageDbContext(options))
            {

                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.Vehicles.AddRange(Utils.GetVehicles());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new UserService(actContext, mapper, emailService.Object, userManager.Object);
                var expected = actContext.Users.Count() - 1;

                int makeId = 3;
                var result = sut.DeleteAsync(makeId);
                actContext.SaveChanges();

                var given = actContext.Users.Count();

                //Assert
                Assert.AreEqual(expected, given);
                Assert.AreEqual(true, result.Result);
            }
        }

        [TestMethod]
        public async Task ShouldThrowUserNotFound_IfPassedIncorectId()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ShouldThrowUserNotFound_IfPassedIncorectId));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var emailService = new Mock<IEmailService>();
            var store = new Mock<IUserStore<User>>();
            Mock<UserManager<User>> userManager = new Mock<UserManager<User>>(store.Object, null, null, null, null, null, null, null, null);

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.Vehicles.AddRange(Utils.GetVehicles());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new UserService(actContext, mapper, emailService.Object, userManager.Object);

                int makeId = -1;
                //Assert
                await Assert.ThrowsExceptionAsync<UserNotFoundException>(async () => await sut.DeleteAsync(makeId));
            }
        }

        [TestMethod]
        public async Task ShouldThrolArgumentOutOfRangeException_IfPassedUserWithVehicles()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ShouldThrolArgumentOutOfRangeException_IfPassedUserWithVehicles));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var emailService = new Mock<IEmailService>();
            var store = new Mock<IUserStore<User>>();
            Mock<UserManager<User>> userManager = new Mock<UserManager<User>>(store.Object, null, null, null, null, null, null, null, null);

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.Vehicles.AddRange(Utils.GetVehicles());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new UserService(actContext, mapper, emailService.Object, userManager.Object);

                int makeId = 1;
                //Assert
                await Assert.ThrowsExceptionAsync<ArgumentOutOfRangeException>(async () => await sut.DeleteAsync(makeId));
            }
        }
    }
}
