﻿using AutoMapper;
using CustomExceptions.Exceptions;
using Microsoft.AspNetCore.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SmartGarage.Data;
using SmartGarage.Data.Models;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTO;
using SmartGarage.Services.Services;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServicesTests.UserServiceTests
{
    [TestClass]
    public class Update_Should
    {
        [TestMethod]
        public void ShouldUpdateUserByGivenId_WhenPassedCorrectParameters()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ShouldUpdateUserByGivenId_WhenPassedCorrectParameters));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var emailService = new Mock<IEmailService>();
            var store = new Mock<IUserStore<User>>();
            Mock<UserManager<User>> userManager = new Mock<UserManager<User>>(store.Object, null, null, null, null, null, null, null, null);

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.Vehicles.AddRange(Utils.GetVehicles());
                arrangeContext.Visits.AddRange(Utils.GetVisits());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new UserService(actContext, mapper, emailService.Object, userManager.Object);
                
                var userDTO = new UserDTO();
                userDTO.FirstName = "TestName";
                userDTO.LastName = "TestLastName";
                userDTO.PhoneNumber = "0888097654";
                userDTO.Email = "usertest@abv.bg";

                var userUpdateId = 1;

                var result = sut.UpdateAsync(userUpdateId, userDTO);
                var expected = actContext.Users.First(x => x.Id == userUpdateId);

                //Assert
                Assert.AreEqual(expected.LastName, result.Result.LastName);
                Assert.AreEqual(typeof(UserDTO), result.Result.GetType());
            }
        }

        [TestMethod]
        public async Task PassIncorectUserId_ShouldThrowUserNotFound()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(PassIncorectUserId_ShouldThrowUserNotFound));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var emailService = new Mock<IEmailService>();
            var store = new Mock<IUserStore<User>>();
            Mock<UserManager<User>> userManager = new Mock<UserManager<User>>(store.Object, null, null, null, null, null, null, null, null);

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.Vehicles.AddRange(Utils.GetVehicles());
                arrangeContext.Visits.AddRange(Utils.GetVisits());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new UserService(actContext, mapper, emailService.Object, userManager.Object);

                var userDTO = new UserDTO();
                userDTO.FirstName = "TestName";
                userDTO.LastName = "TestLastName";
                userDTO.PhoneNumber = "0888097654";
                userDTO.Email = "usertest@abv.bg";

                var userUpdateId = -1;
                //Assert
                await Assert.ThrowsExceptionAsync<UserNotFoundException>(async () => await sut.UpdateAsync(userUpdateId, userDTO));
            }
        }

    }
}
