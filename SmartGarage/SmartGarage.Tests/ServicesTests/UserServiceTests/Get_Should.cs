﻿using AutoMapper;
using CustomExceptions.Exceptions;
using Microsoft.AspNetCore.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SmartGarage.Data;
using SmartGarage.Data.Models;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.Services;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServicesTests.UserServiceTests
{
    [TestClass]
    public class Get_Should
    {
        [TestMethod]
        public void User_GetAllData_ShouldReturnCorrectCount()
        {
            var options = Utils.GetOptions(nameof(User_GetAllData_ShouldReturnCorrectCount));
            var mapper = new Mock<IMapper>();
            var emailService = new Mock<IEmailService>();
            var store = new Mock<IUserStore<User>>();
            Mock<UserManager<User>> userManager = new Mock<UserManager<User>>(store.Object, null, null, null, null, null, null, null, null);
            
            using (var arrangeContext = new SmartGarageDbContext(options))
            {      
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new UserService(actContext, mapper.Object, emailService.Object, userManager.Object);
                var result = sut.GetAllAsync();

                var expected = actContext.Users
                    .ToList();

                //Assert
                Assert.AreEqual(expected.Count(), result.Result.Count());
            }
        }

        [TestMethod]
        public void GetByUserId_ShouldReturnCorrectData()
        {
            var options = Utils.GetOptions(nameof(GetByUserId_ShouldReturnCorrectData));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var emailService = new Mock<IEmailService>();
            var store = new Mock<IUserStore<User>>();
            Mock<UserManager<User>> userManager = new Mock<UserManager<User>>(store.Object, null, null, null, null, null, null, null, null);

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {

                var sut = new UserService(actContext, mapper, emailService.Object, userManager.Object);
                var userId = 1;
                var result = sut.GetAsync(userId);

                var expected = actContext.Users
                    .First(u => u.Id == userId);

                //Assert
                Assert.AreEqual(expected.FirstName, result.Result.FirstName);
                Assert.AreEqual(expected.Email, result.Result.Email);
            }
        }
        [TestMethod]
        public async Task PassInvalidUserId_ShouldThrowUserNotFound()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(PassInvalidUserId_ShouldThrowUserNotFound));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            var emailService = new Mock<IEmailService>();
            var store = new Mock<IUserStore<User>>();
            Mock<UserManager<User>> userManager = new Mock<UserManager<User>>(store.Object, null, null, null, null, null, null, null, null);

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.Manufacturers.AddRange(Utils.GetManufacturers());
                arrangeContext.Models.AddRange(Utils.GetModels());
                arrangeContext.Users.AddRange(Utils.GetUsers());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new UserService(actContext, mapper, emailService.Object, userManager.Object);
                //make = manufacturer
                var userId = -1;
                
                //Assert
                await Assert.ThrowsExceptionAsync<UserNotFoundException>(async () => await sut.GetAsync(userId));
            }
        }
    }
}
