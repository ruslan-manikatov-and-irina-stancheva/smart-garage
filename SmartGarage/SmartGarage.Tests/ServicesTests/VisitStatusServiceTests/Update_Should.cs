﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data;
using SmartGarage.Services.DTO;
using SmartGarage.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServicesTests.VisitStatusServiceTests
{
    [TestClass]
    public class Update_Should
    {

        [TestMethod]
        public void ShouldUpdateModelByGivenId_WhenPassedCorrectParameters()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ShouldUpdateModelByGivenId_WhenPassedCorrectParameters));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.VisitStatuses.AddRange(Utils.GetVisitStatuses());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new VisitStatusService(actContext, mapper);

                var statusDto = new VisitStatusDTO();
                statusDto.VisitStatusId = 1;
                statusDto.Name = "UpdateStatus";
                int updatetStatusId = 1;

                var result = sut.UpdateAsync(updatetStatusId, statusDto);
                var espected = actContext.VisitStatuses.First(x => x.VisitStatusId == updatetStatusId);

                //Assert
                Assert.AreEqual(espected.Name, result.Result.Name);
                Assert.AreEqual(typeof(VisitStatusDTO), result.Result.GetType());
            }
        }
        [TestMethod]
        public async Task PassInvalidIdVisitStatus_ShouldThrowArgumentNullException()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(PassInvalidIdVisitStatus_ShouldThrowArgumentNullException));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.VisitStatuses.AddRange(Utils.GetVisitStatuses());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new VisitStatusService(actContext, mapper);

                var statusDto = new VisitStatusDTO();
                statusDto.VisitStatusId = 1;
                statusDto.Name = "UpdateStatus";
                int updatetStatusId = -1;

                //Assert
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(async () => await sut.UpdateAsync(updatetStatusId, statusDto));
            }
        }
    }
}
