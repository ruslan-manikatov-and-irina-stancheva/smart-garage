﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data;
using SmartGarage.Services.DTO;
using SmartGarage.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartGarage.Tests.ServicesTests.VisitStatusServiceTests
{
    [TestClass]
    public class Create_Should
    {
        [TestMethod]
        public void VisitStatus_CreateNew_ShouldAddOneInDatabase()
        {
            var options = Utils.GetOptions(nameof(VisitStatus_CreateNew_ShouldAddOneInDatabase));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.VisitStatuses.AddRange(Utils.GetVisitStatuses());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new VisitStatusService(actContext, mapper);

                var expected = actContext.VisitStatuses.Count() + 1;
                var statusDTO = new VisitStatusCreateDTO();
                statusDTO.Name = "New VisitStatus";
                var result = sut.CreateAsync(statusDTO);

                //Assert
                Assert.AreEqual(expected, actContext.VisitStatuses.Count());
            }
        }


    }
}
