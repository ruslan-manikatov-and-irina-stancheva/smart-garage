﻿using AutoMapper;
using CustomExceptions.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SmartGarage.Data;
using SmartGarage.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServicesTests.VisitStatusServiceTests
{
    [TestClass]
    public class Delete_Should
    {
        [TestMethod]
        public void ShouldDeleteVisitStatus_WhenPassedCorrectIdWithNoVisitsReferenceReference()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ShouldDeleteVisitStatus_WhenPassedCorrectIdWithNoVisitsReferenceReference));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());
            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.VisitStatuses.AddRange(Utils.GetVisitStatuses());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new VisitStatusService(actContext, mapper);
                var expected = actContext.VisitStatuses.Count() - 1;

                int statusId = 4; //no references to it
                var result = sut.DeleteAsync(statusId);
                actContext.SaveChanges();

                var given = actContext.VisitStatuses.Count();

                //Assert
                Assert.AreEqual(expected, given);
            }
        }

        [TestMethod]
        public async Task ShouldThrowArgumentNull_IfPassedIncorectId()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ShouldThrowArgumentNull_IfPassedIncorectId));
            var mapper = new Mock<IMapper>();

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.VisitStatuses.AddRange(Utils.GetVisitStatuses());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new VisitStatusService(actContext, mapper.Object);

                int makeId = -1;
                //Assert
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(async () => await sut.DeleteAsync(makeId));
            }
        }

        [TestMethod]
        public async Task ShouldThrowCanNotBeDelete_IfPassedVisitStatusWithReferenceOfVisits()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ShouldThrowCanNotBeDelete_IfPassedVisitStatusWithReferenceOfVisits));
            var mapper = new Mock<IMapper>();

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.VisitStatuses.AddRange(Utils.GetVisitStatuses());
                arrangeContext.Visits.AddRange(Utils.GetVisits());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new VisitStatusService(actContext, mapper.Object);

                int makeId = 1;
                //Assert
                await Assert.ThrowsExceptionAsync<CanNotBeDeleteException>(async () => await sut.DeleteAsync(makeId));
            }

        }
    }
}
