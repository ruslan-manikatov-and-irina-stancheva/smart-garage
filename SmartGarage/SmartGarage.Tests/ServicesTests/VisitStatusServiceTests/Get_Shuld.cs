﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SmartGarage.Data;
using SmartGarage.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServicesTests.VisitStatusServiceTests
{
    [TestClass]
    public class Get_Shuld
    {
        [TestMethod]
        public void VisitStatus_GetAllData_ShouldReturnCorrectCount()
        {
            var options = Utils.GetOptions(nameof(VisitStatus_GetAllData_ShouldReturnCorrectCount));
            var mapper = new Mock<IMapper>();
            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.VisitStatuses.AddRange(Utils.GetVisitStatuses());
                arrangeContext.SaveChanges();
            }

            //Act      
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new VisitStatusService(actContext, mapper.Object);
                var result = sut.GetAllAsync();
                var expected = actContext.VisitStatuses
                    .ToList();

                //Assert
                Assert.AreEqual(expected.Count(), result.Result.Count);
            }
        }

        [TestMethod]
        public void VisitStatus_GetById_ShouldReturnCorrectData()
        {
            var options = Utils.GetOptions(nameof(VisitStatus_GetById_ShouldReturnCorrectData));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.VisitStatuses.AddRange(Utils.GetVisitStatuses());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new VisitStatusService(actContext, mapper);
                int visitStatusId = 1;

                var result = sut.GetAsync(visitStatusId);

                var expected = actContext.VisitStatuses
                    .First(v => v.VisitStatusId == visitStatusId);
                   
                //Assert
                Assert.AreEqual(expected.Name, result.Result.Name);
            }
        }

        [TestMethod]
        public async Task PassNutExistingId_ShouldThrowArgumentNull()
        {
            var options = Utils.GetOptions(nameof(PassNutExistingId_ShouldThrowArgumentNull));
            IMapper mapper = new Mapper(Utils.GetConfigurationMapper());

            using (var arrangeContext = new SmartGarageDbContext(options))
            {
                arrangeContext.VisitStatuses.AddRange(Utils.GetVisitStatuses());
                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new SmartGarageDbContext(options))
            {
                var sut = new VisitStatusService(actContext, mapper);
                int visitStatusId = -1;

                var result = sut.GetAsync(visitStatusId);

                //Assert
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(async () => await sut.GetAsync(visitStatusId));
            }
        }

    }
}
