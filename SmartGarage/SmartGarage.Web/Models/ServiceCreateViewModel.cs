﻿using SmartGarage.Services.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SmartGarage.Web.Models
{
    public class ServiceCreateViewModel
    {
        public ServiceCreateViewModel()
        {
            this.VehicleTypes = new HashSet<VehicleTypeDTO>();
        }

        [Required]
        [StringLength(70, MinimumLength = 3, ErrorMessage = "Value for {0} must be between {2} and {1}.")]
        public string Name { get; set; }

        public ICollection<VehicleTypeDTO> VehicleTypes { get; set; }

        [Required]
        [Range(0.1, 99999.99, ErrorMessage = "Please enter a value bigger than {1}.")]
        public decimal Price { get; set; }
    }
}
