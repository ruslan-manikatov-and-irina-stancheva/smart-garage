﻿using SmartGarage.Services.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Web.Models
{
    public class CustomerGetAllVisitsViewModel
    {
        public CustomerGetAllVisitsViewModel()
        {
            this.CustomerGetVisitDTOs = new HashSet<CustomerGetVisitDTO>();
        }

        public decimal FullPrice { get; set; }
        public string Currency { get; set; }
        public ICollection<CustomerGetVisitDTO> CustomerGetVisitDTOs { get; set; }
    }
}
