﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SmartGarage.Web.Models
{
    public class VehicleCreateViewModel
    {
        public int VehicleId { get; set; }

        [Required]
        [StringLength(17, MinimumLength = 17, ErrorMessage = "Value for {0} must be exactly {1} symbols.")]
        public string Vin { get; set; }

        [Required]
        [StringLength(8, MinimumLength = 7, ErrorMessage = "Value for {0} must be between {2} and {1}.")]
        public string RegistrationPlate { get; set; }

        public int OwnerId { get; set; }

        public int ModelId { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/yyyy}")]
        public DateTime Year { get; set; }

        public int VehicleTypeId { get; set; }
    }
}