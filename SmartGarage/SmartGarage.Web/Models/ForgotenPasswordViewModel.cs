﻿using System.ComponentModel.DataAnnotations;


namespace SmartGarage.Web.Models
{
    public class ForgotenPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
