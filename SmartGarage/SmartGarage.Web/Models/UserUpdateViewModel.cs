﻿using System.ComponentModel.DataAnnotations;

namespace SmartGarage.Web.Models
{
    public class UserUpdateViewModel
    {
        [Required]
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Value for {0} must be between {2} and {1}.")]
        public string FirstName { get; set; }
        [Required]
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Value for {0} must be between {2} and {1}.")]
        public string LastName { get; set; }
        [Required]
        [RegularExpression(@"^[0-9]{10}$", ErrorMessage = "Enter only alphabets and numbers of First Name")]
        public string PhoneNumber { get; set; }
        [Required]    
        public string Email { get; set; }
        
    }
}
