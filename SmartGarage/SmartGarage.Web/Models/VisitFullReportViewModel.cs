﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SmartGarage.Web.Models
{
    public class VisitFullReportViewModel
    {
        public VisitFullReportViewModel()
        {
            this.VisitServices = new HashSet<ServiceViewModel>();
            this.Currency = "EUR";
        }

        public int VisitId { get; set; }

        public string VisitStatus { get; set; }

        public DateTime ArriveDate { get; set; }

        public DateTime DepartureDate { get; set; }

        public int OwnerId { get; set; }

        public string OwnerUsername { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string PhoneNumber { get; set; }

        public int VehicleId { get; set; }

        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime CreatedOn { get; set; }

        public string Vin { get; set; }

        public string RegistrationPlate { get; set; }

        public string Model { get; set; }

        public string Manufacturer { get; set; }

        [DisplayFormat(DataFormatString = "{0:MM/yyyy}")]
        public DateTime Year { get; set; }

        public string VehicleType { get; set; }

        public ICollection<ServiceViewModel> VisitServices { get; set; }

        public decimal TotalPrice { get; set; }

        public string Currency { get; set; }
    }
}