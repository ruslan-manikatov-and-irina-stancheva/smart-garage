﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SmartGarage.Web.Models
{
    public class VehicleDetailsViewModel
    {
        public VehicleDetailsViewModel()
        {
            this.Visits = new List<int>();
        }

        public int VehicleId { get; set; }

        public string Vin { get; set; }

        public string RegistrationPlate { get; set; }

        public int OwnerId { get; set; }

        public string Owner { get; set; }

        public int ModelId { get; set; }

        public string Model { get; set; }

        public string Manufacturer { get; set; }

        [DisplayFormat(DataFormatString = "{0:MM/yyyy}")]
        public DateTime Year { get; set; }

        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime CreatedOn { get; set; }

        public int VehicleTypeId { get; set; }

        public string VehicleType { get; set; }

        public ICollection<int> Visits { get; set; }
    }
}
