﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SmartGarage.Web.Models
{
    public class VisitViewModel
    {
        public VisitViewModel()
        {
            this.VisitServices = new HashSet<ServiceViewModel>();
        }

        public int VisitId { get; set; }

        public int VehicleId { get; set; }

        public string VisitStatus { get; set; }

        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime ArriveDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime DepartureDate { get; set; }

        public ICollection<ServiceViewModel> VisitServices { get; set; }
    }
}
