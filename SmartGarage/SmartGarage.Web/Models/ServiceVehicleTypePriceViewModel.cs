﻿namespace SmartGarage.Web.Models
{
    public class ServiceVehicleTypePriceViewModel
    {
        public string Name { get; set; }
        public string VehicleType { get; set; }
        public decimal Price { get; set; }
    }
}
