﻿namespace SmartGarage.Web.Models
{
    public class CarModelCreateViewModel
    {
        public int ModelId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public int ManufacturerId { get; set; }
    }
}
