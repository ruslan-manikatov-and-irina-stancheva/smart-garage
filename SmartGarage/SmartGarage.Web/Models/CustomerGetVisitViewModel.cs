﻿using SmartGarage.Services.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Web.Models
{
    public class CustomerGetVisitViewModel
    {

        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }

        public string VehicleModelName { get; set; }
        public string VehicleVin { get; set; }
        public string vehicleType { get; set; }

        public int VisitId { get; set; }
        public DateTime ArriveDate { get; set; }
        public string VisitStatusName { get; set; }
        public string VisitCurrencyPrice { get; set; }
        public decimal VisitCost { get; set; }

        public decimal VehicleTypeCoefficient { get; set; }

        public ICollection<ServiceDTO> ServiceDTOs { get; set; }
    }
}
