﻿using System.ComponentModel.DataAnnotations;

namespace SmartGarage.Web.Models
{
    public class VisitStatusCreateViewModel
    {
        [Required]
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Value for {0} must be between {2} and {1}.")]
        public string Name { get; set; }
    }
}
