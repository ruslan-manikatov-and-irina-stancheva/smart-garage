﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SmartGarage.Web.Models
{
    public class VisitCreateViewModel
    {
        public VisitCreateViewModel()
        {
            this.VisitServices = new HashSet<ServiceViewModel>();
        }

        public int VehicleId { get; set; }

        [Required]
        public DateTime DepartureDate { get; set; }

        public ICollection<ServiceViewModel> VisitServices { get; set; }
    }
}
