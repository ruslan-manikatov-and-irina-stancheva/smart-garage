﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SmartGarage.Web.Models
{
    public class ServiceViewModel
    {
        public ServiceViewModel()
        {
            this.VehicleTypes = new List<int>();
            this.Currency = "EUR";
        }

        public int ServiceId { get; set; }

        public string Name { get; set; }

        public ICollection<int> VehicleTypes { get; set; }

        public decimal Price { get; set; }

        public string Currency { get; set; }
    }
}
