﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SmartGarage.Web.Models
{
    public class VehicleUpdateByCustomerViewModel
    {
        [Required]
        [StringLength(17, MinimumLength = 17, ErrorMessage = "Value for {0} must be exactly {1} symbols.")]
        public string Vin { get; set; }

        [Required]
        [StringLength(8, MinimumLength = 7, ErrorMessage = "Value for {0} must be between {2} and {1}.")]
        public string RegistrationPlate { get; set; }

        public CarModelViewModel ManufacturerModel { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/yyyy}")]
        public DateTime Year { get; set; }

        public int VehicleTypeId { get; set; }
    }
}