﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Web.Models
{
    public class RoleViewModel
    {
        public int RoleId { get; set; }
        public string Name { get; set; }

    }
}
