﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SmartGarage.Web.Models
{
    public class CounterViewModel
    {
        private readonly int servicesCount;
        private readonly int customersCount;
        private readonly int vehiclesCount;
        private readonly int visitsCount;

        public int ServicesCount { get; set; }
        public int CustomersCount { get; set; }
        public int VehiclesCount { get; set; }
        public int VisitsCount { get; set; }
    }
}
