﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartGarage.Web.Models
{
    public class VehicleTypeViewModel
    {
        public int VehicleTypeId { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 3, ErrorMessage = "Value for {0} must be exactly {1} symbols.")]
        public string Name { get; set; }

        [Required]
        [Range(1.0, 99.99, ErrorMessage = "Please enter a value bigger than {1}.")]
        public decimal Coefficient { get; set; }
    }
}
