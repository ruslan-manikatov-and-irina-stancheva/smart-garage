﻿using System.ComponentModel.DataAnnotations;

namespace SmartGarage.Web.Models
{
    public class ManufacturerViewModel
    {
        public int ManufacturerId { get; set; }
        [Required]
        [StringLength(30, MinimumLength = 2, ErrorMessage = "Value for {0} must be between {2} and {1}.")]
        public string Name { get; set; }
        [Required]
        [StringLength(30, MinimumLength = 2, ErrorMessage = "Value for {0} must be between {2} and {1}.")]
        public string Code { get; set; }
    }
}
