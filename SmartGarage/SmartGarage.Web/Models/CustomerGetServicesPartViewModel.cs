﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Web.Models
{
    public class CustomerGetServicesPartViewModel
    {
        public decimal ServicePrice { get; set; }
        public int ServicesId { get; set; }
        public string ServiceName { get; set; }
    }
}
