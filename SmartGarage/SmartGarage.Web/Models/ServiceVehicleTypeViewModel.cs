﻿namespace SmartGarage.Web.Models
{
    public class ServiceVehicleTypeViewModel
    {
        public int ServiceId { get; set; }
        public string Name { get; set; }
        public string VehicleType { get; set; }
        public decimal Price { get; set; }
        public string Currency { get; set; }
    }
}
