﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Web.Models
{
    public class CustomerGetAllServicesViewModel
    {
        public int VisitId { get; set; }
        public DateTime ArriveDate { get; set; }
        public string VisitStatusName { get; set; }
        public string VehicleModelName { get; set; }
        public string VehicleVin { get; set; }
        public string VehicleRegistrationPlate { get; set; }
        public ICollection<decimal> ServicePrice { get; set; }
        public ICollection<int> ServicesId { get; set; }
        public ICollection<string> ServiceName { get; set; }
    }
}
