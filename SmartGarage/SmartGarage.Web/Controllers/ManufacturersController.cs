﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using CustomExceptions.Exceptions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTO;
using SmartGarage.Web.Models;
using X.PagedList;

namespace SmartGarage.Web.Controllers
{
    [Authorize(Roles ="employee")]
    public class ManufacturersController : Controller
    {
        private readonly IManufacturerService manufacturerService;
        private readonly IMapper mapper;

        public ManufacturersController(IManufacturerService manufacturerService, IMapper mapper)
        {
            this.manufacturerService = manufacturerService;
            this.mapper = mapper;
        }

        // GET: Manufacturers
        public async Task<IActionResult> Index(int? page)
        {
            try
            {
                var manufacturerDTO = await this.manufacturerService
                    .GetAllAsync();
                var manufacturerView = mapper.Map<ICollection<ManufacturerViewModel>>(manufacturerDTO);
                var pageModel = await manufacturerView.ToPagedListAsync(page ?? 1, 10);
                return View(pageModel);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // GET: Manufacturers/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            try
            {
                var manufacturerDTO = await this.manufacturerService
                    .GetAsync(id);
                var manufacturerView = mapper.Map<ManufacturerViewModel>(manufacturerDTO);
                return View(manufacturerView);
            }
            catch (ManufacturerNotFoundException e)
            {
                return NotFound(e.Message);
            }
            catch (Exception)
            {
                return Conflict();
            }
         
        }

        // GET: Manufacturers/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Manufacturers/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,Code")] ManufacruterCreateViewModel manufacturer)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var manufacturerDto = mapper.Map<ManufacturerCreateDTO>(manufacturer);
                    var dto = await this.manufacturerService.CreateAsync(manufacturerDto);
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception)
                {
                    return Conflict();
                }             
            }
            return View(manufacturer);
        }

        // GET: Manufacturers/Edit/5
        public async Task<IActionResult> Edit(int? id)//////////
        {
            if (id == null)
            {
                return NotFound();
            }

            var manufacturerDTO = await this.manufacturerService
                   .GetAsync(id);
            var manufacturerView = mapper.Map<ManufacruterCreateViewModel>(manufacturerDTO);

            return View(manufacturerView);
        }

        // POST: Manufacturers/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Name,Code")] ManufacruterCreateViewModel manufacturer)
        {
/*            if (id != manufacturer.ManufacturerId)
            {
                return NotFound();
            }*/

            if (ModelState.IsValid)
            {
                try
                {
                    var manufacturerDTO = mapper.Map<ManufacturerCreateDTO>(manufacturer);
                    var modelDTO = await manufacturerService.UpdateAsync(id, manufacturerDTO);
                    return RedirectToAction(nameof(Index));
                }
                catch (ManufacturerNotFoundException e)
                {
                    return BadRequest(e.Message);
                }
                catch (Exception)
                {
                    return Conflict();
                }
                
            }
            return View(manufacturer);
        }

        // GET: Manufacturers/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            try
            {
                var manufacturerDTO = await this.manufacturerService
                        .GetAsync(id);
                var manufacturerView = mapper.Map<ManufacturerViewModel>(manufacturerDTO);
                return View(manufacturerView);
            }
            catch(Exception)
            {
                return BadRequest();
            }
            
        }

        // POST: Manufacturers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                await this.manufacturerService.DeleteAsync(id);
                return RedirectToAction(nameof(Index));
            }
            catch (ManufacturerNotFoundException e)
            {
                return BadRequest(e.Message);
            }
            catch (ArgumentException e)
            {
                return Conflict(e.Message);
            }
            
        }
    }
}
