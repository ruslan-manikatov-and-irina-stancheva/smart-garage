﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SmartGarage.Data;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTO;
using SmartGarage.Web.Models;

namespace SmartGarage.Web.Controllers
{
    [Authorize(Roles = "employee")]
    public class UsersController : Controller
    {
        private readonly SmartGarageDbContext _context;
        private readonly IUserService userService;
        private readonly IMapper mapper;
        private readonly IRoleService roleService;

        public UsersController(SmartGarageDbContext context, IMapper mapper, IUserService userService, IRoleService roleService)
        {
            _context = context;
            this.mapper = mapper;
            this.userService = userService;
            this.roleService = roleService;
        }

        // GET: Users
        
        public async Task<IActionResult> Index(string searchBy, string filter, DateTime? start, DateTime? end, string sortBy)
        {
            var userViewModel = await SelectFilterAsync(searchBy, filter, start, end);

            switch (sortBy)
            {
                case "Name desc":
                    userViewModel = userViewModel.OrderBy(v => v.FirstName).ToList();
                    break;

                case "VisitDate desc":
                    var usersFiltered = await this.userService
                              .SortUsersByVisitDate();
                    var userMapper = mapper.Map<ICollection<UserGetViewModel>>(usersFiltered);
                    return View(userMapper);
                    
                default:
                    break;
            }
            return View(userViewModel);
        }

        private async Task<ICollection<UserGetViewModel>> SelectFilterAsync(string searchBy, string filter, DateTime? start, DateTime? end)
        {
            if (searchBy == "Role" && filter != null)
            {
                var usersFiltered = await this.userService
                              .FilterUsersByRole(filter);
                var userMapper = mapper.Map<ICollection<UserGetViewModel>>(usersFiltered);
                return userMapper;
            }
            else if (searchBy == "LastName")
            {
                var usersFiltered = await this.userService
                              .FilterUsersByLastName(filter);
                var userMapper = mapper.Map<ICollection<UserGetViewModel>>(usersFiltered);
                return userMapper;
            }
            else if(searchBy == "Manufacturer")
            {
                var usersFiltered = await this.userService
                              .FilterUsersByVehicleMake(filter);
                var userMapper = mapper.Map<ICollection<UserGetViewModel>>(usersFiltered);
                return userMapper;
            }
            else if (searchBy == "Email")
            {
                var usersFiltered = await this.userService
                              .FilterUsersByEmail(filter);
                var userMapper = mapper.Map<ICollection<UserGetViewModel>>(usersFiltered);
                return userMapper;
            }
            else if (searchBy == "Phone")
            {
                var usersFiltered = await this.userService
                              .FilterUsersByPhone(filter);
                var userMapper = mapper.Map<ICollection<UserGetViewModel>>(usersFiltered);
                return userMapper;
            }
            else if (searchBy == "Model")
            {
                var usersFiltered = await this.userService
                              .FilterUsersByVehicleModel(filter);
                var userMapper = mapper.Map<ICollection<UserGetViewModel>>(usersFiltered);
                return userMapper;
            }
            else if (start != null || end != null)
            {
                var usersFiltered = await this.userService
                              .FilterUsersByVisitInRange(start, end);
                var userMapper = mapper.Map<ICollection<UserGetViewModel>>(usersFiltered);
                return userMapper;
            }

            var userDto = await this.userService
              .GetAllAsync();
            var userViewModel = mapper.Map<ICollection<UserGetViewModel>>(userDto);
            return userViewModel;

        }

        // GET: Users/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var userDto = await this.userService
                    .GetAsync(id);
            var userView = mapper.Map<UserGetViewModel>(userDto);

            if (userView == null)
            {
                return NotFound();
            }

            return View(userView);
        }

        // GET: Users/Create
        public async Task<IActionResult> Create()
        {
            var roleDto = await this.roleService
              .GetAllAsync();
            var roleViewModel = mapper.Map<ICollection<RoleViewModel>>(roleDto);

            ViewData["RoleId"] = new SelectList(roleViewModel.ToList(), "RoleId", "Name");
            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("FirstName,LastName,PhoneNumber,Email,RoleId")] UserCreateModelView userCreateViewModel)
        {
            if (ModelState.IsValid)
            {
                var carModelDTO = mapper.Map<UserCreateDTO>(userCreateViewModel);
                var dto = await this.userService.CreateAsync(carModelDTO);
                return RedirectToAction(nameof(Index));
            }
            return View(userCreateViewModel);
        }

        // GET: Users/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var userDto = await this.userService
                    .GetAsync(id);
            var userView = mapper.Map<UserUpdateViewModel>(userDto);

            if (userView == null)
            {
                return NotFound();
            }
            return View(userView);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("FirstName,LastName,PhoneNumber,Email")] UserUpdateViewModel userViewModel)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    var useDTO = mapper.Map<UserDTO>(userViewModel);
                    var dto = await userService.UpdateAsync(id, useDTO);

                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }
                return RedirectToAction(nameof(Index));
            }
            return View(userViewModel);
        }

        // GET: Users/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var userDto = await this.userService
                    .GetAsync(id);
            var userView = mapper.Map<UserGetViewModel>(userDto);
            if (userView == null)
            {
                return NotFound();
            }

            return View(userView);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await this.userService.DeleteAsync(id);
            return RedirectToAction(nameof(Index));
        }
    }
}
