﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using CustomExceptions.Exceptions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTO;
using SmartGarage.Web.Models;
using X.PagedList;

namespace SmartGarage.Web.Controllers
{
    public class ServicesController : Controller
    {
        private readonly IMapper mapper;
        private readonly IServiceService serviceService;
        private readonly IVehicleTypeService vehicleTypeService;

        public ServicesController(IMapper mapper, IServiceService serviceService, IVehicleTypeService vehicleTypeService)
        {
            this.mapper = mapper;
            this.serviceService = serviceService;
            this.vehicleTypeService = vehicleTypeService;
        }

        public async Task<IActionResult> Index(int? page, string name, decimal price)
        {
            var serviceViewModel = await FilterAsync(name, price, page);

            if (serviceViewModel == null)
                return NoContent(); 
            else
                return View(serviceViewModel);
        }

        public async Task<IActionResult> Details(int id, int? page)
        {
            var itemsDto = await this.serviceService.GetServiceAndTypeAsync(id);

            if (itemsDto.Count == 0)
            {
                return NotFound();
            }

            var itemsView = mapper.Map<ICollection<ServiceVehicleTypeViewModel>>(itemsDto);
            var pageModel = await itemsView.ToPagedListAsync(page ?? 1, 10);

            return View(pageModel);
        }

        private async Task<IPagedList<ServiceViewModel>> FilterAsync(string name, decimal price, int? page)
        {
            if (name != null && price > 0)
            {
                var servicesFiltered = await GetByNameAndPriceAsync(name, price, page);

                return servicesFiltered;
            }
            else if (name == null && price > 0)
            {
                var servicesFiltered = await GetByPriceAsync(price, page);

                return servicesFiltered;
            }
            else if (name != null && price == 0)
            {
                var servicesFiltered = await GetByNameAsync(name, page);

                return servicesFiltered;
            }

            var servicesDto = await this.serviceService.GetAllAsync();

            var servicesView = mapper.Map<ICollection<ServiceViewModel>>(servicesDto);
            var pageModel = await servicesView.ToPagedListAsync(page ?? 1, 10);

            return pageModel;
        }

        private async Task<IPagedList<ServiceViewModel>> GetByNameAndPriceAsync(string name, decimal price, int? page)
        {
            var itemsDto = await this.serviceService.GetByNameAndPriceAsync(name, price);

            if (itemsDto.Count == 0)
                return null;

            var itemsView = mapper.Map<ICollection<ServiceViewModel>>(itemsDto);
            var pageModel = await itemsView.ToPagedListAsync(page ?? 1, 10);

            return pageModel;
        }

        private async Task<IPagedList<ServiceViewModel>> GetByNameAsync(string name, int? page)
        {
            var itemsDto = await this.serviceService.GetByNameAsync(name);

            if (itemsDto.Count == 0)
                return null;

            var itemsView = mapper.Map<ICollection<ServiceViewModel>>(itemsDto);
            var pageModel = await itemsView.ToPagedListAsync(page ?? 1, 10);

            return pageModel;
        }

        private async Task<IPagedList<ServiceViewModel>> GetByPriceAsync(decimal price, int? page)
        {
            var itemsDto = await this.serviceService.GetByPriceAsync(price);

            if (itemsDto.Count == 0)
                return null;

            var itemsView = mapper.Map<ICollection<ServiceViewModel>>(itemsDto);
            var pageModel = await itemsView.ToPagedListAsync(page ?? 1, 10);

            return pageModel;
        }

        [Authorize(Roles = "employee")]
        public async Task<IActionResult> Create()
        {
            var serviceView = new ServiceCreateViewModel();
            serviceView.VehicleTypes = await this.vehicleTypeService.GetAllAsync();

            return View(serviceView);
        }

        [HttpPost]
        [Authorize(Roles = "employee")]
        public async Task<IActionResult> Create(ServiceCreateViewModel service, List<int> types)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var serviceDto = mapper.Map<ServiceCreateDTO>(service);

                    var newService = await this.serviceService.CreateAsync(serviceDto, types);

                    return RedirectToAction(nameof(Index));
                }
                catch
                {
                    return NotFound();
                }
            }

            return View(service);
        }

        [Authorize(Roles = "employee")]
        public async Task<IActionResult> Edit(int id)
        {
            var service = await this.serviceService.GetAsync(id);

            if (service == null)
            {
                return NotFound();
            }

            var serviceView = mapper.Map<ServiceUpdateViewModel>(service);
            serviceView.VehicleTypes = await this.vehicleTypeService.GetAllAsync(); 

            return View(serviceView);
        }

        [HttpPost]
        [Authorize(Roles = "employee")]
        public async Task<IActionResult> Edit(int id, ServiceUpdateViewModel service, List<int> types)
        {
            if (id != service.ServiceId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var serviceDto = mapper.Map<ServiceUpdateDTO>(service);

                    await this.serviceService.UpdateAsync(id, serviceDto, types);
                }
                catch
                {
                    return NotFound();
                }

                return RedirectToAction(nameof(Index));
            }

            return View(service);
        }

        [Authorize(Roles = "employee")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var service = await this.serviceService.GetAsync(id);

                var serviceView = mapper.Map<ServiceUpdateViewModel>(service);

                return View(serviceView);
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
            catch
            {
                return Conflict();
            }
        }

        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "employee")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                await this.serviceService.DeleteAsync(id);
            }
            catch (Exception)
            {
                return NotFound();
            }

            return RedirectToAction(nameof(Index));
        }
    }
}
