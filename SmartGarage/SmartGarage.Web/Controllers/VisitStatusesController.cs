﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SmartGarage.Data;
using SmartGarage.Data.Models;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTO;
using SmartGarage.Web.Models;
using X.PagedList;

namespace SmartGarage.Web.Controllers
{
    [Authorize(Roles = "employee")]
    public class VisitStatusesController : Controller
    {
        private readonly IMapper mapper;
        private readonly IVisitStatusService visitStatusService;
        public VisitStatusesController(IMapper mapper, IVisitStatusService visitStatusService)
        {
            this.mapper = mapper;
            this.visitStatusService = visitStatusService;
        }

        public async Task<IActionResult> Index(int? page)
        {
            var itemsDto = await this.visitStatusService.GetAllAsync();

            if (itemsDto.Count == 0)
            {
                return NoContent();
            }

            var itemsView = mapper.Map<ICollection<VisitStatusViewModel>>(itemsDto);
            var pageModel = await itemsView.ToPagedListAsync(page ?? 1, 10);

            return View(pageModel);
        }

        public async Task<IActionResult> Details(int id)
        {
            try
            {
                var itemDto = await this.visitStatusService.GetAsync(id);

                var itemView = mapper.Map<VisitStatusViewModel>(itemDto);

                return View(itemView);
            }
            catch
            {
                return NotFound();
            }
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name")] VisitStatusCreateViewModel visitStatusCreateViewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var visitStatusDto = mapper.Map<VisitStatusCreateDTO>(visitStatusCreateViewModel);

                    var newVisitStatus = await this.visitStatusService.CreateAsync(visitStatusDto);

                    return RedirectToAction(nameof(Index));
                }
                catch
                {
                    return NotFound();
                }
            }

            return View(visitStatusCreateViewModel);
        }

        public async Task<IActionResult> Edit(int id)
        {
            try
            {
                var visitStatus = await this.visitStatusService.GetAsync(id);

                var visitStatusView = mapper.Map<VisitStatusViewModel>(visitStatus);

                return View(visitStatusView);
            }
            catch
            {
                return NotFound();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("VisitStatusId,Name")] VisitStatusViewModel visitStatusViewModel)
        {
            if (id != visitStatusViewModel.VisitStatusId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var visitStatusDto = mapper.Map<VisitStatusDTO>(visitStatusViewModel);

                    await this.visitStatusService.UpdateAsync(id, visitStatusDto);

                    return RedirectToAction(nameof(Index));
                }
                catch
                {
                    return NotFound();
                }
            }

            return View(visitStatusViewModel);
        }

        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var visitStatus = await this.visitStatusService.GetAsync(id);

                var visitStatusView = mapper.Map<VisitStatusViewModel>(visitStatus);

                return View(visitStatusView); 
            }
            catch
            {
                return NotFound();
            }
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                await this.visitStatusService.DeleteAsync(id);
            }
            catch
            {
                return NotFound();
            }

            return RedirectToAction(nameof(Index));
        }
    }
}
