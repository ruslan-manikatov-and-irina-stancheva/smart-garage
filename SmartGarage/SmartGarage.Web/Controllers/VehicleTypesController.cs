﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTO;
using SmartGarage.Web.Models;
using X.PagedList;

namespace SmartGarage.Web.Controllers
{
    [Authorize(Roles = "employee")]
    public class VehicleTypesController : Controller
    {
        private readonly IMapper mapper;
        private readonly IVehicleTypeService vehicleTypeService;

        public VehicleTypesController(IMapper mapper, IVehicleTypeService vehicleTypeService)
        {
            this.mapper = mapper;
            this.vehicleTypeService = vehicleTypeService;
        }

        public async Task<IActionResult> Index(int? page)
        {
            var itemsDto = await this.vehicleTypeService.GetAllAsync();

            if (itemsDto.Count == 0)
            {
                return NoContent();
            }

            var itemsView = mapper.Map<ICollection<VehicleTypeViewModel>>(itemsDto);
            var pageModel = await itemsView.ToPagedListAsync(page ?? 1, 10);

            return View(pageModel);
        }

        public async Task<IActionResult> Details(int id)
        {
            try
            {
                var itemDto = await this.vehicleTypeService.GetAsync(id);

                var itemView = mapper.Map<VehicleTypeViewModel>(itemDto);

                return View(itemView);
            }
            catch
            {
                return NotFound();
            }
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,Coefficient")] VehicleTypeCreateViewModel vehicleTypeCreateViewModel) 
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var vehicleTypeDto = mapper.Map<VehicleTypeCreateDTO>(vehicleTypeCreateViewModel);

                    var newVehicleTypeDto = await this.vehicleTypeService.CreateAsync(vehicleTypeDto);

                    return RedirectToAction(nameof(Index));
                }
                catch
                {
                    return NotFound();
                }
            }

            return View(vehicleTypeCreateViewModel);
        }

        // GET: VehicleTypes/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            try
            {
                var vehicleType = await this.vehicleTypeService.GetAsync(id);

                var vehicleTypeView = mapper.Map<VehicleTypeViewModel>(vehicleType);

                return View(vehicleTypeView);
            }
            catch
            {
                return NotFound();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("VehicleTypeId,Name,Coefficient")] VehicleTypeViewModel vehicleTypeViewModel)
        {
            if (id != vehicleTypeViewModel.VehicleTypeId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var vehicleTypeDto = mapper.Map<VehicleTypeDTO>(vehicleTypeViewModel);

                    await this.vehicleTypeService.UpdateAsync(id, vehicleTypeDto);
                }
                catch
                {
                    return NotFound();
                }

                return RedirectToAction(nameof(Index));
            }

            return View(vehicleTypeViewModel);
        }

        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var vehicleType = await this.vehicleTypeService.GetAsync(id);

                var vehicleTypeView = mapper.Map<VehicleTypeViewModel>(vehicleType);

                return View(vehicleTypeView);
            }
            catch
            {
                return NotFound();
            }
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                await this.vehicleTypeService.DeleteAsync(id);
            }
            catch
            {
                return NotFound();
            }

            return RedirectToAction(nameof(Index));
        }
    }
}
