﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SmartGarage.Services.Contracts;
using SmartGarage.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;


namespace SmartGarage.Web.Controllers
{
    [Authorize()]
    public class CustomersController : Controller
    {
        private readonly ICustomerServices customerServices;
        private readonly IPdfGeneratorService pdfGeneratorService;
        private readonly IMapper mapper; 

        public CustomersController(ICustomerServices customerServices, IMapper mapper, IPdfGeneratorService pdfGeneratorService)
        {
            this.customerServices = customerServices;
            this.mapper = mapper;
            this.pdfGeneratorService = pdfGeneratorService;
        }

        public async Task<IActionResult> Vehicles()
        {
            var username = User.Identity.Name; 
            var vehiclesDto = await this.customerServices.GetAllVehicles(username);
            var manufacturerView = mapper.Map<ICollection<CustomerVehiclesViewModel>>(vehiclesDto);


            return View(manufacturerView);
        }

        public async Task<IActionResult> Services(int? page, string currentFilter, string searchString, DateTime? filterDate)
        {
            var username = User.Identity.Name;

            if (searchString != null || filterDate != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.currentFilter = searchString;

            try
            {

                if(searchString == null && filterDate != null)
                {
                    var customerDTO = await this.customerServices.FilterServicesByDate(username, filterDate);
                    var viewModel = mapper.Map<ICollection<CustomerGetAllServicesViewModel>>(customerDTO);
                    var filterByRegPlate = viewModel.Where(s => s.ArriveDate == filterDate);
                    var filterModel = await filterByRegPlate.ToPagedListAsync(page ?? 1, 2);
                    return View(filterModel);
                }

                else if (searchString != null && filterDate == null)
                {
                    var customerDTO = await this.customerServices.FilterServicesByRegPlate(username, searchString);
                    var viewModel = mapper.Map<ICollection<CustomerGetAllServicesViewModel>>(customerDTO);
                    var filterByRegPlate = viewModel.Where(s => s.VehicleRegistrationPlate == searchString);
                    var filterModel = await filterByRegPlate.ToPagedListAsync(page ?? 1, 2);
                    return View(filterModel);
                }
                else if (searchString != null && filterDate != null)
                {
                    var customerDTO = await this.customerServices.FilterServicesByRegPlateAndDate(username, searchString, filterDate);
                    var viewModel = mapper.Map<ICollection<CustomerGetAllServicesViewModel>>(customerDTO);
                    var filterByRegPlate = viewModel.Where(s => s.VehicleRegistrationPlate == searchString);
                    var filterModel = await filterByRegPlate.ToPagedListAsync(page ?? 1, 2);
                    return View(filterModel);
                }

                var customerGetAllServicesDTO = await this.customerServices.GetAllServices(username);
                var customerView = mapper.Map<ICollection<CustomerGetAllServicesViewModel>>(customerGetAllServicesDTO);
                var pageModel = await customerView.ToPagedListAsync(page ?? 1, 2);
                return View(pageModel);
            }
            catch (Exception)
            {
                return BadRequest();
            }   
        }

        public async Task<IActionResult> Visits(string currency, bool generatePdf)
        {
            try
            {
                var username = User.Identity.Name;
                var customerGetAllVisitsDTO = await this.customerServices.GetAllVisitsReport(username, currency);
                var customerView = mapper.Map<CustomerGetAllVisitsViewModel>(customerGetAllVisitsDTO);
                if(generatePdf == true)
                {
                    return (this.pdfGeneratorService.GeneratePdfForAllVisitsOfCustomer(customerGetAllVisitsDTO));
                }
                return View(customerView);
            }
            catch(Exception)
            {
                return BadRequest();
            }
        }

        public async Task<IActionResult> VisitDetails(int? id, string currency, bool generatePdf)
        {
            var username = User.Identity.Name;
            //try
            //{
                if (currency != null)
                {
                    var GetAllVisitsDTO = await this.customerServices.GetVisitReportByIdAndCurrency(username, id, currency);
                    
                    if(generatePdf == true)
                    {
                        return (this.pdfGeneratorService.GeneratePdfForOneVisit(GetAllVisitsDTO));
                    }
                    var viewModel = mapper.Map<CustomerGetVisitViewModel>(GetAllVisitsDTO);
                    return View(viewModel);
                }

                var customerGetAllVisitsDTO = await this.customerServices.GetVisitReportById(username, id);
                if(generatePdf == true)
                {
                    return (this.pdfGeneratorService.GeneratePdfForOneVisit(customerGetAllVisitsDTO));
                }
                var customerView = mapper.Map<CustomerGetVisitViewModel>(customerGetAllVisitsDTO);
                return View(customerView);
            //}
            //catch(Exception)
            //{
            //    return BadRequest();
            //}
        }


        public IActionResult CreateDocument()
        {
            var username = User.Identity.Name;
    

            var customerGetAllVisitsDTO = this.customerServices.GetAllVisitsReport(username, "EUR");

            return (this.pdfGeneratorService.GeneratePdfForAllVisitsOfCustomer(customerGetAllVisitsDTO.Result));
        }

    }
}
