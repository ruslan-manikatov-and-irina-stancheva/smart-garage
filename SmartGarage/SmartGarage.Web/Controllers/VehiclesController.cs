﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTO;
using SmartGarage.Web.Models;
using X.PagedList;

namespace SmartGarage.Web.Controllers
{
    [Authorize(Roles = "employee")]
    public class VehiclesController : Controller
    {
        private readonly IMapper mapper;
        private readonly IVehicleService vehicleService;
        private readonly IModelService modelService;
        private readonly IUserService userService;
        private readonly IVehicleTypeService vehicleTypeService;

        public VehiclesController(IMapper mapper, IVehicleService vehicleService, IModelService modelService, IUserService userService, IVehicleTypeService vehicleTypeService)
        {
            this.mapper = mapper;
            this.vehicleService = vehicleService;
            this.modelService = modelService;
            this.userService = userService;
            this.vehicleTypeService = vehicleTypeService;
        }

        public async Task<IActionResult> Index(int? page, string name)
        {
            var vehiclesViewModel = await FilterAsync(name, page);

            if (vehiclesViewModel == null)
                return NoContent(); 
            else
                return View(vehiclesViewModel);
        }

        private async Task<IPagedList<VehicleViewModel>> FilterAsync(string name, int? page)
        {
            if (name != null)
            {
                var vehiclesFiltered = await GetByCustomer(name, page);

                return vehiclesFiltered;
            }

            var vehiclesDto = await this.vehicleService.GetAllAsync();

            var vehiclesView = mapper.Map<ICollection<VehicleViewModel>>(vehiclesDto);
            var pageModel = await vehiclesView.ToPagedListAsync(page ?? 1, 10);

            return pageModel;
        }

        private async Task<IPagedList<VehicleViewModel>> GetByCustomer(string name, int? page)
        {
            var vehiclesDto = await this.vehicleService.GetByCustomer(name).ToListAsync();

            if (vehiclesDto.Count == 0)
                return null;

            var vehiclesView = mapper.Map<ICollection<VehicleViewModel>>(vehiclesDto);
            var pageModel = await vehiclesView.ToPagedListAsync(page ?? 1, 10);

            return pageModel;
        }

        public async Task<IActionResult> Details(int id)
        {
            try
            {
                var itemDto = await this.vehicleService.GetAsync(id);

                var itemView = mapper.Map<VehicleDetailsViewModel>(itemDto);

                return View(itemView);
            }
            catch
            {
                return NotFound();
            }
        }

        public async Task<IActionResult> Create()
        {
            var carModelDto = await this.modelService.GetAllAsync();
            var carModelView = mapper.Map<ICollection<CarModelViewModel>>(carModelDto);

            var ownerDto = await this.userService.GetAllAsync();
            var ownerView = mapper.Map<ICollection<UserGetViewModel>>(ownerDto);

            var vehicleTypeDto = await this.vehicleTypeService.GetAllAsync();
            var vehicleTypeView = mapper.Map<ICollection<VehicleTypeViewModel>>(vehicleTypeDto);

            ViewData["ModelId"] = new SelectList(carModelView, "ModelId", "Code");
            ViewData["OwnerId"] = new SelectList(ownerView, "Id", "Email");
            ViewData["VehicleTypeId"] = new SelectList(vehicleTypeView, "VehicleTypeId", "Name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken] 
        public async Task<IActionResult> Create([Bind("Vin,RegistrationPlate,OwnerId,ModelId,Year,VehicleTypeId")] VehicleCreateViewModel vehicleCreateViewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var vehicleDto = mapper.Map<VehicleCreateDTO>(vehicleCreateViewModel);

                    var newVisitStatus = await this.vehicleService.CreateAsync(vehicleDto);

                    return RedirectToAction(nameof(Index));
                }
                catch
                {
                    return NotFound();
                }
            }

            var carModelDto = await this.modelService.GetAllAsync();
            var carModelView = mapper.Map<ICollection<CarModelViewModel>>(carModelDto);

            var ownerDto = await this.userService.GetAllAsync();
            var ownerView = mapper.Map<ICollection<UserGetViewModel>>(ownerDto);

            var vehicleTypeDto = await this.vehicleTypeService.GetAllAsync();
            var vehicleTypeView = mapper.Map<ICollection<VehicleTypeViewModel>>(vehicleTypeDto);

            ViewData["ModelId"] = new SelectList(carModelView, "ModelId", "Code", vehicleCreateViewModel.ModelId);
            ViewData["OwnerId"] = new SelectList(ownerView, "Id", "Username", vehicleCreateViewModel.OwnerId);
            ViewData["VehicleTypeId"] = new SelectList(vehicleTypeView, "VehicleTypeId", "Name", vehicleCreateViewModel.VehicleTypeId);

            return View(vehicleCreateViewModel);
        }

        public async Task<IActionResult> Edit(int id)
        {
            try
            {
                var vehicleDto = await this.vehicleService.GetAsync(id);
                var vehicleView = mapper.Map<VehicleCreateViewModel>(vehicleDto);

                var carModelDto = await this.modelService.GetAllAsync();
                var carModelView = mapper.Map<ICollection<CarModelViewModel>>(carModelDto);

                var ownerDto = await this.userService.GetAllAsync();
                var ownerView = mapper.Map<ICollection<UserGetViewModel>>(ownerDto);

                var vehicleTypeDto = await this.vehicleTypeService.GetAllAsync();
                var vehicleTypeView = mapper.Map<ICollection<VehicleTypeViewModel>>(vehicleTypeDto);

                ViewData["ModelId"] = new SelectList(carModelView, "ModelId", "Code", vehicleView.ModelId);
                ViewData["OwnerId"] = new SelectList(ownerView, "Id", "Email", vehicleView.OwnerId);
                ViewData["VehicleTypeId"] = new SelectList(vehicleTypeView, "VehicleTypeId", "Name", vehicleView.VehicleTypeId);

                return View(vehicleView);
            }
            catch
            {
                return NotFound();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("VehicleId,Vin,RegistrationPlate,OwnerId,ModelId,Year,VehicleTypeId")] VehicleCreateViewModel vehicleCreateViewModel)
        {
            if (id != vehicleCreateViewModel.VehicleId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var vehicleDto = mapper.Map<VehicleCreateDTO>(vehicleCreateViewModel);

                    await this.vehicleService.UpdateAsync(id, vehicleDto);
                }
                catch
                {
                    return NotFound();
                }

                return RedirectToAction(nameof(Index));
            }

            var carModelDto = await this.modelService.GetAllAsync();
            var carModelView = mapper.Map<ICollection<CarModelViewModel>>(carModelDto);

            var ownerDto = await this.userService.GetAllAsync();
            var ownerView = mapper.Map<ICollection<UserGetViewModel>>(ownerDto);

            var vehicleTypeDto = await this.vehicleTypeService.GetAllAsync();
            var vehicleTypeView = mapper.Map<ICollection<VehicleTypeViewModel>>(vehicleTypeDto);

            ViewData["ModelId"] = new SelectList(carModelView, "ModelId", "Code", vehicleCreateViewModel.ModelId);
            ViewData["OwnerId"] = new SelectList(ownerView, "Id", "Email", vehicleCreateViewModel.OwnerId);
            ViewData["VehicleTypeId"] = new SelectList(vehicleTypeView, "VehicleTypeId", "Name", vehicleCreateViewModel.VehicleTypeId);

            return View(vehicleCreateViewModel);
        }

        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var vehicleDto = await this.vehicleService.GetAsync(id);

                var vehicleView = mapper.Map<VehicleViewModel>(vehicleDto);

                return View(vehicleView);
            }
            catch(ArgumentNullException)
            {
                return NotFound();
            }
            catch
            {
                return Conflict();
            }
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                await this.vehicleService.DeleteAsync(id);
            }
            catch
            {
                return NotFound();
            }

            return RedirectToAction(nameof(Index));
        }
    }
}
