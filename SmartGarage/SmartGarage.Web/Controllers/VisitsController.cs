﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTO;
using SmartGarage.Web.Models;
using X.PagedList;

namespace SmartGarage.Web.Controllers
{
    [Authorize(Roles = "employee")]
    public class VisitsController : Controller
    {
        private readonly IMapper mapper;
        private readonly IVisitService visitService;
        private readonly IServiceService serviceService;
        private readonly IVehicleService vehicleService;
        private readonly IVisitStatusService visitStatusService; 

        public VisitsController(IMapper mapper, IVisitService visitService, IServiceService serviceService, IVehicleService vehicleService, IVisitStatusService visitStatusService)
        {
            this.mapper = mapper;
            this.visitService = visitService;
            this.serviceService = serviceService;
            this.vehicleService = vehicleService;
            this.visitStatusService = visitStatusService;
        }

        public async Task<IActionResult> Index(int? page)
        {
            var itemsDto = await this.visitService.GetAllAsync();

            if (itemsDto.Count == 0)
            {
                return NoContent();
            }

            var itemsView = mapper.Map<ICollection<VisitViewModel>>(itemsDto);
            var pageModel = await itemsView.ToPagedListAsync(page ?? 1, 10);

            return View(pageModel);
        }

        public async Task<IActionResult> Details(int id)
        {
            try
            {
                var itemDto = await this.visitService.GetAsync(id);

                var itemView = mapper.Map<VisitFullReportViewModel>(itemDto);

                return View(itemView);
            }
            catch
            {
                return NotFound();
            }
        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            var visitView = new VisitCreateViewModel();

            var servicesDto = await this.serviceService.GetAllAsync();
            var servicesView = mapper.Map<ICollection<ServiceViewModel>>(servicesDto);

            var vehiclesDto = await this.vehicleService.GetAllAsync();
            var vehiclesView = mapper.Map<ICollection<VehicleViewModel>>(vehiclesDto);

            //ViewData["ServiceId"] = new SelectList(servicesView, "ServiceId", "Name");
            ViewData["VehiclesId"] = new SelectList(vehiclesView, "VehicleId", "RegistrationPlate");

            visitView.DepartureDate = DateTime.Now;
            visitView.VisitServices = servicesView;

            return View(visitView);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(VisitCreateViewModel visitCreateViewModel,  List<int> services)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var visitDto = mapper.Map<VisitCreateDTO>(visitCreateViewModel);

                    var newVisit = await this.visitService.CreateAsync(visitDto, services);

                    return RedirectToAction(nameof(Index));
                }
                catch
                {
                    return NotFound();
                }
            }

            return View(visitCreateViewModel);
        }

        public async Task<IActionResult> Edit(int id)
        {
            try
            {
                var visitDto = await this.visitService.GetAsync(id);
                var visitView = mapper.Map<VisitUpdateViewModel>(visitDto);

                var vehiclesDto = await this.vehicleService.GetAllAsync();
                var vehiclesView = mapper.Map<ICollection<VehicleViewModel>>(vehiclesDto);

                var visitStatusesDto = await this.visitStatusService.GetAllAsync();
                var visitStatusesView = mapper.Map<ICollection<VisitStatusViewModel>>(visitStatusesDto);

                var servicesDto = await this.serviceService.GetAllAsync();
                var servicesView = mapper.Map<ICollection<ServiceViewModel>>(servicesDto);

                ViewData["VehicleId"] = new SelectList(vehiclesView, "VehicleId", "RegistrationPlate", visitView.VehicleId);
                ViewData["VisitStatusId"] = new SelectList(visitStatusesView, "VisitStatusId", "Name", visitView.VisitStatusId);
                visitView.VisitServices = servicesView;

                return View(visitView);
            }
            catch
            {
                return NotFound();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, VisitUpdateViewModel visitUpdateViewModel, List<int> services)
        {
            if (id != visitUpdateViewModel.VisitId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var visitDto = mapper.Map<VisitUpdateDTO>(visitUpdateViewModel);

                    await this.visitService.UpdateAsync(id, visitDto, services);
                }
                catch
                {
                    return NotFound();
                }

                return RedirectToAction(nameof(Index));
            }

            return View(visitUpdateViewModel);
        }

        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var visitDto = await this.visitService.GetAsync(id);

                var visitView = mapper.Map<VisitFullReportViewModel>(visitDto);

                return View(visitView);
            }
            catch (ArgumentNullException)
            {
                return NotFound();
            }
            catch
            {
                return Conflict();
            }
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                await this.visitService.DeleteAsync(id);
            }
            catch
            {
                return NotFound();
            }

            return RedirectToAction(nameof(Index));
        }
    }
}
