﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SmartGarage.Data;
using SmartGarage.Data.Models;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTO;
using SmartGarage.Web.Models;
using X.PagedList;

namespace SmartGarage.Web.Controllers
{
    [Authorize(Roles = "employee")]
    public class ModelsController : Controller
    {

        private readonly IModelService modelService;
        private readonly IManufacturerService manufacturerService;
        private readonly IMapper mapper;

        public ModelsController(IModelService modelService, IMapper mapper, IManufacturerService manufacturerService)
        {
            this.modelService = modelService;
            this.mapper = mapper;
            this.manufacturerService = manufacturerService;
        }

        // GET: Models
        public async Task<IActionResult> Index(int? page)
        {
            try
            {
                var carModelDTO = await this.modelService
                    .GetAllAsync();
                var carModelView = mapper.Map<ICollection<CarModelViewModel>>(carModelDTO);
                var pageModel = await carModelView.ToPagedListAsync(page ?? 1, 10);

                return View(pageModel);
            }
            catch (Exception)
            {
                return BadRequest();
            }
            
        }

        // GET: Models/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || id <= 0)
            {
                return NotFound();
            }
            try
            {
                var carModelDto = await this.modelService
                         .GetAsync(id);
                var carModelView = mapper.Map<CarModelViewModel>(carModelDto);
            return View(carModelView);
            }
            catch(Exception)
            {
                return BadRequest();
            }
        }

        // GET: Models/Create
        public async Task<IActionResult> Create()
        {
            var manufacturerDTO = await this.manufacturerService
            .GetAllAsync();
            var manufacturerView = mapper.Map<ICollection<ManufacturerViewModel>>(manufacturerDTO);

            ViewData["ManufacturerId"] = new SelectList(manufacturerView, "ManufacturerId", "Name");
            return View();
        }

        // POST: Models/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,Code,ManufacturerId")] CarModelCreateViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var carModelDTO = mapper.Map<CarModelCreateDTO>(viewModel);
                var dto = await this.modelService.CreateAsync(carModelDTO);
                return RedirectToAction(nameof(Index));
            }
            var manufacturerDTO = await this.manufacturerService
            .GetAllAsync();
            var manufacturerView = mapper.Map<ICollection<ManufacturerViewModel>>(manufacturerDTO);

            ViewData["ManufacturerId"] = new SelectList(manufacturerView, "ManufacturerId", "Name", viewModel.ManufacturerId);
            return View(viewModel);
        }

        // GET: Models/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var carModelDto = await this.modelService
                    .GetAsync(id);
                var carModelView = mapper.Map<CarModelCreateViewModel>(carModelDto);

 
            if (carModelView == null)
            {
                return NotFound();
            }

            var manufacturerDTO = await this.manufacturerService
            .GetAllAsync();
            var manufacturerView = mapper.Map<ICollection<ManufacturerViewModel>>(manufacturerDTO);

            ViewData["ManufacturerId"] = new SelectList(manufacturerView, "ManufacturerId", "Name", carModelView.ManufacturerId);
            return View(carModelView);
        }

        // POST: Models/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ModelId,Name,Code,ManufacturerId")] CarModelCreateViewModel model)
        {
            if (id != model.ModelId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var carModelDTO = mapper.Map<CarModelCreateDTO>(model);
                    await modelService.UpdateAsync(id, carModelDTO);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ModelExists(model.ModelId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            var manufacturerDTO = await this.manufacturerService
            .GetAllAsync();
            var manufacturerView = mapper.Map<ICollection<ManufacturerViewModel>>(manufacturerDTO);

            ViewData["ManufacturerId"] = new SelectList(manufacturerView, "ManufacturerId", "Name", model.ManufacturerId);
            return View(model);
        }

        // GET: Models/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var carModelDto = await this.modelService
                   .GetAsync(id);
            var carModelView = mapper.Map<CarModelViewModel>(carModelDto);
            if (carModelView == null)
            {
                return NotFound();
            }

            return View(carModelView);
        }

        // POST: Models/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await this.modelService.DeleteAsync(id);
            return RedirectToAction(nameof(Index));
        }

        private bool ModelExists(int id)
        {
            var carModelDto =  this.modelService
                   .GetAsync(id);
            if (carModelDto == null)
            {
                return false;
            }
            else
                return true;
        }
    }
}
