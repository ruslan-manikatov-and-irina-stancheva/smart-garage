﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SmartGarage.Services.Contracts;
using SmartGarage.Web.Models;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IServiceService serviceService;
        private readonly IUserService userService;
        private readonly IVehicleService vehicleService;
        private readonly IVisitService visitService;

        public HomeController(ILogger<HomeController> logger, IServiceService serviceService, IUserService userService, IVehicleService vehicleService, IVisitService visitService)
        {
            _logger = logger;
            this.serviceService = serviceService;
            this.userService = userService;
            this.vehicleService = vehicleService;
            this.visitService = visitService;
        }

        public IActionResult Index()
        {
            var counter = new CounterViewModel()
            {
                ServicesCount = this.serviceService.GetAllAsync().Result.Count,
                CustomersCount = this.userService.GetAllAsync().Result.Count(),
                VehiclesCount = this.vehicleService.GetAllAsync().Result.Count,
                VisitsCount = this.visitService.GetAllAsync().Result.Count,
            };

            return View(counter);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

    }
}
