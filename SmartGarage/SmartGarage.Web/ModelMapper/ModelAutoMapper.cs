﻿using AutoMapper;
using SmartGarage.Services.DTO;
using SmartGarage.Web.Models;
using System.Collections.Generic;

namespace SmartGarage.Web.ModelMapper
{
    public class ModelAutoMapper : Profile
    {
        public ModelAutoMapper()
        {
            CreateMap<ManufacturerDTO, ManufacturerViewModel>().ReverseMap();
            CreateMap<ManufacruterCreateViewModel, ManufacturerCreateDTO>().ReverseMap();
            CreateMap<ManufacturerDTO, ManufacruterCreateViewModel>().ReverseMap();

            CreateMap<ICollection<ManufacturerViewModel>, ManufacturerDTO>();

            CreateMap<ServiceDTO, ServiceViewModel>().ReverseMap();
            CreateMap<ServiceVehicleTypeDTO, ServiceVehicleTypeViewModel>().ReverseMap();
            CreateMap<ServiceCreateViewModel, ServiceCreateDTO>().ReverseMap();
            CreateMap<ServiceUpdateViewModel, ServiceUpdateDTO>().ReverseMap();

            CreateMap<CarModelViewModel, CarModelDTO>().ReverseMap();
            CreateMap<CarModelCreateViewModel, CarModelCreateDTO>().ReverseMap();

            CreateMap<UserGetDTO, UserUpdateViewModel>().ReverseMap();
            CreateMap<UserGetDTO, UserGetViewModel>().ReverseMap();
            CreateMap<UserDTO, UserUpdateViewModel>().ReverseMap();
            CreateMap<UserCreateDTO, UserCreateModelView>().ReverseMap();

            CreateMap<ManufacturerDTO, ManufacturerViewModel>().ReverseMap();
            CreateMap<ManufacruterCreateViewModel, ManufacturerCreateDTO>().ReverseMap();

            CreateMap<VehicleDTO, VehicleViewModel>().ReverseMap();
            CreateMap<VehicleDTO, VehicleCreateViewModel>();
            CreateMap<VehicleDetailsDTO, VehicleCreateViewModel>();
            CreateMap<VehicleDetailsDTO, VehicleDetailsViewModel>().ReverseMap();
            CreateMap<VehicleCreateViewModel, VehicleCreateDTO>().ReverseMap();
            CreateMap<VehicleUpdateByCustomerViewModel, VehicleUpdateByCustomerDTO>().ReverseMap();

            CreateMap<VehicleTypeDTO, VehicleTypeViewModel>().ReverseMap();
            CreateMap<VehicleTypeCreateDTO, VehicleTypeCreateViewModel>().ReverseMap();

            CreateMap<VisitDTO, VisitViewModel>().ReverseMap();
            CreateMap<VisitCreateViewModel, VisitCreateDTO>().ReverseMap();
            CreateMap<VisitUpdateViewModel, VisitUpdateDTO>().ReverseMap();
            CreateMap<VisitFullReportDTO, VisitFullReportViewModel>().ReverseMap();
            CreateMap<VisitFullReportDTO, VisitUpdateViewModel>();

            CreateMap<VisitStatusDTO, VisitStatusViewModel>().ReverseMap();
            CreateMap<VisitStatusCreateViewModel, VisitStatusCreateDTO>().ReverseMap();

            CreateMap<CarModelViewModel, CarModelDTO>().ReverseMap();
            CreateMap<CarModelCreateViewModel, CarModelCreateDTO>().ReverseMap();
            CreateMap<CarModelCreateViewModel, CarModelDTO>().ReverseMap();

            CreateMap<ForgottenPasswordDataViewModel, ForgottenPasswordDataDTO>().ReverseMap();

            CreateMap<RoleDTO, RoleViewModel>().ReverseMap();

            CreateMap<CustomerVehiclesDTO, CustomerVehiclesViewModel>().ReverseMap();
            CreateMap<CustomerGetAllServicesDTO, CustomerGetAllServicesViewModel>().ReverseMap();

            CreateMap<CustomerGetAllVisitsDTO, CustomerGetAllVisitsViewModel>().ReverseMap();
            CreateMap<CustomerGetVisitDTO, CustomerGetVisitViewModel>().ReverseMap();

        }

    }
}
