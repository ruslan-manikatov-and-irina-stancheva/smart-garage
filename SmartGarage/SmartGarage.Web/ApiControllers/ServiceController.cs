﻿using AutoMapper;
using CustomExceptions.Exceptions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTO;
using SmartGarage.Web.Models;
using System;
using System.Threading.Tasks;
using X.PagedList;

namespace SmartGarage.Web.ApiControllers
{
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    public class ServiceController : ControllerBase
    {
        private readonly IServiceService serviceService;
        private readonly IMapper mapper;

        public ServiceController(IServiceService serviceService, IMapper mapper)
        {
            this.serviceService = serviceService;
            this.mapper = mapper;
        }

        [HttpGet("")]
        [AllowAnonymous]
        public async Task<IActionResult> GetAllAsync([FromQuery] int? page)
        {
            try
            {
                var servicesDto = await this.serviceService.GetAllAsync();

                var pageModel = await servicesDto.ToPagedListAsync(page ?? 1, 10);

                if (pageModel.Count == 0)
                    return NoContent();
                return Ok(pageModel);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet("{id}")]
        [AllowAnonymous]
        public async Task<IActionResult> GetServiceAndTypeAsync(int id, [FromQuery] int? page)
        {
            try
            {
                var serviceDto = await this.serviceService.GetServiceAndTypeAsync(id);

                var pageModel = await serviceDto.ToPagedListAsync(page ?? 1, 10);

                if (pageModel.Count == 0)
                    return NoContent();
                return Ok(pageModel);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet("service/price")]
        [AllowAnonymous]
        public async Task<IActionResult> GetByNameAndPriceAsync([FromQuery] string service, decimal price, int? page)
        {
            try
            {
                var serviceDto = await this.serviceService.GetByNameAndPriceAsync(service, price);

                var pageModel = await serviceDto.ToPagedListAsync(page ?? 1, 10);

                if (pageModel.Count == 0)
                    return NoContent();
                return Ok(pageModel);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet("service")]
        [AllowAnonymous]
        public async Task<IActionResult> GetByNameAsync([FromQuery] string service, int? page)
        {
            try
            {
                var serviceDto = await this.serviceService.GetByNameAsync(service);

                var pageModel = await serviceDto.ToPagedListAsync(page ?? 1, 10);

                if (pageModel.Count == 0)
                    return NoContent();
                return Ok(pageModel);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet("price")]
        [AllowAnonymous]
        public async Task<IActionResult> GetByPriceAsync([FromQuery] decimal price, int? page)
        {
            try
            {
                var servicesDto = await this.serviceService.GetByPriceAsync(price);

                var pageModel = await servicesDto.ToPagedListAsync(page ?? 1, 10);

                if (pageModel.Count == 0)
                    return NoContent();
                return Ok(pageModel);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpPost("")]
        public async Task<IActionResult> PostAsync([FromBody] ServiceCreateViewModel serviceCreateViewModel)
        {
            try
            {
                var serviceDto = mapper.Map<ServiceCreateDTO>(serviceCreateViewModel);
                var newServiceDto = await this.serviceService.CreateAsync(serviceDto);

                return Created("post", newServiceDto);
            }
            catch (AccessViolationException)
            {
                return Forbid();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutAsync(int id, [FromBody] ServiceUpdateViewModel serviceUpdateViewModel)
        {
            try
            {
                var serviceDto = mapper.Map<ServiceUpdateDTO>(serviceUpdateViewModel);
                var updateServiceDto = await this.serviceService.UpdateAsync(id, serviceDto);

                return Ok(updateServiceDto);
            }
            catch (AccessViolationException)
            {
                return Forbid();
            }
            catch (NotFoundException ex) 
            {
                return NotFound(ex.Message);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            try
            {
                await this.serviceService.DeleteAsync(id);

                return NoContent();
            }
            catch (AccessViolationException)
            {
                return Forbid();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}
