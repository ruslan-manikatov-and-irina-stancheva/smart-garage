﻿using AutoMapper;
using CustomExceptions.Exceptions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SmartGarage.Data;
using SmartGarage.Data.Models;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTO;
using SmartGarage.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Web.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ModelController : ControllerBase
    {
        private readonly IModelService modelService;
        private readonly IMapper mapper;

        public ModelController(IModelService modelService, IMapper mapper)
        {
            this.modelService = modelService;
            this.mapper = mapper;
        }

        //[HttpGet("")]
        //public async Task<IActionResult> Get([FromQuery] PaginationParameters paginationParameters)
        //{
        //    try
        //    {
        //        var carModelDTO = await this.modelService
        //            .GetAllAsync(paginationParameters);
        //        var carModelView = mapper.Map<ICollection<CarModelViewModel>>(carModelDTO);
        //        return Ok(carModelView);
        //    }
        //    catch (Exception)
        //    {
        //        return BadRequest();
        //    }
        //}

       [HttpGet("{id}")]
        public async Task<IActionResult> Get(int? id)
        {
            try
            {
                var carModelDto = await this.modelService
                    .GetAsync(id);
                var carModelView = mapper.Map<CarModelViewModel>(carModelDto);
                return Ok(carModelView);
            }
            catch (ModelNotFoundException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception)
            {
                return Conflict();
            }
        }

        [HttpPost("")]
        public async Task<IActionResult> Post([FromBody] CarModelCreateViewModel viewModel)
        {

            try
            {
                var carModelDTO = mapper.Map<CarModelCreateDTO>(viewModel);
                var dto = await this.modelService.CreateAsync(carModelDTO);
                return Created("Post", dto);
            }
            catch (Exception)
            {
                return Conflict();
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] CarModelCreateViewModel viewModel)
        {
            try
            {
                var carModelDTO = mapper.Map<CarModelCreateDTO>(viewModel);
                var modelDTO = await modelService.UpdateAsync(id, carModelDTO);
                return Ok(modelDTO);
            }
            catch (ModelNotFoundException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception)
            {
                return Conflict();
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await this.modelService.DeleteAsync(id);
                return NoContent();
            }
            catch (ModelNotFoundException e)
            {
                return BadRequest(e.Message);
            }
            catch (ArgumentException e)
            {
                return Conflict(e.Message);
            }
        }


    }
}
