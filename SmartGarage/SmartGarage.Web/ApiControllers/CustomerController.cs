﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SmartGarage.Services.Contracts;
using System;
using System.Threading.Tasks;

namespace SmartGarage.Web.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomerServices customerServices;

        public CustomerController(ICustomerServices customerServices)
        {
            this.customerServices = customerServices;
        }

        [HttpGet("Vehicles")]
        public async Task<IActionResult> GetAllVehicles()
        {
            try
            {
                var username = User.Identity.Name; //will give me the user name of the Jwt authenticated user
                var vehiclesDto = await this.customerServices.GetAllVehicles(username);
                return Ok(vehiclesDto);
            }
            catch (Exception)
            {
                return BadRequest();
            }

        }


        [HttpGet("Services")]
        public async Task<IActionResult> GetAllServices()
        {
            try
            {
                var username = User.Identity.Name; //will give me the user name of the Jwt authenticated user

                var dto = await this.customerServices.GetAllServices(username);
                return Ok(dto);
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        [HttpGet("Visits/Id")]
        public async Task<IActionResult> GetVisitById([FromQuery] int visitId)
        {
            try
            {
                var username = User.Identity.Name; //will give me the user name of the Jwt authenticated user
                var dto = await this.customerServices.GetVisitReportById(username, visitId);
                return Ok(dto);
            }
            catch (ArgumentNullException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        [HttpGet("Visits/Currency")]
        public async Task<IActionResult> GetAllVisits([FromQuery] string currency)
        {
            try
            {
                var username = User.Identity.Name; //will give me the user name of the Jwt authenticated user
                var dto = await this.customerServices.GetAllVisitsReport(username, currency);
                return Ok(dto);
            }
            catch (ArgumentNullException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        [HttpGet("Visits/Id/Currency")]
        public async Task<IActionResult> GetVisitByIdAndCurrency([FromQuery] int visitId, string currency)
        {
            try
            {
                var username = User.Identity.Name; //will give me the user name of the Jwt authenticated user
                var dto = await this.customerServices.GetVisitReportByIdAndCurrency(username, visitId, currency);
                return Ok(dto);
            }
            catch (ArgumentNullException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        [HttpGet("Filter/Services/RegistrationPlate")]
        public async Task<IActionResult> GetAllServicesByVehicleRegPlate([FromQuery] string registrationPlate)
        {
            try
            {
                var username = User.Identity.Name; //will give me the user name of the Jwt authenticated user

                var dto = await this.customerServices.FilterServicesByRegPlate(username, registrationPlate);
                return Ok(dto);
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        [HttpGet("Filter/Services/Date")]
        public async Task<IActionResult> GetAllServicesByDate([FromQuery] DateTime date)
        {
            try
            {
                var username = User.Identity.Name; //will give me the user name of the Jwt authenticated user

                var dto = await this.customerServices.FilterServicesByDate(username, date);
                return Ok(dto);
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        [HttpGet("Filter/Services/RegistrationPlate/Date")]
        public async Task<IActionResult> GetAllServicesByDate([FromQuery] string registrationPlate, DateTime date)
        {
            try
            {
                var username = User.Identity.Name; //will give me the user name of the Jwt authenticated user

                var dto = await this.customerServices.FilterServicesByRegPlateAndDate(username, registrationPlate, date);
                return Ok(dto);
            }
            catch (Exception e)
            {
                throw e;
            }

        }
    }
}
