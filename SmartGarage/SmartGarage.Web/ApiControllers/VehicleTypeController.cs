﻿using AutoMapper;
using CustomExceptions.Exceptions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTO;
using SmartGarage.Web.Models;
using System;
using System.Threading.Tasks;
using X.PagedList;

namespace SmartGarage.Web.ApiControllers
{
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    public class VehicleTypeController : ControllerBase
    {
        private readonly IVehicleTypeService vehicleTypeService;
        private readonly IMapper mapper;

        public VehicleTypeController(IVehicleTypeService vehicleTypeService, IMapper mapper)
        {
            this.vehicleTypeService = vehicleTypeService;
            this.mapper = mapper;
        }

        [HttpGet("")]
        public async Task<IActionResult> GetAllAsync([FromQuery] int? page)
        {
            try
            {
                var vehicleTypesDto = await this.vehicleTypeService.GetAllAsync();

                var pageModel = await vehicleTypesDto.ToPagedListAsync(page ?? 1, 10);

                if (pageModel.Count == 0)
                    return NoContent();
                return Ok(pageModel);
            }
            catch (AccessViolationException)
            {
                return Forbid();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(int id)
        {
            try
            {
                var vehicleTypeDto = await this.vehicleTypeService.GetAsync(id);

                var vehicleTypeView = mapper.Map<VehicleTypeViewModel>(vehicleTypeDto);

                return Ok(vehicleTypeView);
            }
            catch (AccessViolationException)
            {
                return Forbid();
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpPost("")]
        public async Task<IActionResult> PostAsync([FromBody] VehicleTypeCreateViewModel vehicleTypeCreateViewModel)
        {
            try
            {
                var vehicleTypeDto = mapper.Map<VehicleTypeCreateDTO>(vehicleTypeCreateViewModel);
                var newVehicleTypeDto = await this.vehicleTypeService.CreateAsync(vehicleTypeDto);

                return Created("post", newVehicleTypeDto);
            }
            catch (AccessViolationException)
            {
                return Forbid();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutAsync(int id, [FromBody] VehicleTypeViewModel vehicleTypeViewModel)
        {
            try
            {
                var vehicleTypeDto = mapper.Map<VehicleTypeDTO>(vehicleTypeViewModel);
                var updateVehicleTypeDto = await this.vehicleTypeService.UpdateAsync(id, vehicleTypeDto);

                return Ok(updateVehicleTypeDto);
            }
            catch (AccessViolationException)
            {
                return Forbid();
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            try
            {
                await this.vehicleTypeService.DeleteAsync(id);

                return NoContent();
            }
            catch (AccessViolationException)
            {
                return Forbid();
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
