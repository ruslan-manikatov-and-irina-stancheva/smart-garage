﻿using AutoMapper;
using CustomExceptions.Exceptions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTO;
using SmartGarage.Web.Models;
using System;
using System.Threading.Tasks;
using X.PagedList;

namespace SmartGarage.Web.ApiControllers
{
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    public class VisitStatusController : ControllerBase
    {
        private readonly IVisitStatusService visitStatusService;
        private readonly IMapper mapper;

        public VisitStatusController(IVisitStatusService visitStatusService, IMapper mapper)
        {
            this.visitStatusService = visitStatusService;
            this.mapper = mapper;
        }

        [HttpGet("")]
        public async Task<IActionResult> GetAllAsync([FromQuery] int? page)
        {
            try
            {
                var visitStatusesDto = await this.visitStatusService.GetAllAsync();

                var pageModel = await visitStatusesDto.ToPagedListAsync(page ?? 1, 10);

                if (pageModel.Count == 0)
                    return NoContent();
                return Ok(pageModel);
            }
            catch (AccessViolationException)
            {
                return Forbid();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(int id)
        {
            try
            {
                var visitStatusDto = await this.visitStatusService.GetAsync(id);

                var visitStatusView = mapper.Map<VisitStatusViewModel>(visitStatusDto);

                return Ok(visitStatusView);
            }
            catch (AccessViolationException)
            {
                return Forbid();
            }
            catch (ArgumentNullException)
            {
                return NotFound();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpPost("")]
        public async Task<IActionResult> PostAsync([FromBody] VisitStatusCreateViewModel visitStatusCreateViewModel)
        {
            try
            {
                var visitStatusDto = mapper.Map<VisitStatusCreateDTO>(visitStatusCreateViewModel);
                var newVisitStatusDto = await this.visitStatusService.CreateAsync(visitStatusDto);

                return Created("post", newVisitStatusDto);
            }
            catch (AccessViolationException)
            {
                return Forbid();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutAsync(int id, [FromBody] VisitStatusViewModel visitStatusViewModel)
        {
            try
            {
                var visitStatusDto = mapper.Map<VisitStatusDTO>(visitStatusViewModel);
                var updateVisitStatusDto = await this.visitStatusService.UpdateAsync(id, visitStatusDto);

                return Ok(updateVisitStatusDto);
            }
            catch (AccessViolationException)
            {
                return Forbid();
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            try
            {
                await this.visitStatusService.DeleteAsync(id);

                return NoContent();
            }
            catch (AccessViolationException)
            {
                return Forbid();
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}
