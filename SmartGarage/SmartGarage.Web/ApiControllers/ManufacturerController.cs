﻿using AutoMapper;
using CustomExceptions.Exceptions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTO;
using SmartGarage.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Web.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ManufacturerController : ControllerBase
    {
        private readonly IManufacturerService manufacturerService;
        private readonly IMapper mapper;

        public ManufacturerController(IManufacturerService manufacturerService, IMapper mapper)
        {
            this.manufacturerService = manufacturerService;
            this.mapper = mapper;
        }

        [HttpGet("")]
        public async Task<IActionResult> Get()
        {
            try
            {
                var manufacturerDTO = await this.manufacturerService
                    .GetAllAsync();
                var manufacturerView = mapper.Map<ICollection<ManufacturerViewModel>>(manufacturerDTO);
                return Ok(manufacturerView);
            }
            catch(Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var manufacturerDTO = await this.manufacturerService
                    .GetAsync(id);
                var manufacturerView = mapper.Map<ManufacturerViewModel>(manufacturerDTO);
                return Ok(manufacturerView);
            }
            catch(ManufacturerNotFoundException e)
            {
                return BadRequest(e.Message);
            } 
            catch(Exception)
            {
                return Conflict();
            }
        }

        [HttpPost("")]
        public async Task<IActionResult> Post([FromBody] ManufacruterCreateViewModel viewModel)
        {
           
            try
            {
                var manufacturerDto = mapper.Map<ManufacturerCreateDTO>(viewModel);
                var dto = await this.manufacturerService.CreateAsync(manufacturerDto);
                return Created("Post", dto);
            }
            catch(Exception)
            {
                return Conflict();
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id,[FromBody] ManufacruterCreateViewModel viewModel)
        {
            try
            {
                var manufacturerDTO = mapper.Map<ManufacturerCreateDTO>(viewModel);
                var modelDTO = await manufacturerService.UpdateAsync(id, manufacturerDTO);
                return Ok(modelDTO);
            }
            catch (ManufacturerNotFoundException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception)
            {
                return Conflict();
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await this.manufacturerService.DeleteAsync(id);
                return NoContent();
            }
            catch (ManufacturerNotFoundException e)
            {
                return BadRequest(e.Message);
            }
            catch (ArgumentException e)
            {
                return Conflict(e.Message);
            }
        }


    }
}
