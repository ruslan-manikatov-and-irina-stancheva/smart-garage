﻿using AutoMapper;
using CustomExceptions.Exceptions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SmartGarage.Services.AuthenticateModels;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTO;
using SmartGarage.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace SmartGarage.Web.ApiControllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService userService;
        private readonly IMapper mapper;

        public UserController(IUserService userService, IMapper mapper)
        {
            this.userService = userService;
            this.mapper = mapper;
        }

        [HttpGet("")]
        public async Task<IActionResult> Get()
        {
            try
            {
                var userDto = await this.userService
                    .GetAllAsync();
                var userViewModel = mapper.Map<ICollection<UserGetViewModel>>(userDto);
                return Ok(userViewModel);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [HttpGet("Sort/Name")]
        public async Task<IActionResult> SortByName()
        {
            try
            {
                var userDto = await this.userService
                    .SortUsersByName();
                var userViewModel = mapper.Map<ICollection<UserGetViewModel>>(userDto);
                return Ok(userViewModel);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet("Sort/Date")]
        public async Task<IActionResult> SortByVisitDate()
        {
            try
            {
                var userDto = await this.userService
                    .SortUsersByVisitDate();
                var userViewModel = mapper.Map<ICollection<UserGetViewModel>>(userDto);
                return Ok(userViewModel);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet("Filter/Role")]
        public async Task<IActionResult> FilterByLastRole([FromQuery] string role)
        {
            try
            {
                var userDto = await this.userService
                    .FilterUsersByRole(role);
                var userViewModel = mapper.Map<ICollection<UserGetViewModel>>(userDto);
                return Ok(userViewModel);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet("Filter/LastName")]
        public async Task<IActionResult> FilterByLastName([FromQuery] string lastName)
        {
            try
            {
                var userDto = await this.userService
                    .FilterUsersByLastName(lastName);
                var userViewModel = mapper.Map<ICollection<UserGetViewModel>>(userDto);
                return Ok(userViewModel);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet("Filter/Email")]
        public async Task<IActionResult> FilterByEmail([FromQuery] string email)
        {
            try
            {
                var userDto = await this.userService
                    .FilterUsersByEmail(email);
                var userViewModel = mapper.Map<ICollection<UserGetViewModel>>(userDto);
                return Ok(userViewModel);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [HttpGet("Filter/Phone")]
        public async Task<IActionResult> FilterByPhone([FromQuery] string phone)
        {
            try
            {
                var userDto = await this.userService
                    .FilterUsersByPhone(phone);
                var userViewModel = mapper.Map<ICollection<UserGetViewModel>>(userDto);
                return Ok(userViewModel);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [HttpGet("Filter/Vehicle/Manufacturer")]
        public async Task<IActionResult> FilterByVehicleManufacturer([FromQuery] string make)
        {
            try
            {
                var userDto = await this.userService
                    .FilterUsersByVehicleMake(make);
                var userViewModel = mapper.Map<ICollection<UserGetViewModel>>(userDto);
                return Ok(userViewModel);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        [HttpGet("Filter/Vehicle/Model")]
        public async Task<IActionResult> FilterByVehicleModel([FromQuery] string model)
        {
            try
            {
                var userDto = await this.userService
                    .FilterUsersByVehicleModel(model);
                var userViewModel = mapper.Map<ICollection<UserGetViewModel>>(userDto);
                return Ok(userViewModel);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet("Filter/Vehicle/VisitInRange")]
        public async Task<IActionResult> FilterByVehicleModelInVisitRange([FromQuery]DateTime fromDate, DateTime toDate)
        {
            try
            {
                var userDto = await this.userService
                    .FilterUsersByVisitInRange(fromDate, toDate);
                var userViewModel = mapper.Map<ICollection<UserGetViewModel>>(userDto);
                return Ok(userViewModel);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }


        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int? id)
        {
            try
            {
                var userDto = await this.userService
                    .GetAsync(id);
                var userView = mapper.Map<UserGetViewModel>(userDto);
                return Ok(userView);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception)
            {
                return Conflict();
            }
        }



        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] UserUpdateViewModel viewModel)
        {
            try
            {
                var useDTO = mapper.Map<UserDTO>(viewModel);
                var dto = await userService.UpdateAsync(id, useDTO);
                return Ok(dto);
            }
            catch (UserNotFoundException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception)
            {
                return Conflict();
            }
        }

        [HttpDelete("{id}")]       
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await this.userService.DeleteAsync(id);
                return NoContent();
            }
            catch (UserNotFoundException e)
            {
                return BadRequest(e.Message);
            }
            catch (ArgumentException e)
            {
                return Conflict(e.Message);
            }
        }

        [HttpPost("")]
        public async Task<IActionResult> Post([FromBody] UserCreateModelView viewModel)
        {

            try
            {
                var carModelDTO = mapper.Map<UserCreateDTO>(viewModel);
                var dto = await this.userService.CreateAsync(carModelDTO);
                return Created("Post", dto);
            }
            catch (RoleNotFoundException e)
            {
                return BadRequest(e.Message);
            }
            /*catch (Exception)
            {
                return Conflict();
            }*/
        }
        
    }
}
