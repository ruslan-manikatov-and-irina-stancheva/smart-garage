﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SmartGarage.Services.AuthenticateModels;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTO;
using SmartGarage.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Web.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class AccountController : ControllerBase
    {
        private readonly IAccountService accountService;
        private readonly IMapper mapper;

        public AccountController(IAccountService accountService, IMapper mapper)
        {
            this.accountService = accountService;
            this.mapper = mapper;
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public async Task<IActionResult> Authenticate([FromBody] AuthenticateRequest model)
        {
            var response = await accountService.AuthenticateAsync(model);
            if (response == null)
            {
                return BadRequest(new { messa = "Invalid username or password!" });
            }
            return Ok(response);
        }
        [AllowAnonymous]
        [HttpPost("sendForgottenPassword/Email")]
        public async Task<IActionResult> SendForgottenPasswordEmail([FromQuery] string email)
        {
            var response = await accountService.SendForgottenPasswordEmail(email);
            if (response == null)
            {
                return BadRequest(new { messa = "Invalid username or password!" });
            }
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpPost("ChangeForgottenPassword")]
        public async Task<IActionResult> SendForgottenPasswordEmail([FromBody] ForgottenPasswordDataViewModel model)
        {

            var manufacturerDTO = mapper.Map<ForgottenPasswordDataDTO>(model);

            var response = await accountService.UpdateForgottenPassword(manufacturerDTO);
            if (response == false)
            {
                return BadRequest();
            }
            return Ok(response);
        }

    }
}

