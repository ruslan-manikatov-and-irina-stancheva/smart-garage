﻿using AutoMapper;
using CustomExceptions.Exceptions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTO;
using SmartGarage.Web.Models;
using System;
using System.Threading.Tasks;
using X.PagedList;

namespace SmartGarage.Web.ApiControllers
{
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    public class VisitController : ControllerBase
    {
        private readonly IVisitService visitService;
        private readonly IMapper mapper;

        public VisitController(IVisitService visitService, IMapper mapper)
        {
            this.visitService = visitService;
            this.mapper = mapper;
        }

        [HttpGet("")]
        public async Task<IActionResult> GetAllAsync([FromQuery] int? page)
        {
            try
            {
                var visitsDto = await this.visitService.GetAllAsync();

                //var visitsView = mapper.Map<ICollection<VisitViewModel>>(visitsDto);
                var visitsDtoPaged = await visitsDto.ToPagedListAsync(page ?? 1, 10);

                if (visitsDtoPaged.Count == 0)
                    return NoContent();
                return Ok(visitsDtoPaged);
            }
            catch (AccessViolationException)
            {
                return Forbid();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(int id)
        {
            try
            {
                var visitDto = await this.visitService.GetAsync(id);

                var visitView = mapper.Map<VisitViewModel>(visitDto);

                return Ok(visitView);
            }
            catch (AccessViolationException)
            {
                return Forbid();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpPost("")]
        public async Task<IActionResult> PostAsync([FromBody] VisitCreateViewModel visitCreateViewModel)
        {
            try
            {
                var visitDto = mapper.Map<VisitCreateDTO>(visitCreateViewModel);
                var newVisitDto = await this.visitService.CreateAsync(visitDto);

                return Created("post", newVisitDto);
            }
            catch (AccessViolationException)
            {
                return Forbid();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutAsync(int id, [FromBody] VisitUpdateViewModel visitUpdateViewModel)
        {
            try
            {
                var visitDto = mapper.Map<VisitUpdateDTO>(visitUpdateViewModel);
                var updateVisitDto = await this.visitService.UpdateAsync(id, visitDto);

                return Ok(updateVisitDto);
            }
            catch (AccessViolationException)
            {
                return Forbid();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            try
            {
                await this.visitService.DeleteAsync(id);

                return NoContent();
            }
            catch (AccessViolationException)
            {
                return Forbid();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}
