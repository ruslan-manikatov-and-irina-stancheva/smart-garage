﻿using AutoMapper;
using CustomExceptions.Exceptions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTO;
using SmartGarage.Web.Models;
using System;
using System.Threading.Tasks;
using X.PagedList;

namespace SmartGarage.Web.ApiControllers
{
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    public class VehicleController : ControllerBase
    {
        private readonly IVehicleService vehicleService;
        private readonly IMapper mapper;

        public VehicleController(IVehicleService vehicleService, IMapper mapper)
        {
            this.vehicleService = vehicleService;
            this.mapper = mapper;
        }

        [HttpGet("")]
        public async Task<IActionResult> GetAllAsync([FromQuery] int? page)
        {
            try
            {
                var vehiclesDto = await this.vehicleService.GetAllAsync();

                var pageModel = await vehiclesDto.ToPagedListAsync(page ?? 1, 10);

                if (pageModel.Count == 0)
                    return NoContent();
                return Ok(pageModel);
            }
            catch (AccessViolationException)
            {
                return Forbid();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(int id)
        {
            try
            {
                var vehicleDto = await this.vehicleService.GetAsync(id);

                var vehicleView = mapper.Map<VehicleDetailsViewModel>(vehicleDto);

                return Ok(vehicleView);
            }
            catch (AccessViolationException)
            {
                return Forbid();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet("user/{id}")]
        public async Task<IActionResult> GetByCustomerAsync(int id, [FromQuery] int? page)
        {
            try
            {
                var vehiclesDto = this.vehicleService.GetByCustomer(id);

                var pageModel = await vehiclesDto.ToPagedListAsync(page ?? 1, 10);

                if (pageModel.Count == 0)
                    return NoContent();
                return Ok(pageModel);
            }
            catch (AccessViolationException)
            {
                return Forbid();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet("user/{username}")]
        public async Task<IActionResult> GetByCustomerAsync(string username, [FromQuery] int? page)
        {
            try
            {
                var vehiclesDto = this.vehicleService.GetByCustomer(username);

                var pageModel = await vehiclesDto.ToPagedListAsync(page ?? 1, 10);

                if (pageModel.Count == 0)
                    return NoContent();
                return Ok(pageModel);
            }
            catch (AccessViolationException)
            {
                return Forbid();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpPost("")]
        public async Task<IActionResult> PostAsync([FromBody] VehicleCreateViewModel vehicleCreateViewModel)
        {
            try
            {
                var vehicleDto = mapper.Map<VehicleCreateDTO>(vehicleCreateViewModel);
                var newVehicleDto = await this.vehicleService.CreateAsync(vehicleDto);

                return Created("post", newVehicleDto);
            }
            catch (AccessViolationException)
            {
                return Forbid();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutAsync(int id, [FromBody] VehicleCreateViewModel vehicleUpdateViewModel)
        {
            try
            {
                var vehicleDto = mapper.Map<VehicleCreateDTO>(vehicleUpdateViewModel);
                var updateVehicleDto = await this.vehicleService.UpdateAsync(id, vehicleDto);

                return Ok(updateVehicleDto);
            }
            catch (AccessViolationException)
            {
                return Forbid();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpPut("user/{id}/{vehicleId}")]
        public async Task<IActionResult> PutByCustomerAsync(int id, int vehicleId, [FromBody] VehicleUpdateByCustomerViewModel vehicleUpdateByCustomerViewModel)
        {
            try
            {
                var vehicleDto = mapper.Map<VehicleUpdateByCustomerDTO>(vehicleUpdateByCustomerViewModel);
                var updateVehicleDto = await this.vehicleService.UpdateByCustomerAsync(vehicleId, id, vehicleDto);

                return Ok(updateVehicleDto);
            }
            catch (AccessViolationException)
            {
                return Forbid();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            try
            {
                await this.vehicleService.DeleteAsync(id);

                return NoContent();
            }
            catch (AccessViolationException)
            {
                return Forbid();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}
