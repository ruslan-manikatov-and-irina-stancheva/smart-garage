﻿namespace CustomExceptions
{
    static class ExceptionMessages
    {
        public static string ManufacturerNotFound = "Manufacturer with the given parameters, does not exist in database.";
        public static string ModelNotFound = "Vehicle model with the given parameters, does not exist in database.";
        public static string UserNotFound = "User with the given parameters, does not exist in database.";
        public static string RoleNotFound = "Role with the given parameters, does not exist in database.";
        public static string CanNotBeDelete = "There are object/s that depend on this row.";
        public static string AlreadyExist = "Already exist such variable.";
        public static string WrongDepartureDate = "Departure date can not be earlier than arrival date.";
        public static string NotFound = "Object is not found.";
    }
}
