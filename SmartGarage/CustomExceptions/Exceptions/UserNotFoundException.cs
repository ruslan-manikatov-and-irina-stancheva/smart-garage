﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomExceptions.Exceptions
{
    public class UserNotFoundException : Exception
    {
        public UserNotFoundException()
            : base(ExceptionMessages.UserNotFound)
        {

        }
    }
}
