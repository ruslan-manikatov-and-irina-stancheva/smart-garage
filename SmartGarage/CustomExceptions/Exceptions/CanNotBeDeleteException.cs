﻿using System;

namespace CustomExceptions.Exceptions
{
    public class CanNotBeDeleteException : Exception
    {
        public CanNotBeDeleteException()
            : base(ExceptionMessages.CanNotBeDelete)
        {

        }
    }
}