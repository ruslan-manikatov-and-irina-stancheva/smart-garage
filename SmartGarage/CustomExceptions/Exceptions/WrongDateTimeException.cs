﻿using System;

namespace CustomExceptions.Exceptions
{
    public class WrongDateTimeException : Exception
    {
        public WrongDateTimeException()
            : base(ExceptionMessages.WrongDepartureDate)
        {

        }
    }
}