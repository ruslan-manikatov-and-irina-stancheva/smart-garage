﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomExceptions.Exceptions
{
    public class RoleNotFoundException : Exception
    {
        public RoleNotFoundException()
            : base(ExceptionMessages.RoleNotFound)
        {

        }
    }
}
