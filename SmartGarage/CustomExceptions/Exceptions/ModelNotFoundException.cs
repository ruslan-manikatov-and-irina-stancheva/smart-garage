﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomExceptions.Exceptions
{
    public class ModelNotFoundException : Exception
    {
        public ModelNotFoundException()
            : base(ExceptionMessages.ModelNotFound)
        {

        }
    }
}
