﻿using System;

namespace CustomExceptions.Exceptions
{
    public class AlreadyExistException : Exception
    {
        public AlreadyExistException()
            : base(ExceptionMessages.AlreadyExist)
        {

        }
    }
}
