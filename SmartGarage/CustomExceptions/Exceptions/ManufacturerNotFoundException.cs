﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomExceptions.Exceptions
{
    public class ManufacturerNotFoundException : Exception
    {
        public ManufacturerNotFoundException()
            : base(ExceptionMessages.ManufacturerNotFound)
        {

        }

    }
}
