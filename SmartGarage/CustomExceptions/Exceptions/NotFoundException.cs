﻿using System;

namespace CustomExceptions.Exceptions
{
    public class NotFoundException : Exception
    {
        public NotFoundException()
            : base(ExceptionMessages.NotFound)
        {

        }
    }
}
