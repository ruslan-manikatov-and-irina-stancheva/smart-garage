﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SmartGarage.Data.Models
{
    public class VehicleType
    {
        public VehicleType()
        {
            this.Vehicles = new HashSet<Vehicle>();
            this.ServiceVehicleTypes = new HashSet<ServiceVehicleTypes>();
        }

        [Key]
        public int VehicleTypeId { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 3, ErrorMessage = "Value for {0} must be exactly {1} symbols.")]
        public string Name { get; set; }

        [Required]
        [Range(1.0, 99.99, ErrorMessage = "Please enter a value bigger than {1}.")]
        [Column(TypeName = "decimal(4,2)")]
        public decimal Coefficient { get; set; }

        public virtual ICollection<Vehicle> Vehicles { get; set; }

        public virtual ICollection<ServiceVehicleTypes> ServiceVehicleTypes { get; set; }
    }
}
