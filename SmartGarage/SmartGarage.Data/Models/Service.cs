﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartGarage.Data.Models
{
    public class Service
    {
        public Service()
        {
            this.VisitServices = new HashSet<VisitServices>();
            this.ServiceVehicleTypes = new HashSet<ServiceVehicleTypes>();
        }

        [Key]
        public int ServiceId { get; set; }

        [Required]
        [StringLength(70, MinimumLength = 3, ErrorMessage = "Value for {0} must be between {2} and {1}.")]
        public string Name { get; set; }

        [Required]
        [Range(0.1, 99999.99, ErrorMessage = "Please enter a value bigger than {1}.")]  
        [Column(TypeName = "decimal(7,2)")]
        public decimal Price { get; set; }

        public virtual ICollection<VisitServices> VisitServices { get; set; }

        public virtual ICollection<ServiceVehicleTypes> ServiceVehicleTypes { get; set; }
    }
}
