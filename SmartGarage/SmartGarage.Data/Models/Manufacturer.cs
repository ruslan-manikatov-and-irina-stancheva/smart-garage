﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SmartGarage.Data.Models
{
    public class Manufacturer
    {
        public Manufacturer()
        {
            this.Models = new HashSet<Model>();
        }

        [Key]
        public int ManufacturerId { get; set; }

        [Required]
        [StringLength(30, MinimumLength = 2, ErrorMessage = "Value for {0} must be between {2} and {1}.")]
        public string Name { get; set; }

        [Required]
        [StringLength(30, MinimumLength = 2, ErrorMessage = "Value for {0} must be between {2} and {1}.")]
        public string Code { get; set; }

        public virtual ICollection<Model> Models { get; set; }
    }
}
