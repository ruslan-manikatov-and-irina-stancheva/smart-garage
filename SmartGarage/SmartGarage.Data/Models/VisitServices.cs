﻿namespace SmartGarage.Data.Models
{
    public class VisitServices
    {
       
        public int VisitId { get; set; }

        public virtual Visit Visit { get; set; }

        public int ServiceId { get; set; }

        public virtual Service Service { get; set; }
    }
}
