﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SmartGarage.Data.Models
{
    public class User : IdentityUser<int>
    {
        public User()
        {
            this.CreatedOn = DateTime.Now;
            this.Vehicles = new HashSet<Vehicle>();
        }

        [Required]
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Value for {0} must be between {2} and {1}.")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Value for {0} must be between {2} and {1}.")]
        public string LastName { get; set; }

        [Required]
        [RegularExpression(@"^[0-9]{10}$", ErrorMessage = "Enter only alphabets and numbers of First Name")]
        public override string PhoneNumber { get; set; }

        public DateTime CreatedOn { get; set; }

        public virtual ICollection<Vehicle> Vehicles { get; set; }

        public bool IsDeleted { get; set; }

        public string ForgottenPasswordToken { get; set; }

        public DateTime ForgottenTokenCreation { get; set; }
    }
}
