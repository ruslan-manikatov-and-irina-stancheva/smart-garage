﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartGarage.Data.Models
{
    public class Visit
    {
        public Visit()
        {
            this.VisitStatusId = 1;
            this.ArriveDate = DateTime.Now;
            this.VisitServices = new HashSet<VisitServices>();
        }

        [Key]
        public int VisitId { get; set; }

        [ForeignKey("Vehicle")]
        public int VehicleId { get; set; }

        public virtual Vehicle Vehicle { get; set; }

        [ForeignKey("VisitStatus")]
        public int VisitStatusId { get; set; }

        public virtual VisitStatus VisitStatus { get; set; }

        public DateTime ArriveDate { get; set; }

        [Required]
        public DateTime DepartureDate { get; set; }

        public virtual ICollection<VisitServices> VisitServices { get; set; }

        public bool IsDeleted { get; set; }
    }
}
