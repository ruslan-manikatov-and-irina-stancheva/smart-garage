﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SmartGarage.Data.Models
{
    public class VisitStatus
    {
        public VisitStatus() { }

        [Key]
        public int VisitStatusId { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Value for {0} must be between {2} and {1}.")]
        public string Name { get; set; }

        public virtual ICollection<Visit> Visits { get; set; }
    }
}
