﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartGarage.Data.Models
{
    public class Vehicle
    {
        public Vehicle()
        {
            this.CreatedOn = DateTime.Now;
            this.Visits = new HashSet<Visit>();
        }

        [Key]
        public int VehicleId { get; set; }

        [Required]
        [StringLength(17, MinimumLength = 17, ErrorMessage = "Value for {0} must be exactly {1} symbols.")]
        public string Vin { get; set; }

        [Required]
        [StringLength(8, MinimumLength = 7, ErrorMessage = "Value for {0} must be between {2} and {1}.")]
        public string RegistrationPlate { get; set; }

        [ForeignKey("Owner")]
        public int OwnerId { get; set; }

        public virtual User Owner { get; set; }

        [ForeignKey("Model")]
        public int ModelId { get; set; }

        public virtual Model Model { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime Year { get; set; }

        public DateTime CreatedOn { get; set; }

        public virtual ICollection<Visit> Visits { get; set; }

        public bool IsDeleted { get; set; }

        [ForeignKey("VehicleType")]
        public int VehicleTypeId { get; set; }

        public virtual VehicleType VehicleType { get; set; }

    }
}
