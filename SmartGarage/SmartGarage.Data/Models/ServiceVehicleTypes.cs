﻿namespace SmartGarage.Data.Models
{
    public class ServiceVehicleTypes
    {
        public int ServiceId { get; set; }
        public virtual Service Service { get; set; }
        public int VehicleTypeId { get; set; }
        public virtual VehicleType VehicleType { get; set; }
    }
}
