﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SmartGarage.Data.Models;

namespace SmartGarage.Data.EntityConfigurations
{
    public class UserConfigurations : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.Property(a => a.IsDeleted)
                .HasDefaultValue(false);

            builder.HasQueryFilter(a => !a.IsDeleted);

            builder.HasIndex(u => u.PhoneNumber).IsUnique();
        }
    }
}
