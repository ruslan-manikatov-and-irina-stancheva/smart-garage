﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SmartGarage.Data.Models;

namespace SmartGarage.Data.EntityConfigurations
{
    public class VisitStatusConfigurations : IEntityTypeConfiguration<VisitStatus>
    {
        public void Configure(EntityTypeBuilder<VisitStatus> builder)
        {
            builder.HasIndex(v => v.Name).IsUnique();
        }
    }
}
