﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SmartGarage.Data.Models;

namespace SmartGarage.Data.EntityConfigurations
{
    public class VisitServicesConfigurations : IEntityTypeConfiguration<VisitServices>
    {
        public void Configure(EntityTypeBuilder<VisitServices> builder)
        {
            builder.HasKey(v => new { v.VisitId, v.ServiceId });

            builder.HasOne(v => v.Visit)
                .WithMany(v => v.VisitServices);

            builder.HasOne(v => v.Service)
                .WithMany(v => v.VisitServices);
        }
    }
}
