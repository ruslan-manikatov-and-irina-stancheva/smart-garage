﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SmartGarage.Data.Models;

namespace SmartGarage.Data.EntityConfigurations
{
    public class VehicleConfigurations : IEntityTypeConfiguration<Vehicle>
    {
        public void Configure(EntityTypeBuilder<Vehicle> builder)
        {
            builder.Property(c => c.IsDeleted)
              .HasDefaultValue(false);

            builder.HasQueryFilter(a => !a.IsDeleted);

            builder.HasIndex(v => v.RegistrationPlate).IsUnique();

            builder.HasIndex(v => v.Vin).IsUnique();
        }
    }
}
