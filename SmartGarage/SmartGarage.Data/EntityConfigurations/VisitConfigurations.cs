﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SmartGarage.Data.Models;


namespace SmartGarage.Data.EntityConfigurations
{
    public class VisitConfigurations : IEntityTypeConfiguration<Visit>
    {
        public void Configure(EntityTypeBuilder<Visit> builder)
        {
            builder.Property(p => p.IsDeleted)
            .HasDefaultValue(false);

            builder.HasQueryFilter(a => !a.IsDeleted);
        }
    }
}
