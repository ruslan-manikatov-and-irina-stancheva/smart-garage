﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SmartGarage.Data.Models;

namespace SmartGarage.Data.EntityConfigurations
{
    public class ServiceVehicleTypesConfigurations : IEntityTypeConfiguration<ServiceVehicleTypes>
    {
        public void Configure(EntityTypeBuilder<ServiceVehicleTypes> builder)
        {
            builder.HasKey(s => new { s.ServiceId, s.VehicleTypeId });

            builder.HasOne(s => s.Service)
                .WithMany(s => s.ServiceVehicleTypes);

            builder.HasOne(s => s.VehicleType)
                .WithMany(s => s.ServiceVehicleTypes);
        }
    }
}
