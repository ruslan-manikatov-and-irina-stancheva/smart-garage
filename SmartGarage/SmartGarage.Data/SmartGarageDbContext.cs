﻿using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SmartGarage.Data.Models;

namespace SmartGarage.Data
{
    public class SmartGarageDbContext : IdentityDbContext<User, Role, int>
    {
        public SmartGarageDbContext() { }

        public SmartGarageDbContext(DbContextOptions<SmartGarageDbContext> options)
            : base(options) { }

        public DbSet<Manufacturer> Manufacturers { get; set; }
        public DbSet<Model> Models { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<Vehicle> Vehicles { get; set; }
        public DbSet<Visit> Visits { get; set; }
        public DbSet<VisitStatus> VisitStatuses { get; set; }
        public DbSet<VisitServices> VisitServices { get; set; }
        public DbSet<ServiceVehicleTypes> ServiceVehicleTypes { get; set; }
        public DbSet<VehicleType> VehicleTypes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {

            var cascadeFKs = builder.Model.GetEntityTypes()
                .SelectMany(t => t.GetForeignKeys())
                .Where(fk => !fk.IsOwnership && fk.DeleteBehavior == DeleteBehavior.Cascade);

            //set delete behavior 
            foreach (var fk in cascadeFKs)
            {
                fk.DeleteBehavior = DeleteBehavior.Restrict;
            }

            builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

            this.Seed(builder);

            base.OnModelCreating(builder);
        }

        protected void Seed(ModelBuilder builder)
        {
            //Roles
            builder.Entity<Role>().HasData(
                new Role { Id = 1, Name = "employee", NormalizedName = "EMPLOYEE" },
                new Role { Id = 2, Name = "customer", NormalizedName = "CUSTOMER" },
                new Role { Id = 3, Name = "golden customer", NormalizedName = "GOLDEN CUSTOMER" }
            );

            //Password Hasher
            var passHasher = new PasswordHasher<User>();

            //Employee
            var employee = new User();
            employee.Id = 1;
            employee.UserName = "ruslan@smartgarage.com";
            employee.NormalizedUserName = "RUSLAN@SMARTGARAGE.COM";
            employee.Email = "ruslan@smartgarage.com";
            employee.NormalizedEmail = "RUSLAN@SMARTGARAGE.COM";
            employee.PasswordHash = passHasher.HashPassword(employee, "Ruslan26!");
            employee.PhoneNumber = "0887123213";
            employee.FirstName = "Ruslan";
            employee.LastName = "Manikatov";
            employee.SecurityStamp = "justSecurityStamp";

            builder.Entity<User>().HasData(employee);

            var employeeUserRole = new IdentityUserRole<int>();
            employeeUserRole.RoleId = 1;
            employeeUserRole.UserId = employee.Id;
            builder.Entity<IdentityUserRole<int>>().HasData(employeeUserRole);

            //Customer
            var customer = new User();
            customer.Id = 2;
            customer.UserName = "jessy_jay64@gmail.com";
            customer.NormalizedUserName = "JESSY_JAY@GMAIL.COM";
            customer.Email = "jessy_jay64@gmail.com";
            customer.NormalizedEmail = "JESSY_JAY@GMAIL.COM";
            customer.PasswordHash = passHasher.HashPassword(customer, "Jessy2!");
            customer.PhoneNumber = "0895856382";
            customer.FirstName = "Jessy";
            customer.LastName = "Jay";
            customer.SecurityStamp = "justAnothertringStamp";
            builder.Entity<User>().HasData(customer);

            var customerUserRole = new IdentityUserRole<int>();
            customerUserRole.RoleId = 2;
            customerUserRole.UserId = customer.Id;
            builder.Entity<IdentityUserRole<int>>().HasData(customerUserRole);
        }
    }
}
