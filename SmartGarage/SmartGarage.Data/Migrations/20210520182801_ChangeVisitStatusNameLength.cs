﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SmartGarage.Data.Migrations
{
    public partial class ChangeVisitStatusNameLength : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "VisitStatuses",
                maxLength: 20,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(15)",
                oldMaxLength: 15);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "0b7f5826-dab7-46c6-b799-c53c1bd55dfc");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "3d9a2136-7cef-485b-aba4-62be111c34f1");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "ce4311cb-cf7a-4aee-99e1-71deefc03b3c");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "7fabb289-c3ed-42d1-9d46-d48ecedae5a1", new DateTime(2021, 5, 20, 21, 28, 1, 423, DateTimeKind.Local).AddTicks(1308), "AQAAAAEAACcQAAAAEBdSi3atVAKxmz6PpemgzOt6D1AFBXunG+FxRiGfG9/VqnBXEr4v/2Ed8rQLY3uohA==" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "VisitStatuses",
                type: "nvarchar(15)",
                maxLength: 15,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 20);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "36affe6b-bc90-49c0-aa67-b00a7b9c0a5b");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "96f3df5e-06fd-4ded-a651-2cc2ca081c3d");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "e82478dc-187e-4e1c-af25-7b68caa44a7c");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "5ab82610-0dc7-46e4-a9e7-8093bfb6355f", new DateTime(2021, 5, 19, 13, 46, 22, 421, DateTimeKind.Local).AddTicks(6542), "AQAAAAEAACcQAAAAEB7/XBjlZY9ijv6x11tEP8zTTTpNVhx8KH5bbrLLo0vAN/t12SM1rV2iQjanZbQk+Q==" });
        }
    }
}
