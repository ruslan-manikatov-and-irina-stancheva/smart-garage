﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SmartGarage.Data.Migrations
{
    public partial class RemoveModelIsUniqueConstraint : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Models_Name",
                table: "Models");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "36affe6b-bc90-49c0-aa67-b00a7b9c0a5b");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "96f3df5e-06fd-4ded-a651-2cc2ca081c3d");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "e82478dc-187e-4e1c-af25-7b68caa44a7c");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "5ab82610-0dc7-46e4-a9e7-8093bfb6355f", new DateTime(2021, 5, 19, 13, 46, 22, 421, DateTimeKind.Local).AddTicks(6542), "AQAAAAEAACcQAAAAEB7/XBjlZY9ijv6x11tEP8zTTTpNVhx8KH5bbrLLo0vAN/t12SM1rV2iQjanZbQk+Q==" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "161f6a66-383a-422a-88fa-4eae554c01b5");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "a3981884-5bef-4451-ab08-4def4b49fd86");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "ca87a5d4-5c0a-4201-bf71-3638d29947ba");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "a1efaf18-42d4-4c2b-847c-cd3aa6b102ab", new DateTime(2021, 5, 19, 11, 45, 53, 5, DateTimeKind.Local).AddTicks(4593), "AQAAAAEAACcQAAAAEAp3vRUhtZ+dI4eswbX3NsAtx4m9iP+Yt+LxiaOLgkvYhzt74zjbMrRM8pwuVZAc8g==" });

            migrationBuilder.CreateIndex(
                name: "IX_Models_Name",
                table: "Models",
                column: "Name",
                unique: true);
        }
    }
}
