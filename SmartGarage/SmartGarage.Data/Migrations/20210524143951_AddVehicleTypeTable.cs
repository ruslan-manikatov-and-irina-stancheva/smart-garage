﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SmartGarage.Data.Migrations
{
    public partial class AddVehicleTypeTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "VehicleTypeId",
                table: "Vehicles",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "VehicleType",
                columns: table => new
                {
                    VehicleTypeId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 20, nullable: false),
                    Coefficient = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleType", x => x.VehicleTypeId);
                });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "9ed4c659-c192-43b2-a66b-e4d070f6eb84");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "c40b4580-dcd1-4499-9df3-26092fd61bff");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "18bfef3d-4acd-4879-95ba-8971378d337b");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash", "SecurityStamp" },
                values: new object[] { "b44fbee2-0434-4ec3-acab-40f40d7ef1c8", new DateTime(2021, 5, 24, 17, 39, 50, 846, DateTimeKind.Local).AddTicks(3724), "AQAAAAEAACcQAAAAEDvEeuzLuygKr7UQKnDhvAx1ns6OtyT0XxZBarTLSej5jQETHm51Ex+Qpo1MoPqGdA==", "justSecurityStamp" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "ef742962-dd86-449d-874e-00bae8295ed3", new DateTime(2021, 5, 24, 17, 39, 50, 860, DateTimeKind.Local).AddTicks(6269), "AQAAAAEAACcQAAAAENeISDMqQE4qggYfQl2aqdwR+e5EQ1W5ffGHTsUgPdzMGs4DQMhJf4pJ1Y6WF0QHXA==" });

            migrationBuilder.CreateIndex(
                name: "IX_Vehicles_VehicleTypeId",
                table: "Vehicles",
                column: "VehicleTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Vehicles_VehicleType_VehicleTypeId",
                table: "Vehicles",
                column: "VehicleTypeId",
                principalTable: "VehicleType",
                principalColumn: "VehicleTypeId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Vehicles_VehicleType_VehicleTypeId",
                table: "Vehicles");

            migrationBuilder.DropTable(
                name: "VehicleType");

            migrationBuilder.DropIndex(
                name: "IX_Vehicles_VehicleTypeId",
                table: "Vehicles");

            migrationBuilder.DropColumn(
                name: "VehicleTypeId",
                table: "Vehicles");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "8d52d5ae-a5ca-4e50-939d-8b61f0008949");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "d6be750a-e8de-404a-b2bb-8148b326d719");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "a12635e8-94e8-40b2-9c74-0e331a5fa210");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash", "SecurityStamp" },
                values: new object[] { "848b9aef-3cf6-4fe7-9c37-1729ac60fc33", new DateTime(2021, 5, 24, 8, 28, 56, 100, DateTimeKind.Local).AddTicks(5061), "AQAAAAEAACcQAAAAEKWUH4XX6haxr51cMJmwejlpI8lFLVS+YALVoA5XJ4BZ1SLZl6Gcci2kENaJcvlkjg==", "justAstringStamp" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "a803fe84-c1f6-4b6d-b33f-6db08cb89b47", new DateTime(2021, 5, 24, 8, 28, 56, 109, DateTimeKind.Local).AddTicks(8430), "AQAAAAEAACcQAAAAEPVkvAgaABf3MI3X+CQgZXgovhJ6E1k7gN4ZtJ8AaC5tXFh91g/1JNQxJJ7dRUaMLw==" });
        }
    }
}
