﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SmartGarage.Data.Migrations
{
    public partial class AddCustomerInDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "8d52d5ae-a5ca-4e50-939d-8b61f0008949");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "d6be750a-e8de-404a-b2bb-8148b326d719");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "a12635e8-94e8-40b2-9c74-0e331a5fa210");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "848b9aef-3cf6-4fe7-9c37-1729ac60fc33", new DateTime(2021, 5, 24, 8, 28, 56, 100, DateTimeKind.Local).AddTicks(5061), "AQAAAAEAACcQAAAAEKWUH4XX6haxr51cMJmwejlpI8lFLVS+YALVoA5XJ4BZ1SLZl6Gcci2kENaJcvlkjg==" });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "CreatedOn", "Email", "EmailConfirmed", "FirstName", "LastName", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[] { 2, 0, "a803fe84-c1f6-4b6d-b33f-6db08cb89b47", new DateTime(2021, 5, 24, 8, 28, 56, 109, DateTimeKind.Local).AddTicks(8430), "jessy_jay64@gmail.com", false, "Jessy", "Jay", false, null, "JESSY_JAY@GMAIL.COM", "JESSY_JAY@GMAIL.COM", "AQAAAAEAACcQAAAAEPVkvAgaABf3MI3X+CQgZXgovhJ6E1k7gN4ZtJ8AaC5tXFh91g/1JNQxJJ7dRUaMLw==", "0895856382", false, "justAnothertringStamp", false, "jessy_jay64@gmail.com" });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[] { 2, 2 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { 2, 2 });

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "876c88db-4fd7-44d7-9d0d-9620a7e01390");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "711c3a47-39b1-4efa-80bb-42b6f365eaa0");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "b4b5000e-e892-4723-9138-5446a5762719");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "377da484-be5e-495e-bb8b-33401e6b5b1f", new DateTime(2021, 5, 24, 8, 18, 21, 333, DateTimeKind.Local).AddTicks(4416), "AQAAAAEAACcQAAAAEBD2bR8T+ShSrUadQR5wEWzOZZsYdRR50irq00x8/yQloVxjTYftZ4tb3dOxgJdEgA==" });
        }
    }
}
