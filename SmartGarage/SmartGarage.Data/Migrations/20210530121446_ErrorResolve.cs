﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SmartGarage.Data.Migrations
{
    public partial class ErrorResolve : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ServiceVehicleTypes_VehicleType_VehicleTypeId",
                table: "ServiceVehicleTypes");

            migrationBuilder.DropForeignKey(
                name: "FK_Vehicles_VehicleType_VehicleTypeId",
                table: "Vehicles");

            migrationBuilder.DropPrimaryKey(
                name: "PK_VehicleType",
                table: "VehicleType");

            migrationBuilder.DropColumn(
                name: "ForgottenTokenCreationTime",
                table: "AspNetUsers");

            migrationBuilder.RenameTable(
                name: "VehicleType",
                newName: "VehicleTypes");

            migrationBuilder.RenameIndex(
                name: "IX_VehicleType_Name",
                table: "VehicleTypes",
                newName: "IX_VehicleTypes_Name");

            migrationBuilder.AddColumn<DateTime>(
                name: "ForgottenTokenCreation",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddPrimaryKey(
                name: "PK_VehicleTypes",
                table: "VehicleTypes",
                column: "VehicleTypeId");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "b85aff78-bef6-43b8-bb01-31aa5893030f");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "7b15f1dd-68b5-444e-8de9-98c138475c9f");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "82e87d3d-657d-4e7b-b9f2-1c27adeb50db");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "376bb68a-588a-4af1-8294-74ab4603b051", new DateTime(2021, 5, 30, 15, 14, 45, 597, DateTimeKind.Local).AddTicks(356), "AQAAAAEAACcQAAAAEJ4g9BzMJ+7hDuUeAZI3cvNx1Z+zh+v1995Efv3Sjel8OowmNPElxT3dBrv4NRlvLA==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "a5fd22e4-0b6d-4245-9386-14d083ddf67c", new DateTime(2021, 5, 30, 15, 14, 45, 606, DateTimeKind.Local).AddTicks(5032), "AQAAAAEAACcQAAAAENFfQFk7kZF70oqLHO7xhAdpMcpz+NHElHEiHjNCZAezQ9CpWMVjiJT1Abin2TktMg==" });

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceVehicleTypes_VehicleTypes_VehicleTypeId",
                table: "ServiceVehicleTypes",
                column: "VehicleTypeId",
                principalTable: "VehicleTypes",
                principalColumn: "VehicleTypeId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Vehicles_VehicleTypes_VehicleTypeId",
                table: "Vehicles",
                column: "VehicleTypeId",
                principalTable: "VehicleTypes",
                principalColumn: "VehicleTypeId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ServiceVehicleTypes_VehicleTypes_VehicleTypeId",
                table: "ServiceVehicleTypes");

            migrationBuilder.DropForeignKey(
                name: "FK_Vehicles_VehicleTypes_VehicleTypeId",
                table: "Vehicles");

            migrationBuilder.DropPrimaryKey(
                name: "PK_VehicleTypes",
                table: "VehicleTypes");

            migrationBuilder.DropColumn(
                name: "ForgottenTokenCreation",
                table: "AspNetUsers");

            migrationBuilder.RenameTable(
                name: "VehicleTypes",
                newName: "VehicleType");

            migrationBuilder.RenameIndex(
                name: "IX_VehicleTypes_Name",
                table: "VehicleType",
                newName: "IX_VehicleType_Name");

            migrationBuilder.AddColumn<DateTime>(
                name: "ForgottenTokenCreationTime",
                table: "AspNetUsers",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddPrimaryKey(
                name: "PK_VehicleType",
                table: "VehicleType",
                column: "VehicleTypeId");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "1b830e69-9f7a-49e5-b0e6-0a7d820f6e0a");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "a69cd29e-1a1e-4a3f-8933-94068f0c73bf");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "2b13c4ae-9558-4fbb-b8d3-1ab7a1e7d98a");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "0c4c74ea-2380-4e72-84ea-70bf974b3368", new DateTime(2021, 5, 29, 13, 30, 18, 23, DateTimeKind.Local).AddTicks(8469), "AQAAAAEAACcQAAAAEHmzXZobBU9XDsE5dhZ52wEgAXCq4nTzpw9fv/Aqysms6SrBZN4y3d6G+A00atbseA==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "9cff525d-4907-47d6-bb9f-928731677bc2", new DateTime(2021, 5, 29, 13, 30, 18, 37, DateTimeKind.Local).AddTicks(1914), "AQAAAAEAACcQAAAAEHJReh+y3OcxRQ0+1fpRJeFd6tTPQaZEG77BoUBGSyh+eonALjWbTq8IpSV4/UCQ5w==" });

            migrationBuilder.AddForeignKey(
                name: "FK_ServiceVehicleTypes_VehicleType_VehicleTypeId",
                table: "ServiceVehicleTypes",
                column: "VehicleTypeId",
                principalTable: "VehicleType",
                principalColumn: "VehicleTypeId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Vehicles_VehicleType_VehicleTypeId",
                table: "Vehicles",
                column: "VehicleTypeId",
                principalTable: "VehicleType",
                principalColumn: "VehicleTypeId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
