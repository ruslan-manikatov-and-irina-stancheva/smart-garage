﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SmartGarage.Data.Migrations
{
    public partial class SecurityStampSeedUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "161f6a66-383a-422a-88fa-4eae554c01b5");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "a3981884-5bef-4451-ab08-4def4b49fd86");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "ca87a5d4-5c0a-4201-bf71-3638d29947ba");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash", "SecurityStamp" },
                values: new object[] { "a1efaf18-42d4-4c2b-847c-cd3aa6b102ab", new DateTime(2021, 5, 19, 11, 45, 53, 5, DateTimeKind.Local).AddTicks(4593), "AQAAAAEAACcQAAAAEAp3vRUhtZ+dI4eswbX3NsAtx4m9iP+Yt+LxiaOLgkvYhzt74zjbMrRM8pwuVZAc8g==", "justAstringStamp" });

            migrationBuilder.CreateIndex(
                name: "IX_Models_Name",
                table: "Models",
                column: "Name",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Models_Name",
                table: "Models");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "3bc1230a-2590-4d36-9b06-27baeb79ac02");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "e274cdb5-2524-4d8e-94b4-7c09e530e1ff");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "10a9b691-df2c-429b-a8b2-7d0e943eac6e");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash", "SecurityStamp" },
                values: new object[] { "e9580abb-6ae6-4057-9c3d-58267e185db4", new DateTime(2021, 5, 15, 19, 2, 35, 244, DateTimeKind.Local).AddTicks(4061), "AQAAAAEAACcQAAAAEOlXv8FmNEsvPdWNbZf+hRzthoMbvyROUxud+TlE5bclFtATyuIh4PEOvQ+wdiEd1g==", null });
        }
    }
}
