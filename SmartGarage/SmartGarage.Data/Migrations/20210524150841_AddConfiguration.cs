﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SmartGarage.Data.Migrations
{
    public partial class AddConfiguration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ServiceVehicleTypes",
                columns: table => new
                {
                    ServiceId = table.Column<int>(nullable: false),
                    VehicleTypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceVehicleTypes", x => new { x.ServiceId, x.VehicleTypeId });
                    table.ForeignKey(
                        name: "FK_ServiceVehicleTypes_Services_ServiceId",
                        column: x => x.ServiceId,
                        principalTable: "Services",
                        principalColumn: "ServiceId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ServiceVehicleTypes_VehicleType_VehicleTypeId",
                        column: x => x.VehicleTypeId,
                        principalTable: "VehicleType",
                        principalColumn: "VehicleTypeId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "4377ab67-8866-4003-bb4a-c85dea4437f7");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "06e8a45f-f2b4-40a4-accf-ee9597169ec8");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "dc36970c-17a3-4d65-a3e3-960bbc7ef09e");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "a3cfe169-35dd-4f89-866f-a5025393589a", new DateTime(2021, 5, 24, 18, 8, 40, 263, DateTimeKind.Local).AddTicks(2424), "AQAAAAEAACcQAAAAEEOSkCG30goT3/P9OmVr7KfPlE4MTHQLxUehyTaxIHIpbHCtOve+bQo8gNU578TQLw==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "27a32dbf-528d-4eaf-a176-ef35aff7c0c5", new DateTime(2021, 5, 24, 18, 8, 40, 279, DateTimeKind.Local).AddTicks(6932), "AQAAAAEAACcQAAAAEIyhsCJLoREiBYwHIrM7fMVdfdXqCeLTX4QOG+UYOWAJsTzp6oAjiBu9ZsjohjjdwA==" });

            migrationBuilder.CreateIndex(
                name: "IX_ServiceVehicleTypes_VehicleTypeId",
                table: "ServiceVehicleTypes",
                column: "VehicleTypeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ServiceVehicleTypes");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "9ed4c659-c192-43b2-a66b-e4d070f6eb84");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "c40b4580-dcd1-4499-9df3-26092fd61bff");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "18bfef3d-4acd-4879-95ba-8971378d337b");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "b44fbee2-0434-4ec3-acab-40f40d7ef1c8", new DateTime(2021, 5, 24, 17, 39, 50, 846, DateTimeKind.Local).AddTicks(3724), "AQAAAAEAACcQAAAAEDvEeuzLuygKr7UQKnDhvAx1ns6OtyT0XxZBarTLSej5jQETHm51Ex+Qpo1MoPqGdA==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "ef742962-dd86-449d-874e-00bae8295ed3", new DateTime(2021, 5, 24, 17, 39, 50, 860, DateTimeKind.Local).AddTicks(6269), "AQAAAAEAACcQAAAAENeISDMqQE4qggYfQl2aqdwR+e5EQ1W5ffGHTsUgPdzMGs4DQMhJf4pJ1Y6WF0QHXA==" });
        }
    }
}
