﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SmartGarage.Data.Migrations
{
    public partial class AddColumnsInUserTableChangeFloatToDecimalInServicesTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "Coefficient",
                table: "VehicleType",
                type: "decimal(4,2)",
                nullable: false,
                oldClrType: typeof(float),
                oldType: "real");

            migrationBuilder.AlterColumn<decimal>(
                name: "Price",
                table: "Services",
                type: "decimal(7,2)",
                nullable: false,
                oldClrType: typeof(float),
                oldType: "real");

            migrationBuilder.AddColumn<string>(
                name: "ForgottenPasswordToken",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ForgottenTokenCreationTime",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "1b830e69-9f7a-49e5-b0e6-0a7d820f6e0a");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "a69cd29e-1a1e-4a3f-8933-94068f0c73bf");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "2b13c4ae-9558-4fbb-b8d3-1ab7a1e7d98a");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "0c4c74ea-2380-4e72-84ea-70bf974b3368", new DateTime(2021, 5, 29, 13, 30, 18, 23, DateTimeKind.Local).AddTicks(8469), "AQAAAAEAACcQAAAAEHmzXZobBU9XDsE5dhZ52wEgAXCq4nTzpw9fv/Aqysms6SrBZN4y3d6G+A00atbseA==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "9cff525d-4907-47d6-bb9f-928731677bc2", new DateTime(2021, 5, 29, 13, 30, 18, 37, DateTimeKind.Local).AddTicks(1914), "AQAAAAEAACcQAAAAEHJReh+y3OcxRQ0+1fpRJeFd6tTPQaZEG77BoUBGSyh+eonALjWbTq8IpSV4/UCQ5w==" });

            migrationBuilder.CreateIndex(
                name: "IX_VehicleType_Name",
                table: "VehicleType",
                column: "Name",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_VehicleType_Name",
                table: "VehicleType");

            migrationBuilder.DropColumn(
                name: "ForgottenPasswordToken",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "ForgottenTokenCreationTime",
                table: "AspNetUsers");

            migrationBuilder.AlterColumn<float>(
                name: "Coefficient",
                table: "VehicleType",
                type: "real",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(4,2)");

            migrationBuilder.AlterColumn<float>(
                name: "Price",
                table: "Services",
                type: "real",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(7,2)");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "4377ab67-8866-4003-bb4a-c85dea4437f7");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "06e8a45f-f2b4-40a4-accf-ee9597169ec8");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "dc36970c-17a3-4d65-a3e3-960bbc7ef09e");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "a3cfe169-35dd-4f89-866f-a5025393589a", new DateTime(2021, 5, 24, 18, 8, 40, 263, DateTimeKind.Local).AddTicks(2424), "AQAAAAEAACcQAAAAEEOSkCG30goT3/P9OmVr7KfPlE4MTHQLxUehyTaxIHIpbHCtOve+bQo8gNU578TQLw==" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "27a32dbf-528d-4eaf-a176-ef35aff7c0c5", new DateTime(2021, 5, 24, 18, 8, 40, 279, DateTimeKind.Local).AddTicks(6932), "AQAAAAEAACcQAAAAEIyhsCJLoREiBYwHIrM7fMVdfdXqCeLTX4QOG+UYOWAJsTzp6oAjiBu9ZsjohjjdwA==" });
        }
    }
}
