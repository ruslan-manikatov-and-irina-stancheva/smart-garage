﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SmartGarage.Data.Migrations
{
    public partial class ChangeYearInVehiclesFromDateTimeToDateFormat : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { 2, 2 });

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "876c88db-4fd7-44d7-9d0d-9620a7e01390");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "711c3a47-39b1-4efa-80bb-42b6f365eaa0");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "b4b5000e-e892-4723-9138-5446a5762719");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "377da484-be5e-495e-bb8b-33401e6b5b1f", new DateTime(2021, 5, 24, 8, 18, 21, 333, DateTimeKind.Local).AddTicks(4416), "AQAAAAEAACcQAAAAEBD2bR8T+ShSrUadQR5wEWzOZZsYdRR50irq00x8/yQloVxjTYftZ4tb3dOxgJdEgA==" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "7fbb7d90-c929-46a9-9f56-a4994aa88b65");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "688607fd-719a-412c-b9db-f3d639d268d6");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "a2984288-a32b-4be0-9b32-6ec24aa6174f");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "CreatedOn", "PasswordHash" },
                values: new object[] { "3682b24b-4ecb-4a51-b7d9-25b00b85e972", new DateTime(2021, 5, 24, 7, 39, 42, 217, DateTimeKind.Local).AddTicks(9281), "AQAAAAEAACcQAAAAEPaqxYbySZBNbeH2xEH2M6nnGpdFjtTRp67ORnkecFyAuYB3nYHqgYus3iGGhpahqw==" });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "CreatedOn", "Email", "EmailConfirmed", "FirstName", "LastName", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[] { 2, 0, "9d8df822-4df5-4739-95a0-8d0b62f83455", new DateTime(2021, 5, 24, 7, 39, 42, 226, DateTimeKind.Local).AddTicks(7421), "jessy_jay64@gmail.com", false, "Jessy", "Jay", false, null, "JESSY_JAY@GMAIL.COM", "JESSY_JAY@GMAIL.COM", "AQAAAAEAACcQAAAAEFRABzDuGMY84bAmwGc7DCTcZ1WpMxgWnEfyEtZ8wREKzvN6AXwq9nGr8cNZqbPvNA==", "0895856382", false, "justAnothertringStamp", false, "jessy_jay64@gmail.com" });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[] { 2, 2 });
        }
    }
}
